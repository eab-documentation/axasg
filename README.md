## Getting Started
```
cd ./website && npm install && npm start
```

## Hot reload
`npm start` have hot reload feature. The page content change will auto update. But side bar not,
you need to re-start the app again to see the change.
