---
id: cicd-demo-project-guideline
title: Demo Project CI/CD Guideline 持续集成与持续交付例子指南
sidebar_label: Demo Project CI/CD Guideline 持续集成与持续交付例子指南
---
| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |

I will use a simple demo (react starter demo) to show how to do CI/CD on EAB

我会用一个简单的例子 （react的示例项目）去展示在EAB需要怎么做持续集成持续交付

## Before start
Make sure the following items are ready.

确保下面的项目都准备好了。

| Key | Value | Link | Version | Comment |
| :-: | :-: | :-: | :-: | :-: |
| Git Repository | Gitlab | http://192.168.225.151/ | v12.9.4-ee | Use Gitlab on data center as example<br/>使用数据中心的Gitlab作为例子 |
| CI Server | Gitlab Runner | https://docs.gitlab.com/runner/ | v12.10.0 | Setup by IT Department.<br/>Refer to: https://docs.gitlab.com/runner/<br/>这个是IT部门提前搭建好的<br/>搭建方法参考：https://docs.gitlab.com/runner/ |
| Host Server | Openshift | https://admin.ocp.eabdc:8443/ | OpenShift Master: v3.9.25 <br/>Kubernetes Master: v1.9.1+a0ce1bc657 <br/> OpenShift Web Console: v3.9.14 |  |
| Language | Node.js | https://nodejs.org/ | v12.14.0 |  |

## Create Project on Gitlab
[![Create Project on Gitlab](assets/img/cicd/example1.png)](assets/img/cicd/example1.png)

- Name: ReactDemo
[![Enter new project Name](assets/img/cicd/example2.png)](assets/img/cicd/example2.png)

[![New project created](assets/img/cicd/example3.png)](assets/img/cicd/example3.png)

## Clone project
[![Clone project](assets/img/cicd/example4.png)](assets/img/cicd/example4.png)

## Write code (Use React Demo)
As an example, I use [React Demo tool](https://github.com/facebook/create-react-app) to generate code.

作为例子，我使用[React Demo工具](https://github.com/facebook/create-react-app)来生成代码

```
npx create-react-app reactdemo
cd reactdemo
npm i (or yarn)
npm run start (or yarn start)
```

Open browser and visit [localhost:3000](localhost:3000), then you can see the React Demo UI.

打开浏览器访问 [localhost:3000](localhost:3000), 就可以看到React的例子了。
[![See react](assets/img/cicd/example5.png)](assets/img/cicd/example5.png)

## Check in
[![See react](assets/img/cicd/example6.png)](assets/img/cicd/example6.png)

## Test code immediately after checking in
Sometimes we want to do unit test / eslint check immediately after checking in. If test fail, system can notify the related developer. Then developer can know the result immediately, fix it and check in again. Or we will know the issue in the future days and the bug fix become very hard.

有时候我们想checkin代码之后马上进行单元测试或者eslint检查。如果失败，系统就通知开发者，开发者收到消息后立马进行修复，然后重新提交。否则，我们就要等到几天后才发现问题，到时候改起来就变得很困难了。

How to realize the purpose? We can add a CI. Runs CI script to check the code.

如何实现这个目标呢？我们需要增加一个CI。通过跑CI脚本进行代码的检查。

### Write a CI script
Gitlab provide a CI platform to run pipeline on Gitlab Runner. We need write down CI script on ```.gitlab-ci.yml``` file. Then Gitlab CI will read this file and auto run after each check in.

Gitlab提供了CI平台去在Gitlab Runner里面跑脚本。我们只需要在```.gitlab-ci.yml```文件内定义好CI要跑的脚本， Gitlab就会在你每次check in代码之后自动读这个文件并跑里面的脚本。

Because the CI script will run on Gitlab Runner and Gitlab Runner is clean. Only source code on the Runner. We need prepare every things and then start test. So we need install dependencies firstly and then run test script.

因为CI脚本会在Gitlab Runner上跑，并且Gitlab Runner是干净的。里面除了代码没有别的东西。所以我们需要准备好所有东西，再让它开始测试。所以我们需要先装库，然后跑测试任务。

More about Gitlab CI script, please refer to: https://docs.gitlab.com/ee/ci/yaml/

更多关于Gitlab CI脚本的信息，可以参考：https://docs.gitlab.com/ee/ci/yaml/

To this project, we define a following script:

对于这个项目，它自带了单元测试，我们直接采用这个单元测试来进行CI测试，但是由于这个命令```npm run test```需要人手去选择测试的类型，所以我们在命令前面加上了CI=true


对于这个例子, 我们定义了下面的这个脚本

```
image: node:12

stages:
  - check

check:
  stage: check
  script:
    - npm i
    - npm run test

```

If we directly run command ```npm run test (yarn test)``` on local environment, it will show a question. Then developer select an option to continue. 

如果我们直接在本地运行```npm run test (yarn test)```，它会有一个问题显示出来，开发者需要选择一个项目来继续。

[![See react](assets/img/cicd/example7.png)](assets/img/cicd/example7.png)

But for CI, we can't select anything manually. So we need set ```CI=true``` before ```react-scripts test``` according to this [document](https://create-react-app.dev/docs/running-tests/#linux-macos-bash). After setting, this script can be run automatically.

但是在CI当中，我们没有办法手动选择任何东西。所以根据[文档](https://create-react-app.dev/docs/running-tests/#linux-macos-bash)，我们需要在```react-scripts test```前面设置```CI=true```。当设置完后，这个脚本就可以自动去跑了。

[![See react](assets/img/cicd/example8.png)](assets/img/cicd/example8.png)

### Monitor test status
Check in ```.gitlab-ci.yml``` then we can try to run the CI script.

提交```.gitlab-ci.yml```代码，然后我们就可以尝试去跑CI脚本了。

[![See react](assets/img/cicd/example9.png)](assets/img/cicd/example9.png)

Next, we open Gitlab CI/CD page of your project. A new CI pipeline is created.

接下来我们打开Gitlab项目的CI/CD页面。可以看到有个新的CI pipeline创建出来了

[![See react](assets/img/cicd/example10.png)](assets/img/cicd/example10.png)

Click the progress circle to go into the detail page.

点击进度圈圈进入到详情页。

[![See react](assets/img/cicd/example11.png)](assets/img/cicd/example11.png)

[![See react](assets/img/cicd/example12.png)](assets/img/cicd/example12.png)
On stage(1), CI start download docker node:12

在（1）中，下载好定义的node:12 docker

From stage (2), start to execute script.

从（2）开始，就开始执行脚本内容

### CI Fail
If CI fail，what is going on?

如果CI发生错误，会有什么样的效果？

Let's make some mistake.

让我们制造一些错误

We can see that the link text should be ```Learn React``` from test case file.

在测试用例文件中，我们可以看到这个链接的文字期望值是```Learn React```

[![See react](assets/img/cicd/example14.png)](assets/img/cicd/example14.png)
[![See react](assets/img/cicd/example13.png)](assets/img/cicd/example13.png)

If someone add digital ```2``` after ```Learn React``` accidentally and check in to git.

如果某条友不小心在代码中的这个文字加了个```2```在```Learn React```后面，并且提交了上去。

[![See react](assets/img/cicd/example15.png)](assets/img/cicd/example15.png)

我们可以看到Gitlab的CI pipeline会报错，并且会发邮件提醒开发者。

[![See react](assets/img/cicd/example16.png)](assets/img/cicd/example16.png)

[![See react](assets/img/cicd/example17.png)](assets/img/cicd/example17.png)

### For project
If you are in a project and many members work for this project. Then we need merge request. We can set ```Pipelines must succeed``` for merge request, then this CI test is very helpful. Your code can't be merged if CI fail.

如果你是在一个项目当中，并且很多团队成员在为它开发。这个时候我们就需要merge request了。 我们可以设置在设置页面设置merge request的时候```Pipelines must succeed```，这个对CI的测试很有用。当一个开发者请求合并代码的时候，如果CI没有通过，就合并不了代码。
[![See react](assets/img/cicd/example18.png)](assets/img/cicd/example18.png)

## Create SIT environment
We use Openshift as server manage platform to host SIT environment with build-in docker. The following paragraph shows how to create and maintain SIT environment.

我们用Openshift作为SIT场的服务器管理平台, 内部自带docker。下面讲解一下如何使用Openshift创建并维护SIT场。

### Define ```.s2i``` file
In Openshift, there are 2 steps to run a service - build and deploy. 
How to define what script will Openshift run on build and deploy stages? 
Use .s2i(source to image) to define. 

Reter to offical document: [https://docs.openshift.com/container-platform/3.6/creating_images/s2i.html](https://docs.openshift.com/container-platform/3.6/creating_images/s2i.html)

在Openshift，我们需要两个步骤去运行服务 - build和deploy。 
怎么去定义Openshift中的build和deploy里面的脚本呢？ 
使用.s2i (代码到镜像)去定义

请参考文档：[https://docs.openshift.com/container-platform/3.6/creating_images/s2i.html](https://docs.openshift.com/container-platform/3.6/creating_images/s2i.html)

We put ```.s2i/bin``` folder under project root. ```assemble``` file is for defining build script and ```run``` file is for defining deploy script. Just like this:

我们把这个```.s2i/bin```放在根目录下面，里面有两个文件```assemble```和```run```，```assemble```是用来定于build的行为的，```run```是用来定于deploy的行为的，代码结构如下：

```
reactdemo
└───.s2i
│    └───bin
│        └───assemble // for defining build script, 用于定义build的行为
│        └───run // for defining deploy script，用于定义deploy的行为
└───OTHER_FOLDERS
```
In Openshift, we only need install lib for once for a set of static code. In other words, we need only run ```npm install``` for once. But for running service, maybe we need reboot service when we meet some issues. In other words, we need run ```npm run start``` for more than once. So we define ```npm install``` in ```build``` file and define ```npm run start``` in ```run``` file

在Openshift服务器中，对于一段固定的代码，我们只需要安装一次lib，也就是只要跑一次```npm install```,但在运行服务的时候我们可能会遇到服务重启的现象，重新的时候就需要重新运行```npm run start```，所以在```build```文件当中，我们定义```npm install```，在deploy的时候我们在```run```文件中定义```npm run start```

There are many new internal projects which has ```.s2i``` folder with several debug-already scripts inside. The most important scripts are ```npm install``` for ```assemble``` file and ```npm run start``` for ```run``` file. Just copy from other projects and modify the important scripts.

公司内部有很多新的node.js项目都有这个 ```.s2i```文件夹，里面定义了很多调试好的脚本，其中最重要的是```assemble```文件中的```npm install```和```run```文件中的```npm run start```，复制他们过来，修改这两个重要的脚本即可. 

assemble file:
```
...
echo "---> Installing dependencies"
npm install
echo "---> Installed dependencies"
...
```

run file
```
...
run_node() {
  echo "npm run start"
  exec npm run start
} 
...
```

### Build
[Log in Openshift](/axasg/docs/architecture/ease-architecture#openshift) then create a project.

[登陆Openshift](/axasg/docs/architecture/ease-architecture#openshift)然后创建一个项目。

[![See react](assets/img/cicd/example20.png)](assets/img/cicd/example20.png)

[![See react](assets/img/cicd/example21.png)](assets/img/cicd/example21.png)

Click the project you create and then redirect to project detail page.

点击你创建对项目，会跳转到项目详细页面。

[![See react](assets/img/cicd/example22.png)](assets/img/cicd/example22.png)

Before building on Openshift, we need make sure the following items

在Openshift进行build之前，需要确定以下几点

- image // 使用什么镜像
- git repository url // 代码仓库的地址
- branch // 代码所使用的分支
- git credential // 代码账号

For our react demo project, should use node image, Gitlab project link, master branch and my Gitlab account.

对于我们的react demo项目，我们用到的是node镜像，Gitlab项目链接，master分支和我的Gitlab账号

Firstly, we need create Gitlab credential on Openshift. Click ```Resources``` -> ```Secret```

首先我们需要创建Gitlab账号。点击```Resources``` -> ```Secret```

[![See react](assets/img/cicd/example23.png)](assets/img/cicd/example23.png)

Secondly, select ```create secret``` button

第二步，选择```create secret```按钮

[![See react](assets/img/cicd/example24.png)](assets/img/cicd/example24.png)

Thirdly, input related info and then click ```create``` button

第三步，输入相关信息并点击```create```按钮

[![See react](assets/img/cicd/example25.png)](assets/img/cicd/example25.png)

Fourthly, Click ```Builds``` --> ```Builds```

第四步，选择```Builds``` --> ```Builds```

[![See react](assets/img/cicd/example26.png)](assets/img/cicd/example26.png)

Fifthly, click ```Add to project``` --> ```Browse Catalog```

第五步，选择```Add to project``` --> ```Browse Catalog```

[![See react](assets/img/cicd/example27.png)](assets/img/cicd/example27.png)

Sixthly, select the image we want

第六步，选择我们的镜像node

[![See react](assets/img/cicd/example28.png)](assets/img/cicd/example28.png)

Seventhly, click ```Next``` button

第七步，点击```Next```按钮

[![See react](assets/img/cicd/example29.png)](assets/img/cicd/example29.png)

Eighthly, input application name (recommend same as Gitlab project name) and git repository link

第八步，输入应用名（建议使用和Gitlab项目一样的名字）和代码仓库链接

[![See react](assets/img/cicd/example31.png)](assets/img/cicd/example31.png)

Note: Please copy and paste the ```http``` one, not ```ssh``` one

注意：这个需要复制粘贴```http```那个链接，而不是```ssh```那个

[![See react](assets/img/cicd/example30.png)](assets/img/cicd/example30.png)

Ninthly, click ```advance option```

第九步，点击```advance option```
[![See react](assets/img/cicd/example32.png)](assets/img/cicd/example32.png)

Tenthly, input branch name and select the secret you create on step 3. Then Click ````Create```` button

第十步，输入分支名并选择你第三步创建的secret。然后点击```创建```按钮
[![See react](assets/img/cicd/example33.png)](assets/img/cicd/example33.png)

Eleventhly, go back to builds page. Waiting for the status become ```complete```

第十一步，返回到builds页面，等待状态变为```complete```

[![See react](assets/img/cicd/example34.png)](assets/img/cicd/example34.png)

[![See react](assets/img/cicd/example35.png)](assets/img/cicd/example35.png)

⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️⬇️

[![See react](assets/img/cicd/example36.png)](assets/img/cicd/example36.png)

### Deploy


Twelvethly, go to ```Applications``` --> ```Pods``` page

第十二步，前往```Applications``` --> ```Pods```页面

[![See react](assets/img/cicd/example38.png)](assets/img/cicd/example38.png)

Thirdteenly, the status will update from ```Container creating``` to ```running```. The name will update from ```DOCKER-NAME-deploy``` to ```DOCKER_NAME—NUMBER```. It means the service can be run normally on Openshift now.

第十三步，这个状态会由```Container creating``` 慢慢变成 ```running```,名字会由```DOCKER-NAME-deploy``` 变成 ```DOCKER_NAME—NUMBER```,这个时候服务就可以正常跑起来了.

[![See react](assets/img/cicd/example39.png)](assets/img/cicd/example39.png)

### View Log
Click ```Applications``` --> ```Pods``` then select your project (format: ```project name - build order - random number```). Choose ```Logs``` tab and then you can view the logs.

点击 ```Applications``` --> ```Pods，选择你的项目（格式是：``````项目名 - build次序号 - 随机数```）。选择```Logs```选项卡就可以看到log了。

  [![See react](assets/img/cicd/example56.png)](assets/img/cicd/example56.png)

### Environment Variable / Config Map
For some services, we have different configs for different environments. So we need cover the config on git and use the related config to setup environment. There are 2 kinds of config on Openshift:

对于很多服务来说，我们在不同环境会有不同的配置，所以我们需要覆盖掉git上面的配置来设置环境。在Openshift中，我们有两种配置设置方法：

- Environment Variable

  Environment Variable support key-value. For some old projects we used environment variable, such as ```ease-api```. Go to ```Applications``` --> ```Deployments``` and then select your project. Switch to Environment tab and then you can view and edit environment variables. 

  Environment Variable支持键值对。在一些旧项目中我们都使用environment variable，例如```ease-api```。 前往```Applications``` --> ```Deployments```选择你的项目。切换到Environment选项卡，你就可以查看和编辑environment variables。

  [![See react](assets/img/cicd/example40.png)](assets/img/cicd/example40.png)
  <br/>
  [![See react](assets/img/cicd/example41.png)](assets/img/cicd/example41.png)

- Config Map

  Config Map support json format and file. Recommend that using config map because it's more flexible. Click ```Resources``` -> ```Config Maps``` and then click ```Create Config Map``` button on the top right.

  Config Map支持json格式和文件，推荐用config map因为它用起来比较灵活。 点击```Resources``` -> ```Config Maps```然后点击右上角的```Create Config Map```按钮

[![See react](assets/img/cicd/example42.png)](assets/img/cicd/example42.png)

[![See react](assets/img/cicd/example43.png)](assets/img/cicd/example43.png)

  Input the name, key and value. 
  - Name is the config map name, recommend a meaning name. 
  - Key is the config file name on the project. 
  - Value is the current environment value inside the file.

  输入config map的名字，key，值。
   - 名字就是自定义的一个config map的名字，取个有意义而且好认的名字。
   - key就是在项目当中这个文件的名字。
   - 值就是这个文件里面的内容

[![See react](assets/img/cicd/example45.png)](assets/img/cicd/example45.png)

Now the config map is ready. But it doesn't belongs to any projects. We should make a connection for them. Click ```Applications``` -> ```Deployments``` and select your project. Choose ```configuration``` tab then click ```Add config files``` button.

现在config map设置好了，但是它并不属于任何的项目，我们应该设置一个关联。点击```Applications``` -> ```Deployments```然后选择你的项目。进入到```configuration```并点击```Add config files```按钮。

[![See react](assets/img/cicd/example40.png)](assets/img/cicd/example40.png)

[![See react](assets/img/cicd/example48.png)](assets/img/cicd/example48.png)

Select the config map you created just now from the dropdown list. Input the Mount Path as format ```/opt/app-root/src/ + YOUR CONFIG FOLDER```.  Finally click ```Add``` button to confirm.

从下拉框选择你刚刚创建到config map，输入绑定的路径，格式为```/opt/app-root/src/ + 配置文件目录```. 最后点击```Add```按钮确认。

[![See react](assets/img/cicd/example49.png)](assets/img/cicd/example49.png)

Note: ```/opt/app-root/src/``` is located to your project root folder. 

注意：```/opt/app-root/src/```是固定定位到你的项目根目录的。

For example, if your project code structure is just like this. Then the mount path should be ```/opt/app-root/src/config```.

例如，你的项目代码结构是这样的，那么这个绑定的路径就是```/opt/app-root/src/config```.

[![See react](assets/img/cicd/example50.png)](assets/img/cicd/example50.png)

### Restart service

If you want to restart service without change code. You can go to ```Applications``` -> ```Deployments``` and select your project. Then click ```Deploy``` button on the top right of the page to restart service.

如果你想在不改代码的时候重启服务。你需要点击```Applications``` -> ```Deployments```，选择你的项目，然后点```Deploy```按钮去重启服务

[![See react](assets/img/cicd/example40.png)](assets/img/cicd/example40.png)

[![See react](assets/img/cicd/example51.png)](assets/img/cicd/example51.png)

### Set Port
Openshift service port default is 8080. If you want to update port for one of the projects. You need click ```Applications``` -> ```Services``` and select your project. Click ```Actions``` --> ```Edit YAML``` button on the top right of the page to go to setting page.

Openshift的服务默认端口是8080，如果你想改一个项目的端口，你需要点击```Applications``` -> ```Services```，选择你的项目，点击右上角的```Actions``` --> ```Edit YAML```按钮进入到设置页面。

[![See react](assets/img/cicd/example52.png)](assets/img/cicd/example52.png)

Update these 3 numbers to your target port number. Click ```Save``` to save updated port.

更新这三个数字，令它等于你的目标端口号。 点击```Save``` 保存更改。

[![See react](assets/img/cicd/example53.png)](assets/img/cicd/example53.png)

### Set Route
Openshift give each services as default link. You can use this link to access service. But the url is ulgy and we need custom set route for it. Click ```Applications``` -> ```Routes``` and select your project.Click ```Actions``` --> ```Edit YAML``` button on the top right of the page to go to setting page.

Openshift给每个服务一个默认的路由，你可以使用这个url去访问服务，但是这个默认的地址太丑了，我们需要帮它写一个自定义的路由。 点击```Applications``` -> ```Routes```并选择你的项目，点击右上角的```Actions``` --> ```Edit YAML```按钮进入到设置页面。

[![See react](assets/img/cicd/example54.png)](assets/img/cicd/example54.png)

Update the string with new route on red circle. Click ```Save``` to save updated port.

把新路由更新到红框的地方。点击```Save``` 保存更改。

[![See react](assets/img/cicd/example55.png)](assets/img/cicd/example55.png)

Note: Make sure your first level domain is DNS registered on IT department. Point this domain to Openshift IP. Please ask [Alex Tse](https://teams.microsoft.com/l/chat/0/0?users=alex.tse@eabsystems.com.hk)/[James Ng](https://teams.microsoft.com/l/chat/0/0?users=james.ng@eabsystems.com.hk) of IT department for more info

注意：要留意这个一级域名需要在IT部门注册DNS，将这个域名指向Openshift的IP. 更多的信息请联系IT部门的Alex或者James

### Pod scale up / scale down
When you want to shut down the service or add pos numbers to fit load balance. You can click ```Overview``` on the left menu. Use arrows on your project up and down to control it.

当你想关闭服务，或者增加pod的数量来做负载均衡，你可以点击左边菜单的```Overview```。利用你项目的上下箭头去控制。

[![See react](assets/img/cicd/example60.png)](assets/img/cicd/example60.png)

## Auto trigger build service by Gitlab CI

There is a ```Webhook``` url for the third party service to call. When calling this url, the build will be triggered.

有一个```Webhook``` url给第三方服务来调用，当调用这个url的时候，这个build就会自动被触发。

How to get this ```Webhook``` url? Click ```Builds``` --> ```Builds``` and then select your project. Switch to ```Configuration``` tab and then you can see the ```Generic Webhook URL```. Let's copy this url.

怎么获取这个```Webhook``` url？ 点击```Builds``` --> ```Builds```然后选择你的项目。切换到```Configuration```选项卡，然后就可以看到这个```Generic Webhook URL```了。我们复制这个url。

[![See react](assets/img/cicd/example57.png)](assets/img/cicd/example57.png)

Let's use Postman to test firstly. Paste this url to Postman and select method as ```POST```. Click ```Send``` button. When you receive 200 status, it means you have triggered Openshift build successfully.

让我们先试试用Postman。把url粘贴到Postman中，选择POST方法，然后点Send按钮。当你收到200的状态响应，就意味着你成功的触发了Openshift去build。

[![See react](assets/img/cicd/example58.png)](assets/img/cicd/example58.png)

Then we go back to Openshift Builds --> Builds, you can see a new build created.

回到Openshift的Builds --> Builds，你就会看到一个新的build

[![See react](assets/img/cicd/example59.png)](assets/img/cicd/example59.png)

When developer branch merge into major branch, we need do a auto deploy action on Gitlab CI. We can use curl to POST this url to fit this requirement. After this, the whole CI/CD process is finish.

在Gitlab CI当中，当开发分支合并进主分支之后，我们需要做一个自动部署的行为，我们可以利用这个url，利用curl做一次POST请求，即可完成这个需求，同时也打通了CICD的整个过程。

```
image: node:12

stages:
  - check

check:
  stage: check
  script:
    - npm i
    - npm run test

deploy:
  stage: deploy
  script:
    - curl -X POST --insecure "https://vm-OpenShift-POC-1:8443/oapi/v1/namespaces/reactdemo/buildconfigs/react-demo/webhooks/caff14f5ed7f3e59/generic"
  only:
      - master
```

## Use k8s + docker instead of Openshift
For EASE, we are using Openshift as docker manage platform. But for new projects and innovation projects, we use ```k8s + docker``` to mange docker because ```docker + k8s``` has more guys to use and the community is larger. 

对于EASE，我们使用Openshift进行docker的管理，但对于新项目或者创新项目，我们使用```k8s+docker```的形式，因为```docker+k8s```有更多的人在使用，并且社区也大过Openshift。

If you are using ```docker + k8s```, you need define ```Dockerfile``` just like define ```.s2i``` script.

如果你使用```docker + k8s```, 你需要像```.s2i```一样定义```Dockerfile```

About how to setup Dockerfile, where is the docker registry, how to setup k8s environment, please ask [Alex Tse](https://teams.microsoft.com/l/chat/0/0?users=alex.tse@eabsystems.com.hk)/[James Ng](https://teams.microsoft.com/l/chat/0/0?users=james.ng@eabsystems.com.hk) of IT department

关于如何写Dockerfile，docker仓库的位置，如果设置k8s环境，请咨询IT部门的[Alex Tse](https://teams.microsoft.com/l/chat/0/0?users=alex.tse@eabsystems.com.hk)/[James Ng](https://teams.microsoft.com/l/chat/0/0?users=james.ng@eabsystems.com.hk)

## References
- React Demo Gitlab Link: http://192.168.225.151/kevin.liang/reactdemo
- React Demo Openshift Link: https://vm-openshift-poc-1:8443/console/project/reactdemo/browse/builds














