---
id: cicd-introduction
title: CI/CD Introduction 持续集成与持续交付介绍 
sidebar_label: CI/CD Introduction 持续集成与持续交付介绍
---
| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |

## Concept 概念
[![cicdSummary](assets/img/cicd/cicdSummary.png)](assets/img/cicd/cicdSummary.png)

### Continuous Integration 持续集成
After developer check in code, execute build and (unit) test immediately. We can confirm if the new code is merged correctly with previous code according to the test result.

持续集成强调开发人员提交了新代码之后，立刻进行构建、（单元）测试。根据测试结果，我们可以确定新代码和原有代码能否正确地集成在一起。
[![continuousIntegration](assets/img/cicd/continuousIntegration.jpg)](assets/img/cicd/continuousIntegration.jpg)

### Continuous Delivery 持续交付
Base on Continuous Integration, we deploy the code after merging to production-like environments. For example, after finish unit test, we can deploy our code to stage environment which is connected to database. If no issues on code, we can deploy to production environment manually.

持续交付在持续集成的基础上，将集成后的代码部署到更贴近真实运行环境的「类生产环境」（production-like environments）中。比如，我们完成单元测试后，可以把代码部署到连接数据库的 Staging 环境中更多的测试。如果代码没有问题，可以继续手动部署到生产环境中。
[![continuousDelivery](assets/img/cicd/continuousDelivery.jpg)](assets/img/cicd/continuousDelivery.jpg)

### Continuous Deployment 持续部署
Base on Continuous Delivery, we set deploy to production stage as automatic action.

持续部署则是在持续交付的基础上，把部署到生产环境的过程自动化。
[![continuousDeployment](assets/img/cicd/continuousDeployment.jpg)](assets/img/cicd/continuousDeployment.jpg)


## Reference Link 参考链接
### English
- [What is CICD — Concepts in Continuous Integration and Deployment](https://medium.com/@nirespire/what-is-cicd-concepts-in-continuous-integration-and-deployment-4fe3f6625007)

- [Introduction to CI/CD with GitLab](https://docs.gitlab.com/ee/ci/introduction/)

- [A Brief Introduction to CI/CD](https://dzone.com/articles/the-complete-introduction-to-cicd-1)

### 中文
- [如何理解持续集成、持续交付、持续部署？](https://www.zhihu.com/question/23444990)

- [谈谈持续集成，持续交付，持续部署之间的区别](https://www.jianshu.com/p/2c6ebe34744a)

- [详解持续集成是什么 持续交付、持续部署、流程](https://cloud.tencent.com/developer/article/1465310)
