---
id: mask-import-production-data
title: Mask and Import Production Data
sidebar_label: Mask and Import Production Data
---
<strong>This tool is located at the EASE-WEB Repository (tools/maskProductionData)</strong>

![Tools's Location](assets/img/ease_web/tools/ease-web-tools01.png)

##  Prerequisites
-   Node & Java Installed

## Mask Production Data
-   Put all extracted data in the folder "originalFiles"
-   Change the directory to "tools/makProductionData"
-   In the terminal run "node index.js"
-   All sensitive fields will be masked and created "updateDocs.txt" in the folder "maskedFiles"

## Import Production Data
-   Change the directory to "maskedFiles"
-   In the terminal run below scripts
```sh
java -jar CB_importer.jar 
Sync Gateway IP & Port: 192.168.222.159:4985
Database: axasg_dev
200 - OK
Config file name: updateDocs.txt
```
The above example will import the masked documents to DEV environment