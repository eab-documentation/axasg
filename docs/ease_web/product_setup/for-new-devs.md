---
id: for-new-devs
title: For New EAB Developers
sidebar_label: Before Jumping In
---

## Hello New EAB Devs!

There are a few things you need to know before you start working on the Product Configurator/121 Admin and EASE!

- Product Configurator's code editor doesn't accept the comment syntax, `//hello world`but only, `/*hello world*/`
- Only use the `quotDriver.runFunc()` in the Product Configurator sparingly. Declare code within the illustrateFunc as much as possible. This function
  can slow down the policy creation.
- Remove your logs from the console since they slow down the proposal creation time
- Before using your new local environment, you need to update your server.js and set the field, “secure”, from true to false

## How to use the Debugger

- Copy the link on the console and paste it on your favored browser
- Go to the sources tab then go to QuotDriver.js
- If you want to find the error relating to the product and its calculations u can:
    - Create a breakpoint on the line `let fn = eval('(' + fnStr + ')');`
    - Right click on the breakpoint and choose _“edit breakpoint”_
    - If you want to find the values in illusFunc, make sure you have an expression such as `fnStr.indexOf("Column AE") > -1`
    - Hit Play until breakpoint stops, then you can press the icon "jump into function call" so you can see the values within the IllusFunc
