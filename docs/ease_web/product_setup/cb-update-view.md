---
id: cb-update-view
title: Product Setup
sidebar_label: Product Setup
---

## Location to setup product (EASE-ADMIN - Product Configurator)

### 1. Create a new product
- In Products -> New Product

![Update View Location](assets/img/ease_web/productSetup/product-setup-01.png)

- Select radio button depending on whether its product/ rider, fill in name and product code

![Update View Location](assets/img/ease_web/productSetup/product-setup-02.png)

- Clone from other version to clone exactly from other products setting

![Update View Location](assets/img/ease_web/productSetup/product-setup-03.png)

- Fill in basic product setting info, eg: product code, type, launch and expiry date etc.

![Update View Location](assets/img/ease_web/productSetup/product-setup-04.png)

- In release tab, create a new release to publish product to couchbase db

![Update View Location](assets/img/ease_web/productSetup/product-setup-05.png)

- Add the newly created product.

![Update View Location](assets/img/ease_web/productSetup/product-setup-06.png)

- Can add policy option in policy option tab, eg – death benefit

![Update View Location](assets/img/ease_web/productSetup/product-setup-07.png)

- The options basic & enhanced reflected in UI after publish.

![Update View Location](assets/img/ease_web/productSetup/product-setup-08.png)

- Upload rates table – Rates for quotation

![Update View Location](assets/img/ease_web/productSetup/product-setup-09.png)

- Input the ID of the rate table

![Update View Location](assets/img/ease_web/productSetup/product-setup-10.png)

- Copy/ Input rate table from excel requirement and paste into the table.

![Update View Location](assets/img/ease_web/productSetup/product-setup-11.png)

- Update the formula to handle cases not handle by other tab

![Update View Location](assets/img/ease_web/productSetup/product-setup-12.png)

- Eg: illustrateFunc – to prepare PI data 

![Update View Location](assets/img/ease_web/productSetup/product-setup-13.png)

- Add rider to product 

![Update View Location](assets/img/ease_web/productSetup/product-setup-14.png)

- Select from rider list.

![Update View Location](assets/img/ease_web/productSetup/product-setup-15.png)

- Added

![Update View Location](assets/img/ease_web/productSetup/product-setup-16.png)

- Add, change or remove fund.

![Update View Location](assets/img/ease_web/productSetup/product-setup-17.png)

- Choose from dropdown 

![Update View Location](assets/img/ease_web/productSetup/product-setup-18.png)

- Add/ delete pdf attachement – eg – product summary, fund info booklet

![Update View Location](assets/img/ease_web/productSetup/product-setup-19.png)

- Add- select from local

![Update View Location](assets/img/ease_web/productSetup/product-setup-20.png)

- Add, delete or change pdf template used by BI

![Update View Location](assets/img/ease_web/productSetup/product-setup-21.png)

- After done product setting,save. Back to releases, add product to release and publish.

![Update View Location](assets/img/ease_web/productSetup/product-setup-22.png)

### 2. New/ Modify Funds 

- Add/ delete/ modify Fund

![Update View Location](assets/img/ease_web/productSetup/product-setup-23.png)

- Input all the settings

![Update View Location](assets/img/ease_web/productSetup/product-setup-24.png)

- Delete then add again to update the product highlight sheet

![Update View Location](assets/img/ease_web/productSetup/product-setup-25.png)

- Save/ delete to save/ delete this fund

![Update View Location](assets/img/ease_web/productSetup/product-setup-26.png)

- click on checkbox which fund which are needed to update, then export it.

![Update View Location](assets/img/ease_web/productSetup/product-setup-27.png)

- exported fund json and product highlight sheet pdf in folder, import using cbimporter.

![Update View Location](assets/img/ease_web/productSetup/product-setup-28.png)

### 3. New/ Modify Template

- In left pane, pdf - New template -> input info

![Update View Location](assets/img/ease_web/productSetup/product-setup-29.png)

- Clone from other item and modify based on other existing template. 

![Update View Location](assets/img/ease_web/productSetup/product-setup-30.png)

- Input basic detail such as code so that it can be recognised by product. 

![Update View Location](assets/img/ease_web/productSetup/product-setup-31.png)

- In pages, update in page to give the PI the required structure, eg, wordings and table are done here by modifying the html.

![Update View Location](assets/img/ease_web/productSetup/product-setup-32.png)

- PI structure in complemented by css applied. –eg- font size, column width, spacing width and height etc. 

![Update View Location](assets/img/ease_web/productSetup/product-setup-33.png)

- Update formulate to prepare displayed data, eg: number of rows to be displayed, dynamically generate data such as age, name, premium etc.

![Update View Location](assets/img/ease_web/productSetup/product-setup-34.png)

- Add images – eg: axa logo

![Update View Location](assets/img/ease_web/productSetup/product-setup-35.png)

- To publish to db, back to release and add template into the relase to publish.

![Update View Location](assets/img/ease_web/productSetup/product-setup-36.png)

## Location to setup product (EASE-WEB - EAPP)
### 4. Application Form Mapping

- Map product code to eapp section and eapp form template <b>(application_form_mapping.json)</b>.

```sh
	"HER": {
    "eFormSections": [{
        "items": [{
            "form": "appform_dyn_personal",
            "id": "personal",
            "title": {
              "en": "Personal Details"
            }
          },

          {
            "form": "appform_dyn_residency",
            "id": "residency",
            "title": {
              "en": "Residency & Insurance Info"
            }
          },

          {
            "form": {
              "la": {
                "HER": "dyn_la_insurability_her"
              }
            },
            "id": "insurability",
            "title": {
              "en": "Insurability Information"
            }
          }
        ]

      },

      {
        "items": [{
          "form": "appform_dyn_plan",
          "id": "plan",
          "title": {
            "en": "Plan Details"
          }
        }]
      },

      {
        "items": [{
          "form": "appform_dyn_declaration_him",
          "id": "declaration",
          "title": {
            "en": "Declaration"
          }
        }]
      }
    ],

    "appFormTemplate": {
      "main": "appform_report_main",
      "template": [
        "appform_report_common",
        "appform_report_personal_details",
        "appform_report_plan_details_sav",
        "appform_report_residency",
        "appform_report_insurability_her",
        "appform_report_declaration"
      ],
      "properties": {
        "dollarSignForAllCcy": false
      }
    }
	},
```

### 4. Email Mapping

- Map product code to necessary sections in below 4 json.
    1. eApp_email_attachments.json
```sh
    {
      "productCode": ["SAV", "BAA", "TPX", "TPPX", "LMP", "ESP", "RHP","HIM","HER", "LITE", "NPE", "ESC", "ART2", "SPEN"],
      "attKeys": [
        {
          "id": "fna",
          "title": "Financial Needs Analysis"
        },
        {
          "id": "bi"
        },
        {
          "id": "prodSummary",
          "title": "Product Summary of the accepted plan(s)"
        }
      ]
    },
```

   2. eApp_fa_email_attachments.json
```sh
  "items": [
    {
      "productCode": ["SAV", "BAA", "TPX", "TPPX", "LMP", "ESP", "RHP","HIM","HER", "LITE", "NPE", "ESC", "ART2"],
      "attKeys": [
        {
          "id": "bi"
        },
        {
          "id": "prodSummary",
          "title": "Product Summary of the accepted plan(s)"
        }
      ]
    },
```

  3. eSubmission_email_template_fa.json
```sh
    {
      "productCode": ["IND", "AWT", "PUL", "AWICA", "AWICP", "AWTR"],
      "emailContent": {
        "clientSubject": "AXA Insurance (Proposal number: {{policyNumber}})",
        "clientContent": "<div>Dear {{clientTitle}} {{clientName}},</div><br><div>Thank you for your interest in the AXA suite of products.</div><br><div>Attached is/are document(s) for your reference.</div><br><div><p style='margin-left: 40px'>1)   Policy Illustration Document(s)</p><p style='margin-left: 40px'>2)   Product Summary of ",
        "embedImgs": {
          "axaLogo": "iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAFxGAABcRgEUlENBAAAAB3RJTUUH4QcRCw8WNFDIEgAADghJREFUeNrtmnlwVFXaxn/",
        }
      },
      "attachmentKeys":["bi", "prodSummary", "phs", "fib", "eCpd"]
    },
```

   4. eSubmission_email_template.json
```sh
   "eSubmissionEmails": [
    {
      "productCode": ["SAV", "LMP", "ESP", "RHP", "LITE", "NPE", "ART2", "SPEN"],
      "emailContent": {
        "clientSubject": "AXA Insurance (Proposal number: {{policyNumber}})",
        "clientContent": "<div>Dear {{clientTitle}} {{clientName}},</div><br><div>Thank you for your interest in the AXA suite of products.</div><br><div>Attached is/are document(s) for your reference.</div><br><div><p style='margin-left: 40px'>1)   Financial Needs Analysis</p><p style='margin-left: 40px'>2)   Policy Illustration Document(s)</p><p style='margin-left: 40px'>3)   Product Summary of the accepted plan(s)</p><p style='margin-left: 40px'>4)   Your Guide To Life Insurance (http://www.lia.org.sg/consumers/library/consumer_guides)</p><p style='margin-left: 40px'>5)   Your Guide to Participating Insurance Plans (http://www.lia.org.sg/consumers/library/consumer_guides)</p></div><br><div>The attached document(s) is/are protected with password for security purpose.</div><br><div>The password is your date of birth (DDMMMYYYY).</div><br><div>Example:01FEB1980</div><br><div>This is an auto-generated email message. Please do not reply to this email account.</div><br><div>If you have any enquiries, your Financial Consultant, {{agentName}} will be happy to be of assistance. {{agentName}} can be contacted at {{agentContactNum}}</div><br><div>AXA Insurance Pte Ltd</div><br><img src=\"cid:axaLogo\">",
        "agentSubject": "AXA Insurance (Proposal number: {{policyNumber}})",
        "agentContent": "<div>Dear {{agentTitle}}{{agentName}},</div><br><div>Thank you for submitting {{policyNumber}} for {{clientName}} on {{submissionDate}}. This case is currently pending your Supervisor’s approval.</div><br><div>The following documents have been sent to your client:</div><br><div><p style='margin-left: 40px'>1)   Financial Needs Analysis</p><p style='margin-left: 40px'>2)   Policy Illustration Document(s)</p><p style='margin-left: 40px'>3)   Product Summary of the accepted plan(s)</p><p style='margin-left: 40px'>4)   Your Guide To Life Insurance (http://www.lia.org.sg/consumers/library/consumer_guides)</p><p style='margin-left: 40px'>5)   Your Guide to Participating Insurance Plans (http://www.lia.org.sg/consumers/library/consumer_guides)</p></div><br><div>This is an auto-generated email message. Please do not reply to this email address.</div><br><div>If you have any enquiries with regards to the submitted case, you could contact the support line 90237577 or email to digitalhelpdesk@axa.com.sg</div><br><div>AXA Insurance Pte Ltd</div><br><img src=\"cid:axaLogo\">",
        "embedImgs": {
          "axaLogo": "iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAFxGAABcRgEUlENBAAAAB3RJTUUH4QcRCw8WNFDIEgAADghJREFUeNrtmnlwVFXaxn/33t7T2bo7CwkhJMhmCEsIEIwJm8uAA1oFuFRNUYCDNYo4jDACUgIfI+gojgUDX4oPS6SQD6ZKp1w+ZTJExQCC7AkhEAhbWLMQsnQ6nd7u90c3F5ruQDYdINyq/JFzTr/n3Oe8z3Pe9z1XgA9lHjyAmr"
        }
      },
      "attachmentKeys":["bi", "prodSummary", "fna", "eCpd"]
    },
```

### 5. Payment Mapping

- Map product code to payOptions.json to add payment method, eg: cash, credit card, srs etc.

```sh
    {
      "covCode": "ART2",
      "paymentMethod": "cash",
      "paymentMode": ["S", "Q", "M", "L"],
      "payOptions": ["crCard", "eNets", "axasam", "chequeCashierOrder", "cash", "teleTransfter"]
    },
    {
      "covCode": "ART2",
      "paymentMethod": "srs",
      "paymentMode": ["A", "S", "Q", "M", "L"],
      "payOptions": ["srs"]
    }
```
### 6. Commit
- Export json using product_exporter.js 
```sh
    node product_exporter.js plan {planCode}  (without pdf template json)
    node product_exporter.js basicPlan {planCode}  (with pdf template json)
```
- Update product txt if json is not in product txt, eg: when create a new product or new version of an existing product.
```sh
    node product_exporter.js list {planCode}  
```