---
id: ease-web-installation
title: Local Installation 配置本地环境
sidebar_label: Local Installation 配置本地环境
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 17 April 2020 | Init | Kevin Liang |

## Before start

1.	Make sure you have java SDK 8 installed. JRE or other version of SDK may not work.
2.	Make sure you have python 2.7.x installed. (Only Windows OS)
3.  Make sure you have node v7.10.0.
4.	Make sure your account has admin right, it might now work on npm install or wired things happen.
5.	If npm install generate many dependency errors, please use yarn.

## 开始之前
1. 确保你安装了java SDK 8. JRE或者其他版本的SDK用不了
2. 确保你安装了python v2.7.x (只是Windows系统需要)
3. 确保你安装了node v7.10.0
4. 确保你的账户有管理员权限。不然的话，可能刚开始可以工作，但后面就会有奇怪的事情发生。
5. 如果在安装过程中有很多库错误，请使用yarn
 
##	Setup DEV environment.
1.	Open cmd run as administrator

以管理员模式打开命令行
[![Run as administrator](assets/img/ease_web/setup/run_as_admin.png)](assets/img/ease_web/setup/run_as_admin.png)

2.	Go to source root

去到根目录
[![Go to source root](assets/img/ease_web/setup/go_to_source_root.png)](assets/img/ease_web/setup/go_to_source_root.png)
 
3.	Execute ```npm install -g windows-build-tools --vs2015``` （windows OS Only)

执行```npm install -g windows-build-tools --vs2015``` (仅Windows系统才需要这一步)
[![Install windows build tools](assets/img/ease_web/setup/install_windows_build_tools.png)](assets/img/ease_web/setup/install_windows_build_tools.png)
 
4.	Check the version of Microsoft Visual Studio with ```npm config get msvs_version```. If it is not 2015, set to use 2015 with ```npm config set msvs_version 2015 --global```

使用```npm config get msvs_version```命令来检查Visual Studio的版本。如果不是2015版，用命令```npm config set msvs_version 2015 --global```设置到2015版
[![Check Version of MSVS Version](assets/img/ease_web/setup/check_version_of_msvs_version.png)](assets/img/ease_web/setup/check_version_of_msvs_version.png)
 
5.	Execute ```npm install``` under source root

在根目录执行```npm install```
[![npm install](assets/img/ease_web/setup/npm_install.png)](assets/img/ease_web/setup/npm_install.png)
 
6.	Edit ```server.js```, change secure flag to ```false```

编辑```server.js```，设置https标示为```false```
[![Update server.js](assets/img/ease_web/setup/update_server_js.png)](assets/img/ease_web/setup/update_server_js.png)
 
7.	Execute ```npm run dev``` in cmd

在命令行执行 ```npm run dev```
[![npm run dev](assets/img/ease_web/setup/npm_run_dev.png)](assets/img/ease_web/setup/npm_run_dev.png)
 
8.	Open ```http://localhost:3000```

打开浏览器```http://localhost:3000```
[![Running](assets/img/ease_web/setup/running.png)](assets/img/ease_web/setup/running.png)
 

