---
id: update_product
title: Update Product
sidebar_label: Upgrade to Latest Version
---

## Update Product Version

### 1. Create a new release

- Go to Product Configurator and Create New Release
- Choose to clone from the latest release
- If u want to change/add products such as the product itself/pdf then:
    - clone from the latest version of said pdf/product by clicking "clone latest version"
    -  add to the description e.g. release17/CRXXX
    -  make changes to the latest version
- After making your changes to the latest version, SAVE 
- Add pdf/product to latest release version
- Publish.

### 1a. Updating a Product's PDF 

Sometimes you may need to update the Product Summary or the Fund Info Booklet or even the thumbnail of a Product so it can be updated on EASE. 

- First, upload those files in the latest release of the product in 121 Admin. 

![Update View Location](assets/img/ease_web/updateProduct/update_product2.png)

- Save the your changes then hit Publish for that release.
- Change directory to CB_resources in your local project then enter this on the cmd: `node product_export.js basicPan < planName >`
- Commit only those files/pdf's that need to be committed on SourceTree 
- Push and create a Merge Request on GitLab 

### 2. Committing the new release

-	go to root folder in local machine then change directory to CB_resources
-	Type on the cmd: `node product_export.js basicPan < planName >`
-	Because it's a new release, so the products' versions will also change, go to products.txt file in your project
-   Click Export on the latest release 
-   Add the newly exported files to your product folder in your local environment
-   Update the products.txt for your new release,making sure that the mappings for the new product maps to the files you just added to the products folder

![Update View Location](assets/img/ease_web/updateProduct/update_product1.png)

-	You will see all your changes up to this point in source tree’s unstaged files
-	Commit the newly added product json's and pdf json's AND the products.txt

### 2a. Updating Release Versions on Templates

- In your code editor, do a file search for all json files in your project that are named with sysParameter and change release version, same goes with iOS

### 3a. Masking Funds in Download Material and on EASE

- For EASE, just go to 121 Admin and delete the Fund from the list in the Attachments Tab
- For Download Materials, just go to the Materials.txt in your local environment and find the mapped json file to that certain Fund Code and change the expiry date 

![Update View Location](assets/img/ease_web/updateProduct/update_product3.png)

- After making changes, enter this: `java -jar cb_importer.jar` so that your changes can be imported into Couchbase
- Commit and Push
- You can test your changes by going to the local build and checking download materials if it is shown or not