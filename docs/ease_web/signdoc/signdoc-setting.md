---
id: signdoc-setting
title: SignDoc Setting
sidebar_label: SignDoc Setting
---
## OpenShift Config-Map Setting for EASE-WEB
![config-map](assets/img/ease_web/signdoc/signdoc-setting-01.png)

This is the config-map for EASE-WEB

<u><strong>Here is the link to access SIT 1 EASE-WEB Config-Map</strong></u>
-   https://admin.ocp.eabdc:8443/console/project/axasgsit/browse/config-maps/ease-web-config

<u><strong>Example of the config-map for EASE-WEB</strong></u>
```json
{
    ...
  "signdoc": {
    "host"        : "ease-sit1.intraeabdc",
    "postUrl"     : "https://ease-sit1.intraeabdc/sdweb/load/byurl" ,
    "getPdf"      : "http://ease-web:3000/getAttachmentPdf" ,
    "resultUrl"   : "https://ease-sit1.intraeabdc/getSignDocResult" ,
    "dmsid"       : "de.softpro.sdweb.plugins.impl.ServletDms"
  }
}
```

"signdoc.host", "signdoc.postUrl", "signdoc.resultUrl"
These fields needs to match with EASE-WEB Domain (ease-sit1.intraeabdc)

<strong>Reminder:</strong>
- Cannot put localhost / 127.0.0.1 in the "signdoc.host", "signdoc.postUrl", "signdoc.resultUrl"

## OpenShift Route Setting
Reference: https://admin.ocp.eabdc:8443/console/project/axasgsit/browse/routes/signdoc-route

Here is the example to create signdoc route, this enable the communication between EASE-WEB and SignDoc
![Signdoc Route](assets/img/ease_web/signdoc/signdoc-setting-02.png)

![Signdoc Route](assets/img/ease_web/signdoc/signdoc-setting-03.png)