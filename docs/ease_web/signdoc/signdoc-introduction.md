---
id: signdoc-introduction
title: SignDoc Introduction
sidebar_label: SignDoc Introduction
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |

Hand over by Alex Tang

## Work Flow Image
[![Sign Doc Work Flow](assets/img/ease_web/signdoc/signdoc-introduction1.png)](assets/img/ease_web/signdoc/signdoc-introduction1.png)

## Work Flow
### Step1
A. Web calls the following function to request information that is used for calling Signdoc Web server.

// in ApplicationHandler

getSignatureStatusFromCb

Then Node server will handle the request and prepare the following information:

| Parameter | Comment |
| :-: | :-: |
| docid | String. Application id + ‘_’ + attachment id |
| docurl | URL for Signdoc to get PDF file from node server. The URL only contains the token to retrieve file once in limited time |
| resulturl | URL for show any success / error message returned from Signdoc |
| auth | Token for authentication in posting request to Signdoc. Auth is generated with (i) docid, (ii) docts |
| docts | Current Time. Work with auth. |
| dmsid | Plugin for used for Signdoc to identify how to handle signed PDF |

[This includes Step 1 to 3 in the diagram]

### Step2

B.	After web received the above information from Node server, it will do a POST request to Signdoc Web Server.

[This includes Step 4 in the diagram]

### Step 3
C.	In Signdoc Web Server, it will get the PDF from Node server by using docurl

[This includes Step 5 to 8 in the diagram]

### Step 4
D.	After Signdoc Web Server receives PDF, the signature process can be started.

[This includes Step 9 in the diagram]

### Step 5
E.	When user signs all signature boxes in PDF and click “SAVE” button, Signdoc will send signed PDF to Node server by calling POST request on URL

// POST request called from Signdoc to Node server. This URL is set in sdweb_config.groovy

http://ease-web:3000/setAttachmentSigned

// in ApplicationHandler, the following function will handle the POST request

getSignedPdfFromSignDocPost

[This includes Step 10 to 12 in the diagram]

### Step 6

F.	After signed PDF is saved in couchbase, iFrame for Signdoc will reload with resultUrl. When web detects onLoad event with pathname = resultUrl, it will call Node server to prepare next PDF information

// in ApplicationHandler, the following function is called to provide next PDF info

getUpdatedAttachmentUrl

[This includes Step 13 to 15 in the diagram]

## Configuration

### Update Menu Icon (Enable / Disable)
Located in: sdweb_home/conf/mobile_configuration.xml

Update value for key “Visible”

```
<element id=”XXXX”>
  <parameter key=”Visible” value=”false” />
</element>
```

### Update wording
Located in: sdweb_home/conf/language.properties

### Update major config
Located in: sdweb_config.groovy

| Config | Comment |
| :-: | :-: |
| sdweb.external_server_url="https://ease-sit.intraeab/sdweb" | For setup. |
| sdweb.plugins.loadlist = ['de.softpro.sdweb.plugins.impl.ServletDms']<br/>sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.target_urls=["http://ease-web:3000/setAttachmentSigned"]<br/>sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.use_all_targets=true<br/>sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.createXMLFile=false | To specify how to handle the signed PDF after signing.<br/>target_urls will point to Node server so that Signdoc sends the signed PDF to these location|
| sdweb.authenticate.pluginid="de.softpro.sdweb.plugins.impl.BasicAuthenticator"<br/>sdwebplugins.de.softpro.sdweb.plugins.impl.BasicAuthenticator.enabled=false | For authentication, only POST request with auth and docts from EASE-Web can access. |
| sdweb.certificate.store.pkcs12.file="/var/softpro/sdweb_home/cert/cert_store.p12"<br/>sdweb.certificate.store.pkcs12.password="secret" | For certificate. |
| sdweb.usage.enable.aboutpage=false<br/>sdweb.usage.enable.loadpage=false | To disable the Signdoc About page ^ |
| sdweb.signature.display.signtime=true<br/>sdweb.defaults.signature.date.format="dd/MM/yyyy HH:mm:ss" | To disable the Signdoc About page ^ |

^ About page URL https://ease-sit.intraeab/sdweb/about?showlinks=true

## Additional
If signed PDF cannot be saved after clicking “SAVE”, please check the expiry date for Signdoc license.









