---
id: cb-update-view
title: Update View
sidebar_label: Update View
---

## Location to update Couchbase View (EASE-WEB Repository)
![Update View Location](assets/img/ease_web/couchbase/cb-updateview-01.png)

<u>Couchbase View has been seperated in different partition:</u>
-   application
-   applicationProdQery
-   approval
-   dataSync
-   invalidateViews
-   main    (All view queries are located in this partition)
...

Example to update the view
![Example](assets/img/ease_web/couchbase/cb-updateview-02.png)

- Step 1 Change the code
Original
```js
  signatureExpire: function (doc, meta) {
    if ((doc.type === 'application' && !doc.isSubmittedStatus && !doc.isInvalidated && doc.biSignedDate)
        || (doc.type === 'masterApplication' && !doc.isSubmittedStatus && !doc.isInvalidated && doc.biSignedDate)) {
      var lastUpdateDate = new Date(doc.biSignedDate);
      emit(["01", lastUpdateDate.getTime()], {
        appId: doc.id || doc._id,
        polNo: doc.policyNumber || '',
        bundleId: doc.bundleId,
        signDate: lastUpdateDate.getTime(),
        payMethod: doc.payment && doc.payment.trxMethod,
        payStatus: doc.payment && doc.payment.trxStatus,
        isFullySigned: doc.isFullySigned,
        isInitialPaymentCompleted: doc.isInitialPaymentCompleted,
        docType: doc.type
      });
    }
  }
```

Changed version
```js
  signatureExpire: function (doc, meta) {
    if ((doc.type === 'application' && !doc.isSubmittedStatus && !doc.isInvalidated && doc.biSignedDate)
        || (doc.type === 'masterApplication' && !doc.isSubmittedStatus && !doc.isInvalidated && doc.biSignedDate)) {
      var lastUpdateDate = new Date(doc.biSignedDate);
      emit(["01", lastUpdateDate.getTime()], {
        appId: doc.id || doc._id,
        polNo: doc.policyNumber || '',
        bundleId: doc.bundleId,
        signDate: lastUpdateDate.getTime(),
        payMethod: doc.payment && doc.payment.trxMethod,
        payStatus: doc.payment && doc.payment.trxStatus,
        isFullySigned: doc.isFullySigned,
        isInitialPaymentCompleted: doc.isInitialPaymentCompleted || doc.paymentNotComplete,
        docType: doc.type
      });
    }
  }
```

- Step 2 
    - npm run build-node (Details can refer to package.json in EASE-WEB)

- Step 3
    - After build process completed, run "node ViewGenerator.js"
    - This will generate new view JSON and are located below
```sh
Views export done. ./CB_resources/files/view_application.json
Views export done. ./CB_resources/files/view_applicationProdQery.json
Views export done. ./CB_resources/files/view_approval.json
Views export done. ./CB_resources/files/view_dataSync.json
Views export done. ./CB_resources/files/view_invalidateViews.json
Views export done. ./CB_resources/files/view_main.json
Views export done. ./CB_resources/files/view_mobile.json
Views export done. ./CB_resources/files/view_prod_bundle.json
Views export done. ./CB_resources/files/view_quotation.json
Views export done. ./CB_resources/files/view_report.json
Views export done. ./CB_resources/updateView.txt
```

- Step 4
    - Use updateView.txt to update the view by CB Importer
    - CB Importer will import the view to _design/{{partition}}
    e.g. view_main.json will upload to "_design/main", then system can access the view by "_design/main/_view/signatureExpire"