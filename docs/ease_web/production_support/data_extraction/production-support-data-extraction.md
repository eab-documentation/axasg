---
id: production-support-data-extraction
title: Data Extraction
sidebar_label: Data Extraction
---

## Production Data Extraction Query
-   Oracle Extraction
    - Extract as XML
    ```sql
    set feedback off
    set echo off
    set pagesize 0
    set linesize 32767
    set long 50000
    set longchunksize 50000
    set space 0
    set serveroutput on
    spool {{xml_output}}.txt
    select dbms_xmlgen.getxml('select * from api_wfi_rec_doc_trx where pol_num = ''102-7779048''') api_wfi_rec_doc_trx from dual;
    spool off;
    ```
    Rename the {{xml_output}} and replace the sql script to appropriate one

    - Extract as csv
    ```sql
    set colsep,
    set pagesize 0
    set trimspool on
    set headsep off
    set linesize 32767
    spool {{csv_output}}.csv
    select pol_num, to_char( cast(create_dt as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as create_dt from api_rls_rec_app_trx where pol_num = '302-1226869';
    spool off;
    ```
    Rename the {{csv_output}} and replace the sql script to appropriate one

-   Couchbase Extraction
    - AXA Support Team know how to extract the Full case by policy number
    (Full Case means All JSON files inlcuding Bundle, client profile(s), quotaiton, applicaiton and policy)

-   Server Log Extraction
    - AXA Support Team can extract Logs by time and keywords.

## Import to dev for investigation
-   There is a tool to help the import which is check-in in the EASE-WEB git repository

    Details can be found in Tools Section:
[Tools - Mask and Import Production Data](../../tools/mask-import-production-data.md)