---
id: production-support-data-bcp
title: Data Extraction for BCP Module
sidebar_label: Data Extraction for BCP Module
---

## Related Tables
### Oracle Table ("api_wfi_rec_doc_trx")
###### Description: Audit Log for all WFI request 
- Extract as XML
```sql
set feedback off
set echo off
set pagesize 0
set linesize 32767
set long 50000
set longchunksize 50000
set space 0
set serveroutput on
spool {{xml_output}}.txt
select dbms_xmlgen.getxml('select * from api_wfi_rec_doc_trx where pol_num = ''102-2153793''') api_wfi_rec_doc_trx from dual;
spool off;
```
Rename the {{xml_output}} and replace the sql script to appropriate one

### Oracle Table ("api_rls_rec_app_trx")
###### Description: Audit Log for all rls request 
- Extract as XML
```sql
set feedback off
set echo off
set pagesize 0
set linesize 32767
set long 50000
set longchunksize 50000
set space 0
set serveroutput on
spool {{xml_output}}.txt
select dbms_xmlgen.getxml('select * from api_rls_rec_app_trx where pol_num = ''102-2153793''') api_rls_rec_app_trx from dual;
spool off;
```
Rename the {{xml_output}} and replace the sql script to appropriate one    

### Oracle Table ("batch_submission_request")
###### Description: PRE-SUBMISSION batch job will pick up the policy that approved, rejected or expired and add them into this table for sending WFI / RLS
- Extract as XML
```sql
set feedback off
set echo off
set pagesize 0
set linesize 32767
set long 50000
set longchunksize 50000
set space 0
set serveroutput on
spool {{xml_output}}.txt
select dbms_xmlgen.getxml('select * from batch_submission_request where eapp_no = ''NB001108-00830''') batch_submission_request from dual;
spool off; 
```
Rename the {{xml_output}} and replace the sql script to appropriate one

### Oracle Table ("eapp_mst")
###### Description: Table included all related the document ids of that policy
- Extract as XML
```sql
set feedback off
set echo off
set pagesize 0
set linesize 32767
set long 50000
set longchunksize 50000
set space 0
set serveroutput on
spool {{xml_output}}.txt
select dbms_xmlgen.getxml('select * from eapp_mst where pol_num = ''102-2153793''') eapp_mst from dual;
spool off; 
```
Rename the {{xml_output}} and replace the sql script to appropriate one

### Oracle Table ("wfi_eip_exception")
###### Description: Exception captured in sending WFI / RLS request to AXA
- Extract as XML
```sql
set feedback off
set echo off
set pagesize 0
set linesize 32767
set long 50000
set longchunksize 50000
set space 0
set serveroutput on
spool {{xml_output}}.txt
select dbms_xmlgen.getxml('select * from wfi_eip_exception where pol_num = ''102-2153793''') wfi_eip_exception from dual;
spool off; 
```
Rename the {{xml_output}} and replace the sql script to appropriate one

### Oracle Table ("api_rls_esub_trx")
###### Description: RLS Mapping Output from EASE-API
- Extract as XML
```sql
set feedback off
set echo off
set pagesize 0
set linesize 32767
set long 50000
set longchunksize 50000
set space 0
set serveroutput on
spool {{xml_output}}.txt
select dbms_xmlgen.getxml('select * from api_rls_esub_trx where pol_num = ''102-7864238''') api_rls_esub_trx from dual;
spool off;
```
Rename the {{xml_output}} and replace the sql script to appropriate one