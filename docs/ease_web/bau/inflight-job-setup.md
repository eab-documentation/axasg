---
id: inflight-job-setup
title: Inflight Job Setup
sidebar_label: Inflight Job Setup
---

## Example: (Release 19 - commit hash [6cef7cc3])
### 1. Create Couchbase View in "invalidateView" (bz/cbDao/views/invalidateViews.js)
[Guidelines of Couchbase View Setup](./couchbase-view-setup)
```javascript
release19InflightHandling: function(doc) {
    if (doc && doc.type === 'quotation') {
        var emitObj = {
            bundleId: doc.bundleId,
            clientId: doc.pCid
        };
        var needEmit = false;
        if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
            for (var i = 0; i < doc.fund.funds.length; i++) {
                var fundObj = doc.fund.funds[i];
                if (fundObj && (fundObj.fundCode === 'TGBO' || fundObj.fundCode === 'TGTR')) {
                    needEmit = true;
                    break;
                }
            }
        }

        if (doc.plans) {
            for (var j = 0; j < doc.plans.length; j++) {
                var planObj = doc.plans[j];
                if (planObj && planObj.campaignIds && planObj.campaignIds.indexOf('campaign_TERM18_1') > -1) {
                    needEmit = true;
                }
            }
        }

        if (needEmit) {
            emit(['01'], emitObj);
        }
    }
}
```
- Above view will extract all the client ids that quoted the products with campaign "campaign_TERM18_1" and funds "TGBO"/ "TGTR" before
- Those quotation may be not in the current bundle, so the job needs to filter it by using the current bundle ids

```javascript
.then(param => {
    /**
    * Every Inflight job should create new couchbase view to limit the scope of the cases
    * Reduce the run time and impacted cases
    */
    return getAllCustomerByRelease(['01']).then(allCustomers => {
    return _.set(param, 'allCustomers', allCustomers);
    });
}).then(param => {
    let allCustomers = _.get(param, 'allCustomers', []);
    return getAllValidBundleId().then(bundleIds => {
    let cids = _.map(allCustomers, customer => {
        if (customer.bundleId && customer.clientId && bundleIds.indexOf(customer.bundleId) > -1){
        return customer.clientId;
        }
    });
    cids = _.compact(_.union(cids));
    return _.set(param, 'cids', cids);
    });
})
```

- "getAllCustomerByRelease()" will get all the client ids by using the view created in step 1
- "getAllValidBundleId()" will get all the current bundle ids
- Then the job will filter out the client ids that not in the current bundle, becuase if the cases are in the bundle history, no need to invalidate those cases


### 2. Change the Inflight Job Parameters (CB_resources/files/generalInvalidationParameter.json)

```javascript
{
  "fromTime": "2019-07-17 00:00:00",
    //Remarks: limit the job only execute on 2019-07-17
  "toTime": "2019-07-17 23:59:59",
    //Remarks: limit the job only execute on 2019-07-17
  "scheduleCron": "8 10 * 1-12 *",
    //Remarks: specify the time to execute (minute hours day_of_month months day_of_week ) - https://www.npmjs.com/package/node-schedule
  "jobDetailsDocId":"GeneralInvalidationJob",
    //Remarks: document id in couchbase for auditing after inflight job executed
  "impactedListDocId":"InvalidationImpactedList",
    //Remarks: document id in couchbase for auditing after inflight job executed
  "messages":[
    {
      "combinationId": "invalidateTPXTPPXCombine",
      "messageId": "invalidateCampaignMessage_Release19",
      "message": "Inflight Campaign Message.",
      "messageGroup": "invalidateMsg",
      "messageGroupPriority": "2"
    },
    {
      "combinationId": "invalidateProductsByFundRuleCombine",
      "messageId": "invalidateFundMsg_Release19",
      "message": "Inflight Fund Msg",
      "messageGroup": "invalidateMsg",
      "messageGroupPriority": "2"
    }
  ],
    //Remarks: message used for setting up the message in the landing page
    // The message will be inserted to agent profile UX_ ... when the rule of combinationId is satisfied
  "jobs":[
    {
      "id": "inflightJobRelease",
      "ruleIds":["invalidateTPXTPPX","invalidateFunds"],
      //Remarks: All the rule ids need to insert into above field "ruleIds" and the "ruleIds" need to be same as the key of the rulesParamsMapping
      // Then the job can find the parameter of the rule by the rulesParamsMapping
      // For Release19 there are 2 new rules added, then new functions need to added into "bz/jobs/inflightRules/invalidationRules.js"
      // Will further discuss the new function in next part
      "rulesParamsMapping":{
        "invalidateTPXTPPX":{
          "functId": "invalidateProductsByCampaignRule",
          "campaignIds":["campaign_TERM18_1"],
          "partiallySignedOnly": true
        },
        //Remarks: functId must be same as the function name in the "invalidationRules.js"
        // The other fields in the above object can be changed in different rule
        // because different rule will use different parameter to check the case 
        // above example is checking the the case has campaign or not
        // also the case is partially signed or not
        "invalidateFunds":{
          "functId": "invalidateProductsByFundRule",
          "fundCodes":["TGBO", "TGTR"],
          "partiallySignedOnly": true
        }
      },
      "rulesCombinations":[{
          "combinationId": "invalidateTPXTPPXCombine",
          "ruleIdsLookUp":["invalidateTPXTPPX"],
          "lookUpFormula": "#1",
          "expirePolicy": false
        },
        {
          "combinationId": "invalidateProductsByFundRuleCombine",
          "ruleIdsLookUp":["invalidateFunds"],
          "lookUpFormula": "#1",
          "expirePolicy": false
          //Remarks: This will combine 2 rules to come out the final decision
          // e.g. it will lok up the case that is satisfy the rule "invalidateFunds"
          // if "invalidateFunds" is true #1 will be true 
          // "lookUpFormula" can be setup as "#1 || #2", that means if either one of the rule is satisfied, the case will be invalidated
        }
      ]
    }
  ]
}
```

### 3. Add new functions added into "bz/jobs/inflightRules/invalidationRules.js"

```javascript
const invalidateProductsByCampaignRule = (invalidateParams) => {
  let {quotation, ruleParams} = invalidateParams;
  let {
    campaignIds
  } = ruleParams;
  let planList = _.get(quotation,'plans');
  let hasCampaignPlan = _.filter(planList, plan => {
    let fulfilledCampaign = false;
    _.each(plan.campaignIds, singplePlanCampaignIds => {
      if (campaignIds.indexOf(singplePlanCampaignIds) > -1) {
        fulfilledCampaign = true;
      }
    });
    return fulfilledCampaign;
  });
  if (hasCampaignPlan.length > 0) {
    return true;
  } else {
    return false;
  }
};

const invalidateProductsByFundRule = (invalidateParams) => {
  let {quotation, ruleParams} = invalidateParams;
  let {
    fundCodes
  } = ruleParams;
  let fundList = _.get(quotation,'fund.funds');
  if (_.filter(fundList, fund => { return fundCodes.indexOf(fund.fundCode) > -1; }).length > 0) {
    return true;
  }
  return false;
};

```
- return true means the rule is satisfied and the case will be invalidated if no other related rules.