---
id: refill-policy-number
title: Refill Policy Number
sidebar_label: Refill Policy Number
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 11 Oct 2019 | Init | Kevin Liang |

## Issue we meet
### Web
We retrieve policy numbers on eApp stage. Sometimes, we don't have enough policy numbers in SIT3 environment. 

### iOS
We retrieve policy numbers when user online login or after data sync, we don't have enough policy numbers in SIT3 environment.

## Diagnosis
It's hard to retrieve real policy numbers from AXA. Or we can say we don't need real policy numbers because our internal environment is just for testing. We have 2 tables named "POOL_SHIELD" and "POOL_NON_SHIELD". They are storing policy numbers. So the best way and convenient way to refill policy number is just inserting some dummy records into these 2 tables. 

## Solution
We have prepared 2 sql scripts. One is for shield and the other one is for non-shield. Just exec them to refill policy number.

Note: Don't forget to delete all data in these tables before refilling policy numbers because it maybe has conflicts with other existing policy numbers. Just click to download.

[Refill Shield Policy Number](assets/img/ease_web/policyNumber/refillShieldPolicyNumber.sql)

[Refill Non-shield Policy Number](assets/img/ease_web/policyNumber/refillNonShieldPolicyNumber.sql)