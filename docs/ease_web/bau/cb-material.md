---
id: cb-material
title: Download Material
sidebar_label: Download Material
---

#### 1. Add/ Modify json
- Copy a new material json (eg: pul99_1.json) or just modify existing json to update some fields.
- Below are fields in a material json and fields that are usually updated are as followed.
    - "section" & "sectionId" : determine  which section (eg: New Business Foems, Policy Service Forms) the document should be included, copy existing code unless new section is added.
    - "name" : document name to to displayed on screen.
    - "id"  : to control display sequence, increment the number, if next number already exists then add an alphabet behind the number, eg: prod19b_1.json.
    - "expDate"  : can be used to hide document if not needed anymore/ removed in requirement.
    - "releaseDate": If only require to update pdf, can update only the releaseDate so that jenkins will trigger the deployment for this change.

```sh
{
    "compCode"  : "08",
    "type"      : "material",
    "section"   : "PULSAR Fund Range",
    "sectionId" : "pulf",
    "name"      : "Pulsar/Polaris/Optimus/AXA Wealth Treasure/AXA Wealth Invest (Cash, SRS)/AXA Wealth Accelerate Fund Classification Table",
    "id"        : "pul99_1",
    "effDate"   : "2017-05-22 14:10:34",
    "expDate"   : "2118-05-22 14:19:47",
    "releaseDate":"2019-05-27 09:11:59"      
}
```
![Update View Location](assets/img/ease_web/downloadMaterial/download-material-1.png)

#### 2. Add related documents to <u>ease-web/CB_resources/files/material</u>

![Update View Location](assets/img/ease_web/downloadMaterial/download-material-2.png)

#### 3. Update Materials.txt for new json/ pdf files

```sh
D	pul99_1	pul99_1.json
A	pul99_1	pulf	AWA, AWI (Cash,SRS), Pulsar_Fund Risk Classification Table_Jun 2019.pdf
```

#### 4. Import using cb_importer into couchbase