---
id: couchbase-view-setup
title: Couchbase View Setup
sidebar_label: Couchbase View Setup
---

## Example: (Release 19 - commit hash [6cef7cc3])
### Edit the view in javascript file (bz/cbDao/views/invalidateViews.js)
```javascript
release19InflightHandling: function(doc) {
    if (doc && doc.type === 'quotation') {
        var emitObj = {
            bundleId: doc.bundleId,
            clientId: doc.pCid
        };
        var needEmit = false;
        if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
            for (var i = 0; i < doc.fund.funds.length; i++) {
                var fundObj = doc.fund.funds[i];
                if (fundObj && (fundObj.fundCode === 'TGBO' || fundObj.fundCode === 'TGTR')) {
                    needEmit = true;
                    break;
                }
            }
        }

        if (doc.plans) {
            for (var j = 0; j < doc.plans.length; j++) {
                var planObj = doc.plans[j];
                if (planObj && planObj.campaignIds && planObj.campaignIds.indexOf('campaign_TERM18_1') > -1) {
                    needEmit = true;
                }
            }
        }

        if (needEmit) {
            emit(['01'], emitObj);
        }
    }
}
```

### Build the view
- Execute the command "npm run build-node" in the root level
- Execute the command "node ViewGenerator.js"

### Import the view to the couchbase

```sh
java -jar CB_importer.jar 
Sync Gateway IP & Port: 192.168.222.159:4985
Database: axasg_dev
200 - OK
Config file name: updateView.txt
```
### Wait for Couchbase to complete the indexing