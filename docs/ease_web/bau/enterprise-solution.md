---
id: enterprise-solution
title: Enterprise Solution
sidebar_label: Enterprise Solution
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 27 Aug 2019 | Init | Jericho Mesina |
| 1.1 | 9 Oct 2019 | Add something need attention on iOS 13, add process flow image  | Kevin Liang |

## Process Flow Image
### IS Architecture: Updated design for using AWS S3
Highlighting the AWS S3 connection between EASE WEB and EASE ADMIN
[![Highlighting the AWS S3 connection between EASE WEB and EASE ADMIN Image](assets/img/ease_web/uploadIPA/UpdatedDesignForUsingAWSS3.png)](assets/img/ease_web/uploadIPA/UpdatedDesignForUsingAWSS3.png)

### Process Flow for Uploading iOS App
[![Process Flow for Uploading iOS App Image](assets/img/ease_web/uploadIPA/processFlowForUpload.png)](assets/img/ease_web/uploadIPA/processFlowForUpload.png)

### Process Flow for Downloading iOS App
[![Process Flow for Downloading iOS App Image](assets/img/ease_web/uploadIPA/processFlowForDownload.png)](assets/img/ease_web/uploadIPA/processFlowForDownload.png)

## How to use

### 1. Use Case: Upload
- In EASE Admin, navigate to the "IPA Upload" section. Once there, you can see the current version that the app is on. This means that everyone downloading the app is downloading that version.

![Update View Location](assets/img/ease_web/uploadIPA/uploadIPA.png)

- Type in the version of the newly-uploaded file on the text field then select the .ipa file you want to upload, from your computer.

![Update View Location](assets/img/ease_web/uploadIPA/uploadIPA2.png)

- Hit the IPA File Upload button to upload your .ipa file which also automatically sets the newly-uploaded file as the current version and the (Current Version) app as the previous version.

![Update View Location](assets/img/ease_web/uploadIPA/uploadIPA3.png)

- After the upload, you will now see your version (1.69.89) as the current version and you can press "Rollback Version" to rollback to the previous version (1.69.88).

![Update View Location](assets/img/ease_web/uploadIPA/uploadIPA4.png)

### 2. Use Case: Download

- Navigate to the Download App section on EASE - Web on your iPad. Click on it to download the current version of the App. It automatically installs if it is not yet in your iPd and it will update the app if it is. This feature is only available for Agency Channels.

* How to check if the device is iPad?

On iOS 12 and previous versions, we use "navigator.userAgent". On iPad, it will show just like

```
Mozilla/5.0 (iPad; U; CPU iOS 2_0 like Mac OS X; en-us) 
AppleWebKit/525.18.1 (KHTML, like Gecko) 
Version/3.1.1 Mobile/XXXXX Safari/525.20
```

When we have iPad string, we regards it as iPad. But on iOS 13, the "navigator.userAgent" shows just like 

```Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Safari/605.1.15```

We can't use the previous method to detect. According to the solution of Stack Overflow, we can add one more condition to detect 

```navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1```

Refer to: [https://stackoverflow.com/questions/58019463/how-to-detect-device-name-in-safari-on-ios-13-while-it-doesnt-show-the-correct](https://stackoverflow.com/questions/58019463/how-to-detect-device-name-in-safari-on-ios-13-while-it-doesnt-show-the-correct)

![Update View Location](assets/img/ease_web/uploadIPA/uploadIPA5.png)

### 3. Endpoints

- This endpoint takes the IPA file, encrypts it with a special KMS key from AWS S3 and uploads the IPA file to the AWS bucket. 

```sh
Type: POST
Path: /ease-api/upload/ios

Headers:
Content-Type: application/x-www-form-urlencoded

Body (Multipart/FormData):
version(string) - the version of the app
length(number/integer) - the exact size of the ipa file IN BYTES
file(File) - the IPA file

Response - returns the result from AWS after the upload.
```

- This endpoint generates the presigned url of the latest version of the IPA file (has a special expiration time of 15 minutes)

```sh
Type: GET
Path: /ease-api/retrieve/url/ipa
Headers: none

Response - returns the presigned url
```

- This endpoint retrieves the current and previous versions of the IPA file in the bucket.

```sh
Type: POST
Path: /ease-api/retrieve/versions
Headers: none
Body: none
Response - returns a json representation of the current and previous versions of the IPA file along with other related information and details.
```

- This endpoint retrieves the exceptions that has happend during upload of the file.

```sh
Type: GET
Path: /ease-api/retrieve/exceptions
Headers: none

Response - returns a list of exceptions that has happened.
```

- This endpoint manages the versions when user does a rollback of versions.

```sh
Type: POST
Path: /ease-api/retrieve/exceptions
Headers: none
Body: 
versionawsId (string) - the Version ID of the file defined by AWS s3 (not to be confused with the App version)
```