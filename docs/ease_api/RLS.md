---
id: RLS
title: RLS
sidebar_label: RLS
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1 | 25 FEB 2019 | Init | Kelvin Wong|

## Json structure of RLS mapping (Swagger)
- The Json structure of RLS mapping is based on the swagger file provided by AXA to generate.
- Below is path for latest swagger file
	- `\\\192.168.222.56\axa-sg\RLS Mapping\life-api_application-v2.3.yaml`
- You can open the “yaml” by below online tools to check the json structure for the RLS mapping
	- https://editor.swagger.io/
	
- Below is the step to show how to use the tool to view the structure
	- Step 1: open https://editor.swagger.io/
	- Step 2: Import “yaml” file
[![EASE API RLS](assets/img/ease-api/rls/rls_1.png)](assets/img/ease-api/rls/rls_1.png)
	- Step 3: Now you can view the complete definition of json structure from right panel 
[![EASE API RLS](assets/img/ease-api/rls/rls_2.png)](assets/img/ease-api/rls/rls_2.png)

## Detail business logic for RLS mapping
- For Shield
	- `\\\192.168.222.56\axa-sg\RLS Mapping\Shield`
	
- For Non Shield
	- `\\\192.168.222.56\axa-sg\RLS Mapping\Non Shield`
	
| Document Name | Description |
| - | - |
| RLS-PEGA-EIP Message Specification Matrix v 0.63 | The detail mapping between RLS and EIP |
| RLS_Mapping_Table_Values_v16.1 | The main document to show the business mapping between EASE and RLS |
| MTM_RecordPayment_RecordApplication_v1.2 | Supplement of payment mapping |
| eBI_Nationality.Residency (NB)_v16b | Category mapping for Nationality & Residency |

## Development


- Development environment setup
	- '\\\192.168.222.56\axa-sg\Software\Setup Java development platform in MAC OS X v1.1.docx'

- Entry point for RLS mapping API
	- com.eab.rest.ease.submission.Submission.java

- If there is any new mapping json is needed to create or RLS, please remember to add “rls_” as a prefix in the json file name, below is the current list of mapping json for reference.
	1. rls_standardCountryCodeMapping
	2. rls_packageToRiderMapping
	3. rls_riderClassCodeMapping
	4. rls_salesChargeForRSPMapping
	5. rls_serviceFreeMapping

- When there is a plan code added, it’s required to add the new mapping is one area instead of put the plan code in everywhere.
[![EASE API RLS](assets/img/ease-api/rls/rls_3.png)](assets/img/ease-api/rls/rls_3.png)

- As there are a lot of special handle for investment product in the mapping, if the new product is an investment product, remember to update the “is_ILPProduct” to “true”
[![EASE API RLS](assets/img/ease-api/rls/rls_4.png)](assets/img/ease-api/rls/rls_4.png)

- There are some products are required to include the sale charge information to RLS, remember to update the “changibleIND_PROD” to “true” for those products request by AXA.
[![EASE API RLS](assets/img/ease-api/rls/rls_5.png)](assets/img/ease-api/rls/rls_5.png)
	
- If AXA updated the list for Traditional products, remember to update the “isTraditionalProducts” to “true”
[![EASE API RLS](assets/img/ease-api/rls/rls_6.png)](assets/img/ease-api/rls/rls_6.png)

- Get all lookup json files at the beginning and store in the jsonobject, the syntax can reduce multiple DB access during the program run
[![EASE API RLS](assets/img/ease-api/rls/rls_7.png)](assets/img/ease-api/rls/rls_7.png)
	
## How to do the testing
- Put the policy number in testing link to check the result
- Local environment 
	- http://localhost:8080/ease-api/submission/101-1780622
- EAB SIT 1
	- http://api.ease-sit.intraeab/ease-api/submission/102-1936024
- EAB SIT 2 
	- http://api.ease-sit2.intraeab/ease-api/submission/102-1936024

- Successful case
[![EASE API RLS](assets/img/ease-api/rls/rls_8.png)](assets/img/ease-api/rls/rls_8.png)

- Failure case with error return, also you should receive the notification error email
[![EASE API RLS](assets/img/ease-api/rls/rls_9.png)](assets/img/ease-api/rls/rls_9.png)

- For failure case, you can insert your email in below table to receive the notification error email
[![EASE API RLS](assets/img/ease-api/rls/rls_10.png)](assets/img/ease-api/rls/rls_10.png)

- To submit the case to RLS, you can use postman to include the json file.

Step 1: Request a new policy number (if needed)

|   |  |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Http Link (For SIT 1) | http://api.ease-sit.intraeab/ease-api/policyNumber/assignPolicyNumber |
| Http Link (For SIT 2) | http://api.ease-sit2.intraeab/ease-api/policyNumber/assignPolicyNumber |
| Http method | Post |
| Header      | Content-Type = application/json |
| Body (For non-shield)	| {"companyCD":"5","requestedPolicyNO":"1"} |
| Body (For shield)	| {"companyCD":"3","requestedPolicyNO":"1"} |

- Example:
[![EASE API RLS](assets/img/ease-api/rls/rls_11.png)](assets/img/ease-api/rls/rls_11.png)

Step 2: Submit the request the RLS
|   |  |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Http Link (For SIT 1) | http://api.ease-sit.intraeab/ease-api/application |
| Http Link (For SIT 2) | http://api.ease-sit2.intraeab/ease-api/application |
| Http method | Post |
| Header      | Content-Type = application/json |
| Body 	| The json file generated in RLS mapping API  |

- Successful case: it will return curly brackets
[![EASE API RLS](assets/img/ease-api/rls/rls_12.png)](assets/img/ease-api/rls/rls_12.png)

- For failure case, it will return exception detail from RLS and receive the notification error email
[![EASE API RLS](assets/img/ease-api/rls/rls_13.png)](assets/img/ease-api/rls/rls_13.png)

- For failure case, you can insert your email in below table to receive the notification error email
[![EASE API RLS](assets/img/ease-api/rls/rls_14.png)](assets/img/ease-api/rls/rls_14.png)


## Troubleshooting
- For AXA production
	- Request the full set of json file for the case from AXA and import to our local environment to generate a json file to check the error
	
- For AXA non-production 
	- We can connect to AXA’s couchbase directly from our local development, just simple update/change the path in EnvVaiable.java, password please request your supervisor to get.
[![EASE API RLS](assets/img/ease-api/rls/rls_15.png)](assets/img/ease-api/rls/rls_15.png)

- You also can check below table to see how many cases got failure if the status is equal to “F”
[![EASE API RLS](assets/img/ease-api/rls/rls_16.png)](assets/img/ease-api/rls/rls_16.png)

