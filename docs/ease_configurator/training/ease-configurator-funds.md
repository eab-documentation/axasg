---
id: ease-configurator-funds
title: Funds
sidebar_label: Funds
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/07-Funds/Slide1.png)](assets/img/configurator/training/07-Funds/Slide1.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide2.png)](assets/img/configurator/training/07-Funds/Slide2.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide3.png)](assets/img/configurator/training/07-Funds/Slide3.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide4.png)](assets/img/configurator/training/07-Funds/Slide4.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide5.png)](assets/img/configurator/training/07-Funds/Slide5.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide6.png)](assets/img/configurator/training/07-Funds/Slide6.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide7.png)](assets/img/configurator/training/07-Funds/Slide7.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide8.png)](assets/img/configurator/training/07-Funds/Slide8.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide9.png)](assets/img/configurator/training/07-Funds/Slide9.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide10.png)](assets/img/configurator/training/07-Funds/Slide10.png) 

[![See react](assets/img/configurator/training/07-Funds/Slide11.png)](assets/img/configurator/training/07-Funds/Slide11.png) 