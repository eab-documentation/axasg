---
id: ease-configurator-quotation-calculations
title: Quotation Calculations
sidebar_label: Quotation Calculations
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide1.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide1.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide2.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide2.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide3.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide3.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide4.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide4.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide5.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide5.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide6.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide6.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide7.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide7.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide8.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide8.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide9.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide9.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide10.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide10.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide11.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide11.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide12.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide12.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide13.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide13.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide14.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide14.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide15.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide15.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide16.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide16.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide17.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide17.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide18.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide18.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide19.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide19.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide20.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide20.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide21.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide21.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide22.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide22.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide23.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide23.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide24.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide24.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide25.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide25.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide26.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide26.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide27.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide27.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide28.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide28.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide29.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide29.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide30.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide30.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide31.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide31.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide32.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide32.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide33.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide33.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide34.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide34.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide35.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide35.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide36.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide36.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide37.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide37.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide38.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide38.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide39.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide39.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide40.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide40.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide41.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide41.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide42.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide42.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide43.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide43.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide44.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide44.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide45.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide45.png) 

[![See react](assets/img/configurator/training/04-Quotation_Calculations/Slide46.png)](assets/img/configurator/training/04-Quotation_Calculations/Slide46.png) 

