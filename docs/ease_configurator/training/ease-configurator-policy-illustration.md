---
id: ease-configurator-policy-illustration
title: Policy Illustration
sidebar_label: Policy Illustration
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide1.png)](assets/img/configurator/training/05-Policy_Illustration/Slide1.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide2.png)](assets/img/configurator/training/05-Policy_Illustration/Slide2.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide3.png)](assets/img/configurator/training/05-Policy_Illustration/Slide3.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide4.png)](assets/img/configurator/training/05-Policy_Illustration/Slide4.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide5.png)](assets/img/configurator/training/05-Policy_Illustration/Slide5.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide6.png)](assets/img/configurator/training/05-Policy_Illustration/Slide6.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide7.png)](assets/img/configurator/training/05-Policy_Illustration/Slide7.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide8.png)](assets/img/configurator/training/05-Policy_Illustration/Slide8.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide9.png)](assets/img/configurator/training/05-Policy_Illustration/Slide9.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide10.png)](assets/img/configurator/training/05-Policy_Illustration/Slide10.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide11.png)](assets/img/configurator/training/05-Policy_Illustration/Slide11.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide12.png)](assets/img/configurator/training/05-Policy_Illustration/Slide12.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide13.png)](assets/img/configurator/training/05-Policy_Illustration/Slide13.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide14.png)](assets/img/configurator/training/05-Policy_Illustration/Slide14.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide15.png)](assets/img/configurator/training/05-Policy_Illustration/Slide15.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide16.png)](assets/img/configurator/training/05-Policy_Illustration/Slide16.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide17.png)](assets/img/configurator/training/05-Policy_Illustration/Slide17.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide18.png)](assets/img/configurator/training/05-Policy_Illustration/Slide18.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide19.png)](assets/img/configurator/training/05-Policy_Illustration/Slide19.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide20.png)](assets/img/configurator/training/05-Policy_Illustration/Slide20.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide21.png)](assets/img/configurator/training/05-Policy_Illustration/Slide21.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide22.png)](assets/img/configurator/training/05-Policy_Illustration/Slide22.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide23.png)](assets/img/configurator/training/05-Policy_Illustration/Slide23.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide24.png)](assets/img/configurator/training/05-Policy_Illustration/Slide24.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide25.png)](assets/img/configurator/training/05-Policy_Illustration/Slide25.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide26.png)](assets/img/configurator/training/05-Policy_Illustration/Slide26.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide27.png)](assets/img/configurator/training/05-Policy_Illustration/Slide27.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide28.png)](assets/img/configurator/training/05-Policy_Illustration/Slide28.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide29.png)](assets/img/configurator/training/05-Policy_Illustration/Slide29.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide30.png)](assets/img/configurator/training/05-Policy_Illustration/Slide30.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide31.png)](assets/img/configurator/training/05-Policy_Illustration/Slide31.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide32.png)](assets/img/configurator/training/05-Policy_Illustration/Slide32.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide33.png)](assets/img/configurator/training/05-Policy_Illustration/Slide33.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide34.png)](assets/img/configurator/training/05-Policy_Illustration/Slide34.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide35.png)](assets/img/configurator/training/05-Policy_Illustration/Slide35.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide36.png)](assets/img/configurator/training/05-Policy_Illustration/Slide36.png) 

[![See react](assets/img/configurator/training/05-Policy_Illustration/Slide37.png)](assets/img/configurator/training/05-Policy_Illustration/Slide37.png) 