---
id: ease-configurator-product-filtering
title: Product Filtering
sidebar_label: Product Filtering
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide1.png)](assets/img/configurator/training/02-Product_Filtering/Slide1.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide2.png)](assets/img/configurator/training/02-Product_Filtering/Slide2.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide3.png)](assets/img/configurator/training/02-Product_Filtering/Slide3.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide4.png)](assets/img/configurator/training/02-Product_Filtering/Slide4.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide5.png)](assets/img/configurator/training/02-Product_Filtering/Slide5.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide6.png)](assets/img/configurator/training/02-Product_Filtering/Slide6.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide7.png)](assets/img/configurator/training/02-Product_Filtering/Slide7.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide8.png)](assets/img/configurator/training/02-Product_Filtering/Slide8.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide9.png)](assets/img/configurator/training/02-Product_Filtering/Slide9.png) 

[![See react](assets/img/configurator/training/02-Product_Filtering/Slide10.png)](assets/img/configurator/training/02-Product_Filtering/Slide10.png) 

