---
id: ease-configurator-introduction
title: Introduction
sidebar_label: Introduction
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/01-Introduction/Slide1.png)](assets/img/configurator/training/01-Introduction/Slide1.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide2.png)](assets/img/configurator/training/01-Introduction/Slide2.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide3.png)](assets/img/configurator/training/01-Introduction/Slide3.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide4.png)](assets/img/configurator/training/01-Introduction/Slide4.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide5.png)](assets/img/configurator/training/01-Introduction/Slide5.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide6.png)](assets/img/configurator/training/01-Introduction/Slide6.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide7.png)](assets/img/configurator/training/01-Introduction/Slide7.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide8.png)](assets/img/configurator/training/01-Introduction/Slide8.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide9.png)](assets/img/configurator/training/01-Introduction/Slide9.png) 

[![See react](assets/img/configurator/training/01-Introduction/Slide10.png)](assets/img/configurator/training/01-Introduction/Slide10.png) 


