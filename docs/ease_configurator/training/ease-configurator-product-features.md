---
id: ease-configurator-product-features
title: Product Features
sidebar_label: Product Features
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/03-Product_Features/Slide1.png)](assets/img/configurator/training/03-Product_Features/Slide1.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide2.png)](assets/img/configurator/training/03-Product_Features/Slide2.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide3.png)](assets/img/configurator/training/03-Product_Features/Slide3.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide4.png)](assets/img/configurator/training/03-Product_Features/Slide4.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide5.png)](assets/img/configurator/training/03-Product_Features/Slide5.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide6.png)](assets/img/configurator/training/03-Product_Features/Slide6.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide7.png)](assets/img/configurator/training/03-Product_Features/Slide7.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide8.png)](assets/img/configurator/training/03-Product_Features/Slide8.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide9.png)](assets/img/configurator/training/03-Product_Features/Slide9.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide10.png)](assets/img/configurator/training/03-Product_Features/Slide10.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide11.png)](assets/img/configurator/training/03-Product_Features/Slide11.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide12.png)](assets/img/configurator/training/03-Product_Features/Slide12.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide13.png)](assets/img/configurator/training/03-Product_Features/Slide13.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide14.png)](assets/img/configurator/training/03-Product_Features/Slide14.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide15.png)](assets/img/configurator/training/03-Product_Features/Slide15.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide16.png)](assets/img/configurator/training/03-Product_Features/Slide16.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide17.png)](assets/img/configurator/training/03-Product_Features/Slide17.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide18.png)](assets/img/configurator/training/03-Product_Features/Slide18.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide19.png)](assets/img/configurator/training/03-Product_Features/Slide19.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide20.png)](assets/img/configurator/training/03-Product_Features/Slide20.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide21.png)](assets/img/configurator/training/03-Product_Features/Slide21.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide22.png)](assets/img/configurator/training/03-Product_Features/Slide22.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide23.png)](assets/img/configurator/training/03-Product_Features/Slide23.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide24.png)](assets/img/configurator/training/03-Product_Features/Slide24.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide25.png)](assets/img/configurator/training/03-Product_Features/Slide25.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide26.png)](assets/img/configurator/training/03-Product_Features/Slide26.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide27.png)](assets/img/configurator/training/03-Product_Features/Slide27.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide28.png)](assets/img/configurator/training/03-Product_Features/Slide28.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide29.png)](assets/img/configurator/training/03-Product_Features/Slide29.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide30.png)](assets/img/configurator/training/03-Product_Features/Slide30.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide31.png)](assets/img/configurator/training/03-Product_Features/Slide31.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide32.png)](assets/img/configurator/training/03-Product_Features/Slide32.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide33.png)](assets/img/configurator/training/03-Product_Features/Slide33.png) 

[![See react](assets/img/configurator/training/03-Product_Features/Slide34.png)](assets/img/configurator/training/03-Product_Features/Slide34.png) 