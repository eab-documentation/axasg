---
id: ease-configurator-formula
title: Formula
sidebar_label: Formula
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/06-Formula/Slide1.png)](assets/img/configurator/training/06-Formula/Slide1.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide2.png)](assets/img/configurator/training/06-Formula/Slide2.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide3.png)](assets/img/configurator/training/06-Formula/Slide3.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide4.png)](assets/img/configurator/training/06-Formula/Slide4.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide5.png)](assets/img/configurator/training/06-Formula/Slide5.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide6.png)](assets/img/configurator/training/06-Formula/Slide6.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide7.png)](assets/img/configurator/training/06-Formula/Slide7.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide8.png)](assets/img/configurator/training/06-Formula/Slide8.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide9.png)](assets/img/configurator/training/06-Formula/Slide9.png) 

[![See react](assets/img/configurator/training/06-Formula/Slide10.png)](assets/img/configurator/training/06-Formula/Slide10.png) 