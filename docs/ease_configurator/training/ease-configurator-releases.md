---
id: ease-configurator-releases
title: Releases
sidebar_label: Releases
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

[![See react](assets/img/configurator/training/08-Releases/Slide1.png)](assets/img/configurator/training/08-Releases/Slide1.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide2.png)](assets/img/configurator/training/08-Releases/Slide2.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide3.png)](assets/img/configurator/training/08-Releases/Slide3.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide4.png)](assets/img/configurator/training/08-Releases/Slide4.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide5.png)](assets/img/configurator/training/08-Releases/Slide5.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide6.png)](assets/img/configurator/training/08-Releases/Slide6.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide7.png)](assets/img/configurator/training/08-Releases/Slide7.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide8.png)](assets/img/configurator/training/08-Releases/Slide8.png) 

[![See react](assets/img/configurator/training/08-Releases/Slide9.png)](assets/img/configurator/training/08-Releases/Slide9.png) 