---
id: internal-product-setup
title: Internal Product Setup
sidebar_label: Internal Product Setup
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

## 1.1.	Product Filtering

### 1.1.1.	Product Line

For product filtering and grouping in Products page.

### 1.1.2.	Product Category

For product filtering by Nationality/Residence mapping.

### 1.1.3.	FNA Suitability

By default, the system filters the product list according to the selected product types and needs in FNA for STP BI.

|  | ILP | Whole Life/Endowment | Critical Illness/Term Plan | Accident Plan | Health Plan |
| :-: | :-: | :-: | :-: | :-: | :-: |
| Investment Linked Plans (ILP) | Y | Y | Y |  |  |
| Whole life/Endowment | * | Y | Y |  |  |
| Accident/Medical |  |  | # | # | # |
| Term/CI | * | * | Y |  |  |

- ```*``` Show the product but prompt error message when the user selects the product
- ```#``` Based on selected needs

For example, if “Whole Life/Endowment” and “Term/CI” are selected for product types, the user can see following product lines in the Products page:

-	ILP (cannot create quotation)
-	Whole Life/Endowment
-	Critical Illness/Term Plan

If the default behavior does not cover the product’s requirement, implement checkSuitForViewFunc or checkSuitForQuotFunc to get more control.

checkSuitForViewFunc

Checks if the product can be displayed in the Products page.

Parameters:

| Key | Description |
| :-: | :-: |
| fnaInfo | Wrapper object which contains the FE and FNA objects |

Return value:

Boolean, true if the product can be displayed

checkSuitForQuotFunc

Checks if the user is allowed to enter quotation page with the product.

Parameters:

| Key | Description |
| :-: | :-: |
| fnaInfo | Wrapper object which contains the FE and FNA objects |

Return value:

True if the product is allowed to be quoted, false to show the default error message. Returns string if specific error message is required.

## 1.2.	Validations

### 1.2.1.	Budget Validation

Budget validation will be performed when the user clicks “Create Proposal” for STP BI only. The validation logic is defined in productSuitability.json.

budgetRules

The validation will pass if one of the budget rules is matched. To match a budget rule, the user must select the payment mode/payment method as specified in the rule.

[![See react](assets/img/configurator/image23.png)](assets/img/configurator/image23.png)

e.g. for SAV, validation will pass if regular premium is selected and the payment mode is annual/semi-annual/quarterly/monthly.

checkBudgetFunc

For complicated cases which requires to check other user inputs, checkBudgetFunc can be used. If checkBudgetFunc is used, the budgetRules will be ignored.

Parameters:
| Key | Description |
| :-: | :-: |
| suitability | Suitability object which contains the product suitability mappings |
| fnaInfo | Wrapper object which contains the FE object |
| quotation | Quotation object |

Return value:

True if it is valid, false to show the default error message. Returns string if specific error message is required.

