---
id: product-setup
title: Product Setup
sidebar_label: Product Setup
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

## 1.	Releases

A release is a collection of configurations grouped for publishing or exporting. A release can be a major release with multiple product launches, or a minor one containing only a single template change.

### 1.1.	Create a New Release
- 	Enter the “Releases” module
-	Click “Create Release” from the toolbar
-	Enter the name of the release and click “Save”

[![See react](assets/img/configurator/image24.png)](assets/img/configurator/image24.png)

### 1.2.	Add Items to a Release
A configuration can be a product or a template. Each product or template may have more than one version. A release is linked to specific versions of products and templates.

-	Select a release
-	Click “Add item” from the toolbar
-	Select the type of item (product or PDF)
-	Select the item and the version to be added to the release

[![See react](assets/img/configurator/image25.png)](assets/img/configurator/image25.png)

### 1.3.	Export
A release can be exported into a zip package. All configuration items and their attachments will be included in the zip package.

-	Create a New Release
-	Enter the “Releases” module
-	Select a release and click “Export”

### 1.4.	Publish

A release can be published to a specific environment. The list of available environments can be configured by the administrator of the product configurator.

-	Create a New Release
-	Enter the “Releases” module
-	Select a release and click “Publish”
-	Select an environment to be published

[![See react](assets/img/configurator/image26.png)](assets/img/configurator/image26.png)

### 1.5.	Clone Configurations

You can create a new version of product or PDF by cloning from existing configurations. The system will automatically assign a new version number.

[![See react](assets/img/configurator/image27.png)](assets/img/configurator/image27.png)

1.5.1.	Clone from Other Item

-	Select the product or PDF that requires a new version
-	Click “Clone from other item”
-	Enter the description for the new version and select the version to clone from
-	Click “Save”

[![See react](assets/img/configurator/image28.png)](assets/img/configurator/image28.png)

1.5.2.	Clone Latest Version
-	Select the product or PDF that requires a new version
-	Click “Clone latest version”
-	Enter the description for the new version and click “Save”

[![See react](assets/img/configurator/image29.png)](assets/img/configurator/image29.png)

## 2.	Products

### 2.1.	Product Filtering

Affects the list of products that is available to the agent and the selected client (proposer and life assured).

2.1.1.	Effective Date and Expiry Date

A product will only appear in the product list if it is effective, where the system date is within the product’s launch date and expiry date.

[![See react](assets/img/configurator/image30.png)](assets/img/configurator/image30.png)

2.1.2.	Distribution Channel

The system will filter the products by the agent’s distribution channel. The available list of distribution

[![See react](assets/img/configurator/image31.png)](assets/img/configurator/image31.png)

2.1.3.	Entry Age

The system will check the entry ages of the products against the age of both proposer and life assured.

-	Age: the minimum/maximum age
-	Age Unit: the unit of age
o	Day (attained)
o	Month (attained)
o	Year (attained)
o	Age Nearest Birthday
o	Age Next Birthday
-	Same as Owner: whether the entry age for life assured is the same as the one of proposer

[![See react](assets/img/configurator/image32.png)](assets/img/configurator/image32.png)

2.1.4.	Gender and Smoking Habit

Products can be filtered by the life assured’s gender and smoking habit.

[![See react](assets/img/configurator/image33.png)](assets/img/configurator/image33.png)

2.1.5.	Product Line

For product filtering and grouping in Products page.

2.1.6.	Product Category

For product filtering by Nationality/Residence mapping.

### 2.2.	Basic Plan Features

2.2.1.	Currencies

The available options of currencies for the quotation.

[![See react](assets/img/configurator/image34.png)](assets/img/configurator/image34.png)

2.2.2.	Payment Modes

The available options of payment modes for the quotation.

-	Calc. Operator: the arithmetic operator for the modal factor
-	Factor: the modal factor to be used in premium calculations and illustrations
-	Cov. Month: the number of months represented by the payment mode

[![See react](assets/img/configurator/image35.png)](assets/img/configurator/image35.png)

2.2.3.	Backdating

Whether the backdating option should be enabled for the basic plan. If it is selected, the client will be able to select backdating and select the risk commencement date in the quotation page.

[![See react](assets/img/configurator/image36.png)](assets/img/configurator/image36.png)

2.2.4.	Age Calculation Method

The age calculation method will affect how the system calculates the age of the life assured and the proposer. The ages will be used for all calculations of the quotation, including premium, illustrations etc. The available options are:

-	Nearest Birthday
-	Next Birthday
-	Current Age (Attained age)

[![See react](assets/img/configurator/image37.png)](assets/img/configurator/image37.png)

2.2.5.	GST

A GST rate can be configured when the “Require Goods & Service Tax (GST)” checkbox is selected. The GST rate can then be referenced in the calculation formulae.

[![See react](assets/img/configurator/image38.png)](assets/img/configurator/image38.png)

2.2.6.	Product Thumbnail

A thumbnail can be attached to a basic plan. The image will be shown on the UI representing the product.

[![See react](assets/img/configurator/image39.png)](assets/img/configurator/image39.png)

2.2.7.	Policy Options

Policy options are product-specific features for basic plans. To create the basic definition of policy options:

1.	Go to “Policy Options” section

2.	Click “Add”

    a.	ID: unique identifier for referencing in formulae

    b.	Title: label of the policy option to be shown on UI

    c.	Disabled: allows the user to edit

    d.	Mandatory: requires the user to input

    e.	Group ID: for grouping similar policy options in UI

Available types of policy options:
-	Text Field
-	Picker
-	Date Picker
-	Checkbox
-	Empty Block
-	Note

[![See react](assets/img/configurator/image40.png)](assets/img/configurator/image40.png)

preparePolicyOptions

Parameters:

| Key | Description |
| :-: | :-: |
| planDetail | Product setting of the plan |
| quotation | Quotation object |
| planDetails | Map of covCode to product settings of basic plan and all riders |

Input config (policyOptions):

| Key | Description |
| :-: | :-: |
| type | The type of component to be rendered in UI, e.g. checkbox, datepicker, picker, emptyBlock, text |
| subType | For type = “text” only, e.g. currency, number |
| options | For type = “picker” only, array of options with following fields: ```value``` ```title``` |
| maxLength | For currency and number only |
| disable | Y/N, set to “Y” if the field is not editable |
| mandatory | Y/N, shows mandatory error message if mandatory = “Y” and the value is null |
| errorMsg | Shows the error message below the UI field if exists |
| hintMsg | Shows the hint message below the UI field if the user focuses on the field |

2.2.8.	Funds

Each basic plan has an optional attachable fund list. The available fund list comes from the Funds module.

-	Allow selecting funds: enables the fund selection in the quotation page
-	Have Top-Up Allocation: allows the user to allocate top-ups, should be used with top-up policy options

[![See react](assets/img/configurator/image41.png)](assets/img/configurator/image41.png)

2.2.9.	Attachable Riders

Each basic plan has an attachable rider list. A rider will appear only when both the product filtering logics of the rider and the attaching condition pass.

-	Compulsory: the rider will not be removable
-	Auto attach: the rider will be automatically attach if the conditions are met

[![See react](assets/img/configurator/image42.png)](assets/img/configurator/image42.png)

[![See react](assets/img/configurator/image43.png)](assets/img/configurator/image43.png)

Co-exist rules

Riders may have co-exist rules or dependencies on selection of other riders. To specify the co-exist rules:

1.	Go to “Attachable List”, “Co-exist Rule”
2.	Click “Add”
    a.	Co-exist: riders from group A and B will always be selected and removed together
    b.	Should not co-exist: when a rider from a group is selected, the rider from the other group will be disabled and cannot be selected
    c.	B requires A: users must select the rider from group A in order to select the riders from group B

prepareAttachableRider

You are suggested to execute the default function and then execute additional filtering, i.e. invoking quotDriver.prepareAttachableRider in the beginning.

Parameters:

| Key | Description |
| :-: | :-: |
| planDetail | Product setting of the plan |
| quotation | Quotation object |
| planDetails | Map of covCode to product settings of basic plan and all riders |

Input config (riderList):

Further filter based on the built-in result from the built-in formula:
-	quotDriver.prepareAttachableRider(planDetail, quotation, planDetails)

### 2.3.	Plan Settings

2.3.1.	Policy Term and Premium Term

Override the corresponding formulas to give dynamic list of term options.

prepareTermConfig

Invokes polTermsFunc and premTermsFunc. Sets the option list for policy term and premium term to inputConfig. Also controls the default values and if the fields are editable.

Parameters:

| Key | Description |
| :-: | :-: |
| planDetail | Product setting of the plan |
| quotation | Quotation object |
| planDetails | Map of covCode to product settings of basic plan and all riders |

Input config:

| Key | Description |
| :-: | :-: |
| canEditPolicyTerm | Boolean, true if policy term can be edited |
| policyTermList | Option list for policy term (returns from polTermsFunc) |
| canEditPremTerm | Boolean, true if premium term can be edited |
| premTermList | Option list for premium term (returns from premTermsFunc) |

polTermsFunc

Returns the available option list for policy term.

Parameters:

| Key | Description |
| :-: | :-: |
| planDetail | Product setting of the plan |
| quotation | Quotation object |

Return value (array of term options to be displayed):

| Key | Description |
| :-: | :-: |
| value | Value of the option, e.g. 20, “20_YR”, “65_TA” polTermYr must be set in the plan object later if the value is not a number. |
| title | Label of the option, e.g. “20 Years”, “To Age 65” |
| default | Boolean, true if the option is selected by default |

premTermsFunc

Returns the available option list for premium term.

Parameters:

| Key | Description |
| :-: | :-: |
| planDetail|	Product setting of the plan|
| quotation	| Quotation object| 

Return value (array of term options to be displayed):

| Key | Description |
| :-: | :-: |
| value	| Value of the option, e.g. 20, “20_YR”, “65_TA” premTermYr must be set in the plan object later if the value is not a number.
| title	| Label of the option, e.g. “20 Years”, “To Age 65”| 
| default| 	Boolean, true if the option is selected by default| 

2.3.2.	Sum Insured / Premium Input

Controls if the sum insured field and the premium field can be edited or viewed. Also controls the minimum/maximum input of the 2 fields.

-	Go to “Product Setting”, “Sum Assured/Premium Input”

    o	Allow Input: enable/disable the input field

    o	Display sum insured/premium

    o	Postfix: static text after the input field

    o	Format

        	Multiple

        	Decimal point

    o	Limit: minimum/maximum of the input field

[![See react](assets/img/configurator/image45.png)](assets/img/configurator/image45.png)

[![See react](assets/img/configurator/image46.png)](assets/img/configurator/image46.png)

prepareSAPremConfig

Can be used to set dynamic limits based on the client information or other inputs, e.g. age, currency, SA of basic plan etc.

Parameters:

| Key | Description |
| :-: | :-: |
|planDetail	|Product setting of the plan|
|quotation	|Quotation object (with different payment modes)|
|planDetails	|Map of covCode to product settings of basic plan and all riders|

Input config:
| Key | Description |
| :-: | :-: |
| canEditSumAssured	| Boolean, true if sum insured field can be edited| 
| canEditPremium	| Boolean, true if premium field can be edited| 
| canViewSumAssured	| Boolean, true if sum insured field can be viewed| 
| canViewPremium	| Boolean, true if premium field can be viewed| 
| benlim	| Contains the min and max of sum assured, e.g. { min: 0, max: 150000 }| 
| premlim	| Contains the min and max of premium,e.g. { min: 1000 }| 

2.3.3.	Sum Insured / Insured Calculation

calcQuotPlan

premFunc

Returns the premium of the corresponding payment mode. Used when the user inputs the sum assured and the system is expected to calculate the premium. Will be triggered for each payment mode. i.e. If the quotation supports Annual, Semi-Annual, Quarterly, Monthly, the formula will be executed 4 times with different payment modes.

Parameters:
| Key | Description |
| :-: | :-: |
| quotation	| Quotation object (with different payment modes)| 
| planInfo	| Plan object| 
| planDetail	| Product setting of the plan| 

Return value:

Number (rounded to 2 decimal places)

saFunc

Returns the sum assured of the plan. Used when the user inputs the premium and the system calculates the sum assured. The calculated SA will be used to reverse calculate the premium if premFunc is also implemented.

Parameters:

| Key | Description |
| :-: | :-: |
| quotation	| Quotation object| 
| planInfo	| Plan object| 
| planDetail	| Product setting of the plan| 

Return value:

Number (rounded to 2 decimal places)

2.3.4.	Rate Tables

[![See react](assets/img/configurator/image47.png)](assets/img/configurator/image47.png)

For a simple rate table, the 1st column of the rate table is the lookup key. The lookup value can be a single value or an array, depending on the setting in the rate table.

e.g. Single value: planDetail.rates.TABLE[“R51”] = 1B

[![See react](assets/img/configurator/image48.png)](assets/img/configurator/image48.png)

Array: planDetails.rates.TABLE[“25”] = [0, 0.33, 0.5…]

[![See react](assets/img/configurator/image49.png)](assets/img/configurator/image49.png)

A rate table may consist of an extra level for easier lookup of rates if the “Multiple levels” options is selected.

e.g. planDetail.rates.premRate[“FPR_RP_MNS”]…

[![See react](assets/img/configurator/image50.png)](assets/img/configurator/image50.png)

### 2.4.	Validations
Validates user inputs and the calculation results. The system will validate the input limits, formats, mandatory fields by default. You may override the formulae to perform additional validations before or after calculations.

If an error is found, an error object should be added to the execution context to indicate the details using quotDriver.context.addError(…). The error object should have the fields:

-	covCode: the covCode of the plan where the error message will be attached to
-	msg: the error message to be shown on UI

[![See react](assets/img/configurator/image51.png)](assets/img/configurator/image51.png)

validBeforeCalcFunc

Default behavior (quotValid.validatePlanBeforeCalc) validates mandatory fields of the plan.

Parameters:

| Key | Description |
| :-: | :-: |
| quotation	| Quotation object| 
| planInfo	| Plan object| 
| planDetails	| Map of covCode to product settings of basic plan and all riders| 

validAfterCalcFunc

Default behavior (quotValid.validatePlanAfterCalc) validates minimum, maximum and multiples.

Parameters:
| Key | Description |
| :-: | :-: |
| quotation	| Quotation object| 
| planInfo	| Plan object| 
| planDetails	| Map of covCode to product settings of basic plan and all riders| 

### 2.5.	Backend Required Fields
You may set the backend required fields in any of the formulas (usually in validAfterCalcFunc). The fields may be used for backend system integrations or subsequent modules of the straight-through process. Keep the original behavior when overriding a formula.

[![See react](assets/img/configurator/image52.png)](assets/img/configurator/image52.png)

2.5.1.	Plan Code

Required for RLS and to be shown on proposal. Every plan in the quotation object should consist of the plan code. The plan code may vary based on users’ input, e.g. policy term, sum insured etc.

[![See react](assets/img/configurator/image53.png)](assets/img/configurator/image53.png)
| Field | Description |
| :-: | :-: |
| quotation.plans[x].planCode	| String, plan code according to mapping provided in the product matrix| 

2.5.2.	Policy Term and Premium Term

If the values for the policy term and premium term are codes instead of numbers (e.g. “20_YR”), policyTermYr/premTermYr must be set in the plan for RLS usage.

| Field | Description |
| :-: | :-: |
| quotation.plans[x].policyTermYr	| Integer, number of years of policy term| 
| quotation.plans[x].premTermYr| 	Integer, number of years of premium payment term| 

### 2.6.	Illustration
2.6.1.	Calculation

illustrateFunc

Calculates the illustrations of each policy year.

Parameters:

| Key | Description |
| :-: | :-: |
| quotation	| Quotation object| 
| planInfo	| Plan object| 
| planDetails	| Map of covCode to product settings of basic plan and all riders| 
| extraPara	| System-provided parameters, e.g. date formats, company information etc.| 

Return value:

Array with each item containing the illustration values of the corresponding year.

getIllustrateSummary

Optional. Calculates a summary based on the illustration results from all plans.

Parameters:

| Key | Description |
| :-: | :-: |
| quotation	| Quotation object| 
| planDetails	| Map of covCode to product settings of basic plan and all riders| 
| extraPara	| System-provided parameters, e.g. date formats, company information etc.| 
| illustrations	| Illustration results from all plans| 

Return value:

Array with each item containing the illustration values of the corresponding year.

2.6.2.	Illustration Chart

Enables the illustration chart for the proposal.

-	Support Illustration

    o	Select to enable illustration chart

-	Value ID

    o	The key for looking up the value from the illustrate result

-	Related to Projection Rate

    o	Whether the value is the value or a map of projection rates to values

-	Title

    o	The label displayed in the chart

-	Projections

    o	Whether the illustration chart supports multiple project rates

[![See react](assets/img/configurator/image54.png)](assets/img/configurator/image54.png) 

[![See react](assets/img/configurator/image55.png)](assets/img/configurator/image55.png) 

e.g. If an illustration column is related to the projection rate, the value in the illustrate result should be a map of values with project rate as the key.

[![See react](assets/img/configurator/image56.png)](assets/img/configurator/image56.png) 

### 2.7.	Product Summaries
Static PDF documents that describe the product details. Every product has its own product summary, including basic plans and riders. The system will merge the product summaries of the selected plans into a single PDF.

[![See react](assets/img/configurator/image57.png)](assets/img/configurator/image57.png) 

### 2.8.	Dynamic Attachments
For generating dynamic PDF attachments based on user inputs.

Parameters:
| Key | Description |
| :-: | :-: |
| agent	| Agent object| 
| quotation	| Quotation object| 
| planDetails	| Map of covCode to product settings of basic plan and all riders| 

Return value (array of attachments):

| Key | Description |
| :-: | :-: |
| id	| Attachment ID| 
| label	| Label of the attachment to be shown in UI| 
| fileName	| File name of the attachment when sent via email| 
| files	| Array of files to be merged| 
| files[x].covCode| 	Product code of the file’s source| 
| files[x].fileId	| ID of the file: ```Product Summary: prod_summary``` ```Product Summary (1): prod_summary_1``` ```Product Summary (2): prod_summary_2``` | 

e.g. A dynamic product summary based on the planType:

## 3.	PDF Templates
Uses XSLT, HTML and CSS to generate a PDF report.

1.	Executes the formula prepareReportData to generate report data
2.	Evaluates XSLT templates with the report data result to get HTML pages
3.	Merges HTML pages into a single HTML page
4.	Convert the merged HTML page to a single PDF file

3.1.1.	Templates
Sections
-	Header

    o	Shown in every page as the first section

-	Footer

    o	Shown in every page as the last section

-	Pages

    o	Page contents with predefined number of pages

-	Overlay

    o	Shown in every page on top of the existing contents

-	Images

    o	Referenced in CSS using special syntax, i.e. “background: <img@IMG_ID>”

Styles and Images

[![See react](assets/img/configurator/image59.png)](assets/img/configurator/image59.png) 

[![See react](assets/img/configurator/image60.png)](assets/img/configurator/image60.png) 

Common XSLT Syntax

-	Reference a value from the report data:

```
<xsl:value-of select=”*/agent/company” />
```

-	If/else:
```
<xsl:if test=”*/mainInfo/sameAs=’N’”>
    …
</xsl:if>

<xsl:else>
    …
</xsl:else>

```

-	For loop:
```
<xsl:for-each select=”*/planFields”>
    …
</xsl:for-each>
```

Pre-defined Variables
-	Current page number: [[@PAGE_NO]]
-	Total page number: [[@TOTAL_PAGE]]

3.1.2.	Report Data

Contains system fields and dynamic template-specific report data generated from the formula.

prepareReportData

Parameters:

| Key | Description |
| :-: | :-: |
| quotation	| Quotation object| 
| planInfo	| Plan object of the related rider| 
| planDetails	| Map of covCode to product settings of basic plan and all riders| 
| extraPara	| System-provided parameters, e.g. date formats, company information, illustration data etc.| 

## 4.	Funds

For investment-linked products. The attachable fund list of basic plans comes from the funds setup in configurator.

### 4.1.	Edit Funds

1.	Go to “Funds”
2.	Click “Add” or select an existing fund
3.	Input fund details
4.	Upload corresponding attachments (for editing only)

[![See react](assets/img/configurator/image61.png)](assets/img/configurator/image61.png) 

-	Fund Code

    o	Unique ID of a fund

-	Fund Name

-	Asset Class

-	Annual Manage Fee

-	Payment Method

    o	Affects the available fund list based on the user input of payment method

-	Risk Rating

-	Currency

### 4.2.	Export
Exports the funds and the corresponding attachments in a single zip file.

1.	Go to “Funds”
2.	Select the funds to be exported
3.	Click “Export’
