---
id: ease-configurator-dbsetup
title: Database Setup
sidebar_label: Database Setup
---

Login to Oracle Server: 192.168.222.156 oracle/{{password}} (156 is using for EASE development)

1) exp eadmin/{{password}}@axasgdev file=axasg_iconfig_AXAdeploy_20190327.dmp log=iconfig_AXAdeploy_20190327.log

2) exp eadmin_dev2/{{password}}@axasgdev file=axasg_iconfig_sit2_20180625.dmp log=iconfig_sit2_20180625.log

3) export the data schema from sqlDeveloper ("database export") --> export to a single file

4) select 'TRUNCATE TABLE '||table_name||' ;' from user_tables; 

5) export to csv, run and commit

6) Disable trigger and Constraint

```sql
ALTER TRIGGER RELEASE_HISTORY DISABLE;

ALTER TRIGGER RELEASE_MST DISABLE;

ALTER TRIGGER SYS_USER_MST_HISTORY DISABLE;

ALTER TRIGGER TASK_MST DISABLE;

alter table CONFIG_RELEASE_VERSION 
    DISABLE constraint CONFIG_RELEASE_VERSION_FK1;
alter table CONFIG_RELEASE_VERSION 
    DISABLE constraint CONFIG_RELEASE_VERSION_FK2;
alter table CONFIG_SECTION 
    DISABLE constraint CONFIG_SECTION_FK;
alter table SYS_DYNAMIC_SEARCH_FIELD 
    DISABLE constraint FK_SYS_FIELD_SEARCH_FIELD;
alter table SYS_DYNAMIC_SEARCH_ACTION 
    DISABLE constraint FK_SYS_DYNAMIC_SEARCH_ACTION;
alter table SYS_DYNAMIC_SEARCH_BUTTON 
    DISABLE constraint FK_SYS_DYNAMIC_SEARCH_BUTTON;
alter table CONFIG_VERSION 
    DISABLE constraint CONFIG_VERSION_FK;
```

7) imp eadmin/{{password}}@axasgdev fromuser=eadmin_dev2 touser=eadmin file=axasg_iconfig_sit2_20180625.dmp log=importAdmin.log data_only=y

8) Enable Trigger and Constraint

```sql
ALTER TRIGGER RELEASE_HISTORY ENABLE;

ALTER TRIGGER RELEASE_MST ENABLE;

ALTER TRIGGER SYS_USER_MST_HISTORY ENABLE;

ALTER TRIGGER TASK_MST ENABLE;

alter table CONFIG_RELEASE_VERSION 
    ENABLE constraint CONFIG_RELEASE_VERSION_FK1;
alter table CONFIG_RELEASE_VERSION 
    ENABLE constraint CONFIG_RELEASE_VERSION_FK2;
alter table CONFIG_SECTION 
    ENABLE constraint CONFIG_SECTION_FK;
alter table SYS_DYNAMIC_SEARCH_FIELD 
    ENABLE constraint FK_SYS_FIELD_SEARCH_FIELD;
alter table SYS_DYNAMIC_SEARCH_ACTION 
    ENABLE constraint FK_SYS_DYNAMIC_SEARCH_ACTION;
alter table SYS_DYNAMIC_SEARCH_BUTTON 
    ENABLE constraint FK_SYS_DYNAMIC_SEARCH_BUTTON;
alter table CONFIG_VERSION 
    ENABLE constraint CONFIG_VERSION_FK;
```