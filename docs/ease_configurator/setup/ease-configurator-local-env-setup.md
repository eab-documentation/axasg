---
id: ease-configurator-local-env-setup
title: Local Environment Setup
sidebar_label: Local Environment Setup
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 14 May 2020 | Init | Kevin Liang |

## Prepare software to setup Java development platform in MAC OS X.

[![See react](assets/img/configurator/image1.png)](assets/img/configurator/image1.png)

## Software Preparation

If you miss any software, please check with Roy / Wilson.

Mandatory
- JDK 8
- JBoss EAP 6.4
- Eclipse IDE (version Mars.1) 

Optional

- Oracle Instant Client (embedded “ojdbc6.jar”) – Not required for JBoss

## Check JDK and Instant Client in Mac OS X

Install Java Development Kit 8.  You can use command “java –version” to check any available JDK already installed.

To connect Oracle, please make sure correct version of Oracle Client is already installed.

## JBoss EAP Setup

Unzip “jboss-eap-6.4.0.zip” and put into your workspace.

We use default JBoss Server  “Standalone”.

[![See react](assets/img/configurator/image2.png)](assets/img/configurator/image2.png)

In JBoss root dictionary, create sub directory ```{JBoss_Home}/modules/oracle/jdbc/main/```.

[![See react](assets/img/configurator/image3.png)](assets/img/configurator/image3.png)

Copy Oracle JDBC Driver “ojdbc7.jar” into directory “main”.

In the directory “main”, create text file “module.xml” and paste below text lines into it.

```
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.0" name="oracle.jdbc">
    <resources>
        <resource-root path="ojdbc7.jar"/>
    </resources>
    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
    </dependencies>
</module>
```

Now we can start JBoss Server by shell file ```{JBoss_Home}/bin/standalone.sh```.

After JBoss Server is started, we can run JBoss CLI by shell file ```{JBoss_Home}/bin/jboss-cli.sh``` and type “connect”.

[![See react](assets/img/configurator/image4.png)](assets/img/configurator/image4.png)


And then we can register JDBC Driver into JBoss “Standalone” by below command.

```
/subsystem=datasources/jdbc-driver=OracleDriver:add(driver-name=OracleDriver,driver-module-name=oracle.jdbc,driver-class-name=oracle.jdbc.driver.OracleDriver)
```

[![See react](assets/img/configurator/image5.png)](assets/img/configurator/image5.png)

After ```success```, you can quit CLI by command ```quite```.

Next thing is creating JBoss Management User for login Admin Console.

Run command “add-user.sh” like below screenshot.
-	Management user
-	Username: admin
-	Password: P@ss1234

[![See react](assets/img/configurator/image6.png)](assets/img/configurator/image6.png)

Now you can open JBoss Homepage by ```http://localhost:8080/``` and click Administration Console.  And go to Configuration > Datasource.

[![See react](assets/img/configurator/image7.png)](assets/img/configurator/image7.png)

Press “Add” button and fill in JNDI setting like below screenshots.
- EASE-DS
- java:jboss/EASE-DS

[![See react](assets/img/configurator/image8.png)](assets/img/configurator/image8.png)

Choose “OracleDriver”.

[![See react](assets/img/configurator/image9.png)](assets/img/configurator/image9.png)

Input Connection String, Username, and Password.  Please use “Test Connection” to check your setting is correct.
-	jdbc:oracle:thin:@192.168.222.157:1521:SIT
-	iconfig
-	password

[![See react](assets/img/configurator/image10.png)](assets/img/configurator/image10.png)

After finish, highlight “EASE-DS” and press “Enable” button.

## Eclipse IDE Setup
Now we can open Eclipse.

Add new server into Eclipse.

[![See react](assets/img/configurator/image11.png)](assets/img/configurator/image11.png)

Choose “”.  Press Refresh if you cannot find the JBoss server in the list.

[![See react](assets/img/configurator/image12.png)](assets/img/configurator/image12.png)

[![See react](assets/img/configurator/image13.png)](assets/img/configurator/image13.png)

[![See react](assets/img/configurator/image14.png)](assets/img/configurator/image14.png)

Input JBoss EAP root directory and choose JavaSE-1.8.

[![See react](assets/img/configurator/image15.png)](assets/img/configurator/image15.png)

When JBoss Server is ready in Eclipse, we can create “Dynamic Web Project”.

[![See react](assets/img/configurator/image16.png)](assets/img/configurator/image16.png)

We assume naming application “configurator”.

[![See react](assets/img/configurator/image17.png)](assets/img/configurator/image17.png)

Open project “configurator” properties and press “Add External JARs” button to add external library “ojdbc7.jar”.  This JDBC library file should not put into “configurator”.

[![See react](assets/img/configurator/image18.png)](assets/img/configurator/image18.png)

Compiler should be “1.8”.

[![See react](assets/img/configurator/image19.png)](assets/img/configurator/image19.png)

Project Facets should choose Java using “1.8” version.

[![See react](assets/img/configurator/image20.png)](assets/img/configurator/image20.png)

After all steps are completed, you now can get “src” and “WebContent” from Git server and put into project “configurator”.  Remember Refresh your project.  Otherwise, Eclipse will not load your new files inside the project.

You now can config Eclipse deploying “configurator” to JBoss server choosing “Add and “Remove”.

[![See react](assets/img/configurator/image21.png)](assets/img/configurator/image21.png)

Add target project to target server.

[![See react](assets/img/configurator/image22.png)](assets/img/configurator/image22.png)

