---
id: apply_changes
title: Apply Changes from Web to iOS
sidebar_label: A Step by Step Guide 
---

## First Step

- Export the published release on 121 Admin
- In the iOS project folder, change directory to EASE->RESOURCES->Testing Data->files->product
- Create the new version for iOS by “copy-paste”-ing previous version of said product
- Commit this change in SourceTree
- After committing said changes, copy the exported published releases onto the directory you are in now, in iOS

> <strong>* The reason why we do this is so that we can track changes from web to iOS since changes to web could possibly ruin the iOS version.</strong>

## Second Step

- You will see the difference between web and iOS version so keep the changes u need for the release and discard the rest 
- Commit and Push 
- Add the new files in XCode by navigating to the same directory but in XCode. 
- Right click on the folder and you'll see the option,“Add Files to Ease”. Click on it then select all the files to be added which are usually highlighted in black. 

![Update View Location](assets/img/ease-mobile/apply_changes/apply_change1.png)

- Don't forget to commit the _"project.pbxproj"_ file as well


## How to Update a Product PDF Manually in iOS

Sometimes, there is a possibility that you can't do the above steps because the difference in web and mobile differ greatly so 
you will need to manually add in your changes so you don't make unnecessary changes to the working files. Usually, the changes are 
needed in the template e.g. Supplementary Policy or Total Distribution Costs or Table of Deductions.

-	Save your original or latest working copy of the pdf you want to change, as a new file with html extension. This will be the temporary file you will use to add your changes.
-	Right click on the document and pick “Format Document”.
-	Make Changes(make sure that double quotes are preceded by a backward slash)
-	Save and select all, then go to any online html compression site to compress the text
-   Save that into the document where you previously copied it from(usually the json file of the latest working version)
