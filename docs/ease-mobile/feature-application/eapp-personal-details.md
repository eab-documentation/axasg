---
id: eapp-personal-details
title: Personal Details
sidebar_label: Personal Details
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Jack Hon |

Personal Details is a simply form for user to display and input client's information.

## Basic Concept
- Data stored in **Redux.store[APPLICATION].application.applicationForm.values**, there are two fields called **proposer(object)** and **insured(array)** under this object, these values are being used at **Personal Details**
- The function called **getTargetProfileDetails()** will automatically retrieve the correct data base on the selected profile
- In this article, **profileData** to be used to refer to the selected profile data
- "Proposer" is the default selected profile each time you go to this page

## Section
1. Singpost Branch Details
    - The section only display if the type of login agent account is "Singpost"
2. Details of [ Proposer / Life Assured ]
    - Only for display purpose(No editable files).
3. Professional Details
4. Residnetial Address
5. Mailing Address
    - This section only display if the answer is "No" of the question "Is your Residential Address same as mailing address?"

## Singpost Branch Details
<table>
    <thead>
        <tr>
            <th>Field Name</th>
            <th>Field Id</th>
            <th>Mandatory</th>
            <th>Data Maping</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Branch</td>
            <td>profileData.branchInfo.branch</td>
            <td>Y</td>
            <td>optionsMap.branches</td>
        </tr>
        <tr>
            <td>Referrer ID</td>
            <td>profileData.branchInfo.bankRefId</td>
            <td>Y</td>
            <td></td>
        </tr>
    </tbody>
</table>

## Details of [ Proposer / Life Assured ]
- Common field(simple logic) would not be mentioned
- Only for display purpose.
<table>
    <thead>
        <tr>
            <th>Field Name</th>
            <th>Field Id</th>
            <th>Logic to display this field</th>
            <th>Logic to display value</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Proposer is a Life Assured</td>
            <td></td>
            <td>select "proposer"</td>
            <td>
                <b>Yes</b>: profileData.profileData.extra.isPhSameAsLa === "Y"<br/>
                <b>No </b>: profileData.profileData.extra.isPhSameAsLa === "N"<br/>
            </td>
        </tr>
        <tr>
            <td>Singapore PR Status</td>
            <td>profileData.prStatus</td>
            <td>profileData.nationality != "N1"<br/><b>*N1 = Singapore</b></td>
            <td>Display <b>profileData.prStatus</b> directly</td>
        </tr>
        <tr>
            <td>ID Document Type</td>
            <td>profileData.idDocType</td>
            <td>profileData.idDocType != "other"</td>
            <td>Remap from <b>optionsMap.IDType</b></td>
        </tr>
        <tr>
            <td>ID Type Name</td>
            <td>profileData.idDocTypeOther</td>
            <td>profileData.idDocType = "other"</td>
            <td>Display <b>profileData.idDocTypeOther</b> directly</td>
        </tr>
        <tr>
            <td>ID Type Name</td>
            <td>profileData.idDocTypeOther</td>
            <td>profileData.idDocType = "other"</td>
            <td>profileData.idDocTypeOther</b></td>
        </tr>
        <tr>
            <td>Email</td>
            <td>profileData.email</td>
            <td>select "proposer"</td>
            <td>Display <b>profileData.email</b> directly</td>
        </tr>
        <tr>
            <td>Mobile</td>
            <td>profileData.mobileNo</td>
            <td>If shield, only select "proposer"</td>
            <td>Display <b>profileData.mobileCountryCode</b> and <b>profileData.mobileNo</b> directly</td>
        </tr>
    </tbody>
</table>

## Professional Details
- Common field(simple logic) would not be mentioned
- All editable fields are mandatory
<table>
    <thead>
        <tr>
            <th>Field Name</th>
            <th>Field Id</th>
            <th>Logic to display this field</th>
            <th>Logic to display value</th>
            <th>Read-Only</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Country of Employer/Business/School</td>
            <td>profileData.organizationCountry</td>
            <td>Only non-shield</td>
            <td>Display <b>profileData.organizationCountry</b> directly</td>
            <td>After sign</td>
        </tr>
        <tr>
            <td>Type of Pass</td>
            <td>profileData.pass</td>
            <td>profileData.nationality != "N1" && profileData.prStatus = "N"</td>
            <td>Remap from <b>optionsMap.pass</b></td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Type of pass (Other)</td>
            <td>profileData.passOther</td>
            <td>profileData.pass = "o"</td>
            <td>Display <b>profileData.passOther</b> directly</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Pass Expiry Date (DD/MM/YYYY)</td>
            <td>profileData.passExpDate</td>
            <td>profileData.nationality != "N1" && profileData.prStatus = "N"</td>
            <td>Display <b>profileData.passExpDate</b> directly</td>
            <td>No</td>
        </tr>
        <tr>
            <td>Occupation (Other)</td>
            <td>profileData.occupationOther</td>
            <td>profileData.occupation = "0912"</td>
            <td>Display <b>profileData.occupationOther</b> directly</td>
            <td>No</td>
        </tr>
    </tbody>
</table>

## Residnetial Details
- Common field(simple logic) would not be mentioned
- If is shield, only proposer will show this section
- All fields can not edit after sign
<table>
    <thead>
        <tr>
            <th>Field Name</th>
            <th>Field Id</th>
            <th>Logic to display this field</th>
            <th>Logic to display value</th>
            <th>On Input</th>
            <th>Mandatory</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Country</td>
            <td>profileData.addrCountry<br/>*This filed can not edit</td>
            <td></td>
            <td>Display <b>profileData.addrCountry</b> directly</td>
            <td></td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Postal Code</td>
            <td>profileData.postalCode</td>
            <td></td>
            <td>Display <b>profileData.postalCode</b> directly</td>
            <td>if the value of the field named "Country" is <b>Signapore (profileData.addrCountry = "R2")</b>, then trigger "getPostalCodeFileName()" function each time value being inputed, if function returns value, then fields name "Block/House Number" and "Street/Road Name" will be refresh to a new value.<br/></td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Postal Code</td>
            <td>profileData.postalCode</td>
            <td></td>
            <td>Display <b>profileData.postalCode</b> directly</td>
            <td>if the value of the field named "Country" is <b>Signapore (profileData.addrCountry = "R2")</b>, then trigger "getPostalCodeFileName()" function each time value being inputed, if function returns value, then fields name "Block/House Number" and "Street/Road Name" will be refresh to a new value.<br/></td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Block/House Number</td>
            <td>profileData.addrBlock</td>
            <td></td>
            <td>Display <b>profileData.addrBlock</b> directly</td>
            <td>See "Postal Code"</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Street/Road Name</td>
            <td>profileData.addrStreet</td>
            <td></td>
            <td>Display <b>profileData.addrStreet</b> directly</td>
            <td>See "Postal Code"</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Unit Number</td>
            <td>profileData.unitNum</td>
            <td></td>
            <td>Display <b>profileData.unitNum</b> and its prefix</td>
            <td>Adding prefix "Unit " to its value, but remove when empty</td>
            <td>No</td>
        </tr>
        <tr>
            <td>City/State</td>
            <td>profileData.addrCity</td>
            <td></td>
            <td>if the value of field named "Country" included in <b>optionsMap.city</b>, then this field would display as Dropdown list, text field otherwise</td>
            <td></td>
            <td>No</td>
        </tr>
        <tr>
            <td>City/State</td>
            <td>profileData.addrCity</td>
            <td></td>
            <td>if the value of field named "Country" included in <b>optionsMap.city</b>, then this field would display as Dropdown list, text field otherwise</td>
            <td></td>
            <td>No</td>
        </tr>
        <tr>
            <td>Others</td>
            <td>profileData.otherResidenceCity</td>
            <td>If profileData.addrCountry = "R52" or (profileData.addrCity = "T110" or profileData.addrCity = "T120")<br/>*R52 = "China - other than Major city", T110 = "OTHER THAN Aceh, West Timor, Sulawesi, Maluku", T120 = "OTHER THAN Yala, Patani, Songkhla, Narathiwat"</td>
            <td>Display <b>profileData.otherResidenceCity</b> directly</td>
            <td></td>
            <td>No</td>
        </tr>
    </tbody>
</table>

### Button inside section Residnetial Details

<table>
    <thead>
        <tr>
            <th>Button Name</th>
            <th>Field Id</th>
            <th>Logic to display this field</th>
            <th>On Press</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Populate Residential Address from Proposer</td>
            <td></td>
            <td>Only non-shield, Only life assured</td>
            <td>Copy proposer's "Postal Code", "Block/House Number" and "Street/Road Name" values to Life Assured's related field</td>
        </tr>
        <tr>
            <td>Is your Residential Address same as mailing address?</td>
            <td>profileData.isSameAddr</td>
            <td>Only proposer</td>
            <td>Press "Yes" to hidden the section "Mailing Address", "No" to display and scroll to "Mailing Address"</td>
        </tr>
    </tbody>
</table>

## Mailing Address
- Common field(simple logic) would not be mentioned
- Only proposer will show this section
- All fields can not edit after sign
- This section only display if the answer is "NO" of the question "Is your Residential Address same as mailing address?"

<table>
    <thead>
        <tr>
            <th>Field Name</th>
            <th>Field Id</th>
            <th>Logic to display this field</th>
            <th>Logic to display value</th>
            <th>On Input</th>
            <th>Mandatory</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Country</td>
            <td>profileData.mAddrCountry<br/>*This filed can edit, default value is "R2" (R2 = Singapore)</td>
            <td></td>
            <td>Display <b>profileData.mAddrCountry</b> directly</td>
            <td></td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Postal Code</td>
            <td>profileData.mPostalCode</td>
            <td></td>
            <td>Display <b>profileData.mPostalCode</b> directly</td>
            <td>if the value of the field named "Country" is <b>Signapore (profileData.mAddrCountry = "R2")</b>, then trigger "getPostalCodeFileName()" function each time value being inputed, if function returns value, then fields name "Block/House Number" and "Street/Road Name" will be refresh to a new value.<br/></td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Block/House Number</td>
            <td>profileData.mAddrBlock</td>
            <td></td>
            <td>Display <b>profileData.mAddrBlock</b> directly</td>
            <td>See "Postal Code"</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Street/Road Name</td>
            <td>profileData.mAddrStreet</td>
            <td></td>
            <td>Display <b>profileData.mAddrStreet</b> directly</td>
            <td>See "Postal Code"</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Unit Number</td>
            <td>profileData.mAddrnitNum</td>
            <td></td>
            <td>Display <b>profileData.mAddrnitNum</b> and its prefix</td>
            <td>Adding prefix "Unit " to its value, but remove when empty</td>
            <td>No</td>
        </tr>
    </tbody>
</table>


## Validation
- Look for the keyword "Mandatory" in this document, or look at the logic in related json files.

## Reference
#### Sources Path
- page: app/routes/Application/component/ApplicationFormPersonalDetails/index.js
- json: appform_dyn_personal.json
- json(shield): appformdyn_personal_shield.json
- optionsMap: EAB-WEB-API/src/assetscache/optionsMap.js
