---
id: eapp-application-summary
title: Application Summary
sidebar_label: Application Summary
---

# Application Summary

1. [Application List](#application-list)
1. [Application Item](#application-item)
1. [Proposed Product List](#proposed-product-list)
1. [Bundle Selection](#bundle-selection)
1. [Proposed For Whom](#proposed-for-whom)

![application-summary-example-1](assets/img/application/application-summary-example-1.png)

## Application List

__Application List__ displays all the applications and quotations, seperating them with different tabs `ALL`, `PROPOSED`, `APPLYING`, `SUBMITTED`, `INVALIDATED(SIGNED)`, `INVALIDATED`.

Quotations are under `PROPOSED`, applications are in `APPLYING`, `SUBMITTED`, `INVALIDATED(SIGNED)`, `INVALIDATED`. `ALL` contains all of them.

__Source Code__ : 

1. Get applications and quotations of this client from DB, "getAppListView" in eab-core-api.

1. Classify the applications and quotations into different tabs, the code is written in frontEnd, different in iOS and Web.

## Application Item

Each Application Item represents an application or a quotation, it displayes the product name, quotation ID or application ID, proposer name and insured name, the basic plan and premium of the application, the rider plans and premium of the applicaiton, created time and total premium etc.

An Application Item under `PROPOSED` has button "APPLY", an Application Item under `APPLYING` has button "CONTINUE". If `Recommendation` is not completed, then "APPLY" and "CONTINUE" buttons are disabled.

You may take a reference on [Apply & Continue](eapp-apply&continue.md).

__Source Code__ : 

1. Server returns flags indicating whether `Recommendation` completed and other aspects, it is written in "getAppListView".

1. "APPLY" or "CONTINUE" buttons on off code is written differently in iOS and Web.

## Proposed Product List

__Proposed Product List__ displays all products of the application and quotations of current bundle.

__Source Code__ : 
The code is written in frontEnd, different in iOS and Web. In Web, it is written in "Pos/Applications/index.js".

## Bundle Selection

A bundle consists of several applications. If one of the applications is submitted or has been signed by the customer, then it is not allowed to add a new application or quotation to the bundle. If a new quotaion is created, then a new bundle is created.

Only one bundle is valid. 

__Source Code__ : 
The code is written in frontEnd, different in iOS and Web. In Web, it is written in "Pos/Applications/index.js".

## Proposed For Whom

A third party case or a Shield case may contains several insureds.

__Source Code__ : 
The code is written in frontEnd, different in iOS and Web. In Web, it is written in "Pos/Applications/index.js".