---
id: eapp-declaration
title: Declaration
sidebar_label: Declaration
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 11 FEB 2019 | Init | Fred Fung |

Declaration is a section of Application Page. The main purpose of this page is to collect user's declaration of his/her assets and identity information.

![alt-text](assets/img/application/Application_Declaration_Layout.png)

***

## Business Logic
Proposer is required to complete the mandatory fields. If the application includes dependants, they are also required to complete the mandatory fields. 
<br>When all the mandatory fields are filled in with answers, the icon of the segment at the top will be changed from grey to green.

![alt-text](assets/img/application/Client_Profile_Segment.png)

This section includes many types of form, proposer and dependants may be asked for different questions.

**!! For the generation of the form, please refer to Dynamic Form Page.**

## Questionnaire
<table>
    <thead>
        <tr>
            <td>S.No</td>
            <td>Question</td>
            <td>EASE Answer format</td>
            <td>Reflexive Question if the Qn is selected as Yes</td>
            <td>Savvy Saver</td>
            <td>Band Aid</td>
            <td>AXA Wealth Invest (CPF)</td>
            <td>AXA Wealth Invest (Cash/SRS)</td>
            <td>Term Protector</td>
            <td>Flexi Protector / Flexi Saver</td>
            <td>MumCare / MumCare Plus</td>
            <td>Life Multiprotect</td>
            <td>Early Saver Plus</td>
            <td>Retire Happy</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="4"><b>
            Residency Declaration (Note: EASE would auto-populate the client's Residency category based on validations against the personal information given)
            </td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td>1a</td>
            <td>
            Singapore Citizen – I am currently: 
            i) residing in Singapore. 
            ii) residing outside Singapore. I have resided outside Singapore continuously for less than 5 years preceding the date of proposal.  
            iii) residing outside Singapore. I have resided outside Singapore continuously for 5 years or more preceding the date of proposal. 
            </td>
            <td>Check boxes</td>
            <td>N/A</td>
        </tr>
        <tr>
            <td>1b</td>
            <td>
            Singapore Permanent Resident / Employment Pass / Work Permit
            i) Have you resided in Singapore for at least 183 days in the last 12 months preceding the date of proposal?
            ii) Do you intend to stay in Singapore on a permanent basis? 
            • If you answered ‘No’ for either 1b(i) or 1b(ii), please complete Q2 below - Foreigner Questions.  
            </td>
            <td>Yes/No Radio Button</td>
            <td>If 'No' --> Foreigner Questionnaire</td>
        </tr>
        <tr>
            <td>1c</td>
            <td>
            Dependant / Student / Long Term Visit Pass
            Have you resided in Singapore for at least 90 days in the last 12 months preceding the date of proposal? 
            • Please complete Q2 below - Foreigner Questions (compulsory).
            </td>
            <td>Yes/No Radio Button</td>
            <td rowspan="2">Foreigner Questionnaire (Compulsory)</td>
        </tr>
        <tr>
            <td>1d</td>
            <td>
            Others - If you do not belong to any of the above categories, please tick here.  
            > Please complete Q2 below - Foreigner Questions (compulsory). 
            > Please submit: 
            • Copy of valid passport including pages of entry stamps issued by Singapore immigration (Compulsory).
            • Mainland China Visitor Declaration Form (only for applicants who are China residents).
            </td>
            <td>Auto-checked</td>
        </tr>
        <tr>
            <td colspan="14" align="center"><b>Details of Previous and Concurrent Insurance Applications</td>
        </tr>
        <tr>
            <td></td>
            <td>Do you have any existing life insurance policies or other investment products?</td>
            <td></td>
            <td rowspan="4" align="center">If yes, provide details in Insurance Information (table Q3).</td>
            <td>v</td>
            <td>x</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td></td>
            <td>Do you have any existing personal accident insurance policies?</td>
            <td>Yes/No Radio Button</td>
            <td>x</td>
            <td>v</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
        </tr>
        <tr>
            <td></td>
            <td>Do you have any pending or concurrent insurance application?</td>
            <td>Yes/No Radio Button</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td></td>
            <td>
            Do you intend to purchase a  policy to replace (in part or full) any existing or recently terminated insurance policy, or other investment product from AXA Insurance Pte Ltd or other Financial Institution ? 
            WARNING NOTE:
            It is usually disadvantageous to replace an existing life insurance policy or other investment product with a new one. Some of the factors to consider:
            1. You may suffer a penalty for terminating the original policy or other investment product.
            2. You may incur transaction costs without gaining any real benefit from replacing the policy or other investment product.
            3. You may not be insurable on standard terms or may have to pay a higher premium in view of higher age or the financial benefits accumulated over the years may be lost.
            In your own interest, we would advise that you consult your own financial consultant before making a final decision. Hear from both sides and make a careful comparison to ensure that you are making a decision that is in your best interest.
            </td>
            <td>Yes/No Radio Button</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            Term Conversion	
            </td>
            <td rowspan="2">x</td>
            <td rowspan="2">x</td>
            <td rowspan="2">x</td>
            <td rowspan="2">x</td>
            <td rowspan="2">x</td>
            <td rowspan="2">v(proposer only)</td>
            <td rowspan="2">x</td>
            <td rowspan="2">v(proposer only)</td>
            <td rowspan="2">x</td>
            <td rowspan="2">x</td>
        </tr>
        <tr>
            <td></td>
            <td>
            Is this application a term conversion? 
            <br>
            <b>
            * 
            Displaying condition:
            Existing policy selected as Yes for LA
            </td>
            <td>Yes/No Radio Button</td>
            <td>
            if yes, display a note in red: 
            Note: Please submit duly completed Service Request Form.
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            Bankruptcy
            </td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
        </tr>
        <tr>
            <td></td>
            <td>
            Are you an undischarged bankrupt?
            </td>
            <td>Yes/No Radio Button</td>
            <td>N/A</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            Politically Exposed Person (PEP)
            (Note: Definition of PEP would appear as Tool Tip in EASE)
            </td>
            <td rowspan="2">v</td>
            <td rowspan="2">x</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
        </tr>
        <tr>
            <td></td>
            <td>
            Are you a current/former Political Exposed Person (PEP) or a family member (parent, step parent, child, step-child, adopted child, spouse, sibling, step-sibling and adopted sibling) or close associate (closely connected either socially or professionally) of a PEP? 
            </td>
            <td>Yes/No Radio Button</td>
            <td>Yes. If Yes, please provide additional details (D1)</td>
        </tr>
        <tr>
            <td colspan="4" align="center"><b>FATCA/CRS</td>
            <td rowspan="4">v(proposer only)</td>
            <td rowspan="4">x</td>
            <td rowspan="4">v(proposer only)</td>
            <td rowspan="4">x</td>
            <td rowspan="4">x</td>
            <td rowspan="4">v(proposer only)</td>
            <td rowspan="4">x</td>
            <td rowspan="4">v(proposer only)</td>
            <td rowspan="4">v(proposer only)</td>
            <td rowspan="4">v(proposer only)</td>
        </tr>
        <tr>
            <td></td>
            <td>Is the Proposer a tax resident of Singapore?</td>
            <td>Yes/No Radio Button</td>
            <td>Yes. If Yes, please provide Taxpayer Identification Number (TIN)</td>
        </tr>
        <tr>
            <td></td>
            <td>Is the Proposer a citizen or tax resident of United States (US)?</td>
            <td>Yes/No Radio Button</td>
            <td>Yes. If Yes, please provide Taxpayer Identification Number (TIN)</td>
        </tr>
        <tr>
            <td></td>
            <td>Other than Singapore and US, is the Proposer a tax resident of other countries/jurisdictions?</td>
            <td>Yes/No Radio Button</td>
            <td>Yes. If Yes, provide table for additional details (D2)</td>
        </tr>
        <tr>
            <td colspan="4" align="center"><b>Trusted Individual</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
        </tr>
        <tr>
            <td></td>
            <td>
           I, <NAME> (Trusted Individual’s name - eFNA) of NRIC/Passport: <IC NUMBER> (Trusted Individual’s IC – eFNA) acknowledge that I have fulfilled the above definition of a Trusted Individual and I am a Trusted Individual to <NAME OF CLIENT> (Client’s name – from eFNA/eAPP). I confirm that the Financial Consultant has conversed in <LANGUAGE> (from eAPP) to conduct the Financial Needs Analysis and explained the recommendations and application form(s). I confirm that I understand the explanations and recommendations made by the Financial Consultant and I have translated and explained them to <NAME OF CLIENT> (Client’s name). 
            </td>
            <td>Check box</td>
            <td>
            No. User would need to input language in eApp. 
            (This Section is pre-populated if the Proposer is a Selected Client as per the pre-defined logics built in EASE)
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center"><b>Source of Funds</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td>1</td>
            <td>
            <b>
            Source of Payment 
            </b>
            Please select the origin of the funds used for this application.
            </td>
            <td>Singapore/ Foreign Radio Button</td>
            <td rowspan="4">Yes</td>
            <td rowspan="4" colspan="10" align="center">
            1. System to display Question 1 for all applications
            <br>
            2. System to display Question 2, 3 and 4 for all funds based on CR 40
            <br>
            3. System to display Question 3 and 4 for all foreign funds
            Field validation: Reference to “Declaration Validation Rule” table D3
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>
            <b>
            Payor’s Details
            </b>
             Who is paying the insurance premium for this application?
            </td>
            <td>Check box</td>
        </tr>
        <tr>
            <td>3</td>
            <td>
            <b>
            Source of Wealth
            </b>
             Refers to the origin of funds/assets used for this investment.
            </td>
            <td>Check box</td>
        </tr>
        <tr>
            <td>4</td>
            <td>
            <b>
            Source of Funds
            </b>
             Refers to the origin of your total assets.
            </td>
            <td>Check box</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            Declaration & Authorisation
            </td>
            <td colspan="10">
        </tr>
        <tr>
            <td></td>
            <td>For Life Insurance products</td>
            <td>N/A</td>
            <td>Declaration_Life</td>
            <td>v</td>
            <td>x</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td></td>
            <td>For Accident insurance products</td>
            <td>N/A</td>
            <td>Declaration_Accident</td>
            <td>x</td>
            <td>v</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            Conditional Premium Deposit	
            </td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
            <td rowspan="2">v</td>
        </tr>
        <tr>
        <td></td>
        <td>See attached for Full CPD.</td>
        <td>N/A</td>
        <td>Click to see CPD</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            Roadshow(Only on UI, not eApp form)
            </td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
            <td>v</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            CPFIS Special Account - Authorisation For Withdrawal
            </b>
            (Only on eApp form, not on UI, before Declaration)	
            </td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>Only for "CPF-SA"</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
            <b>
            CPFIS Special Account - Authorisation For Withdrawal
            </b>
            (Only on eApp form, not on UI, before Declaration)	
            </td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>Only for CPF-OA and CPF-SA</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
        </tr>
    </tbody>
</table>




