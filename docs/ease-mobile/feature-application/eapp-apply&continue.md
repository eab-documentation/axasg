---
id: eapp-apply&continue
title: Apply & Continue
sidebar_label: Apply & Continue
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 21 JAN 2019 | Init | Fred Fung |

There are two methods to access Application Page.

## 1. Apply

If user finished recommendation in the first time(please refer to Recommendation document for the flow of Recommendation), it is able to access Application Page through pressing "APPLY" button.

![alt-text](assets/img/application/Application_Apply_Screen.png)

**Mechanism**
When "APPLY" button is pressed, the app will start 


## 2. Continue

Once user applied the application, the status of the application type will become as "application" instead of "quotation". It is able to access Application Page through pressing "CONTINUE" button.

![alt-text](assets/img/application/Application_Continue_Screen.png)
***

## Technical

For the source code relates to this page, please refer to 

- Component @ AXA-SG-APP/app/routes/Applications/components/ApplicationItemHeader/index.js
- Styles @ AXA-SG-APP/app/routes/Applications/components/ApplicationItemHeader/styles.js
- Reducers @ eab-web-api/src/reducers/application.js
- Saga @ eab-web-api/src/saga/application.js
- Trigger condition @ eab-web-api/src/utilities/trigger.js
- Validation @ eab-web-api/src/utilities/validation.js & eab-web-api/src/utilities/application.js
