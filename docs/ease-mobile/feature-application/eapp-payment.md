---
id: eapp-payment
title: Payment
sidebar_label: Payment
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Jack Hon |

## Basic Concept
- This page divided by shield and nono-shield, data structure is different, could be very confused sometimes.
- Non-shield
    - Data stored in **Redux.store[PAYMENT_AND_SUBMISSION].payment**.
- Shield
    - Even throught the original data stored in **Redux.store[APPLICATION].application.payment**, but i desided to copy this store to **Redux.store[PAYMENT_AND_SUBMISSION].payment**, so that **Redux.store[PAYMENT_AND_SUBMISSION]** could be reusable.


## Section
1. Premium Details
    - Non-shield
        - Only for display purpose(No editable files).
    - Shield
        - The sub-section **Subsequent Premium Payment Method** showing as checkbox inside this section.
2. Initial payment method
    - Non-shield
        - All payment method showing inside dropdown box.
    - Shield
        - Only show payment method dropdown box when rider selected(See Quotation).
3. Subsequent Premium Payment Method
    - Non-shield
        - Only one checkbox and one warning message(check the checkbox) showing at this section.
        - Hidden after submission.
    - Shield
        - This section only exist inside the section **Premium Details** as sub-section.

## Reference
#### Sources Path
- page: app/routes/Application/component/PaymentAndSubmission/payment.js
- page(shield): app/routes/Application/component/PaymentAndSubmissionShield/payment.js
- json: paymentTmpl.json
- json(shield): paymentTmpl_shield.json