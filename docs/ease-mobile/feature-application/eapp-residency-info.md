---
id: eapp-residency-info
title: Residency & Insurance Info
sidebar_label: Residency Info
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 21 JAN 2019 | Init | Fred Fung |

Residency & Insurance Info is a section of Application Page. The main purpose of this page is to collect user's data about residency and insurance.

![alt-text](assets/img/application/Application_Residency&Insurance_Info_Layout.png)

***

## Business Logic
Proposer is required to complete the mandatory fields. If the application includes dependants, they are also required to complete the mandatory fields. 
<br>When all the mandatory fields are filled in with answers, the icon of the segment at the top will be changed from grey to green.

![alt-text](assets/img/application/Client_Profile_Segment.png)

This section includes three types of form, proposer and dependants share same questions (except of indicating near the question)

**!! For the generation of the form, please refer to Dynamic Form Page.**


## Form Type
1. **Standard** (User chose all products instead of AXA Band-Aid and AXA Shield)

    - Residency Questions  
        - Singaporean Type (profile's nationality === Singapore)
        - Singapore PR Type (profile's Singapore PR Status === Yes)
        - Pass Type  (profile's Singapore PR Status === No && profile's Type of Pass !== "" )

    - Foreigner Questions (User answered "No" in either one non-Singaporean-type of Residency Questions)
        - Details of Previous and Concurrent Policies
    - saTable
        - Existing Policies (not able to edit option of this question)
        - Pending Applications
        - Policy to be replaced (not able to edit option of this question)

JSON Referrance: axa-sg-app/ios/files/appform_dyn_residency_shield.json
<br>
<br>


2. **Band-aid** (User chose AXA Band-Aid)
No difference of question displaying on the screen, except of some wordings and variables taking.

JSON Referrance: axa-sg-app/ios/files/appform_dyn_residency_shield_acc.json
<br>
<br>

3. **Shield** (User chose AXA Shield)
The title showing on the section list will become as "REPLACEMENT OF POLICY"
![alt-text](assets/img/application/Application_Replacement_of_Policy_Layout.png)
    - REPLACEMENT OF POLICIES Questions (not able to edit option of this question, it can only be changed from Recommendation Page-> "Is this proposal to replace or intended to replace an existing Integrated Shield Plan?") 

JSON Referrance: axa-sg-app/ios/files/appform_dyn_residency_shield_shield.json
<br>
<br>


## Technical
For the source code relate to this page, please refer to 

- Component @ AXA-SG-APP/app/components/DynamicViews/index.js ,<br>AXA-SG-APP/app/routes/Applications/components/ApplicationForm/index.js  , <br>AXA-SG-APP/app/routes/Applications/components/ApplicationFormDynamic/index.js
- Styles @ AXA-SG-APP/app/components/DynamicViews/styles.js,
<br>AXA-SG-APP/app/routes/Applications/components/ApplicationForm/styles.js  , <br>AXA-SG-APP/app/routes/Applications/components/ApplicationFormDynamic/styles.js
- Reducers @ eab-web-api/src/reducers/application.js
- Saga @ eab-web-api/src/saga/application.js

For the mechanism to generate form, please refer to the Dynamic Form document
