---
id: eapp-submission
title: Submission
sidebar_label: Submission
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Jack Hon |

## Basic Concept
- This page divided by shield and nono-shield, data structure is different, could be very confused sometimes.
- Non-shield
    - Data stored in **Redux.store[PAYMENT_AND_SUBMISSION].submission**.
- Shield
    - Even throught the original data stored in **Redux.store[APPLICATION].application.submission**, but i desided to copy this store to **Redux.store[PAYMENT_AND_SUBMISSION].submission**, so that **Redux.store[PAYMENT_AND_SUBMISSION]** could be reusable.


## Section
1. Submission
    - Non-shield
        - Only status message and submit button showing
    - Shield
        - Status message and submit button showing
        - After submit, status message will become table showing submitted status

## Reference
#### Sources Path
- page: app/routes/Application/component/PaymentAndSubmission/submission.js
- page(shield): app/routes/Application/component/PaymentAndSubmissionShield/submission.js
- json: submissionTmpl.json
- json(shield): submissionTmpl_shield.json