---
id: eapp-dynamic-form-intro
title: Dynamic Form Intro
sidebar_label: Dynamic Form Intro
author: Carlton
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 23 JAN 2019 | Init | Carlton Chen |

# What's In This Document

This document describes how eApp Dynamic Form including "eApp-Residency", "eApp-Insurability", "eApp-Declaration" works in EASE iOS.

The replated pages are displayed after user clicking "Apply" button of "Application Summary" page.

EASE-iOS is following the logics of EASE-Web, if you are not sure about the logics in iOS, please read EASE-Web source code to find out.

**We use an newly generated "Term Protector" application as example.**

# Structure

1. [Template](eapp-dynamic-form-template.md) is a Json file indicating what components does an application display in eApp.
1. [Values](eapp-dynamic-form-values.md) stores the values of eApp.
1. [Dynamic Engine](eapp-dynamic-form-dynamic-engine.md) generates UI page with `Template` and `Values`

![eapp-dynamic-form-structure](assets/img/application/eapp-dynamic-form-structure.png)