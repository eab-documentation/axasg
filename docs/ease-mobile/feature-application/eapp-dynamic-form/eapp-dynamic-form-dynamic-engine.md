---
id: eapp-dynamic-form-dynamic-engine
title: Dynamic Engine
sidebar_label: Dynamic Engine
author: Carlton
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 23 JAN 2019 | Init | Carlton Chen |

# Dynamic Engine

__Dynamic Engine__ generates eApp form pages with __Template__ and __Values__. The source code is in __/DynamicViews/index.js__.

It has two main functions:

1. [Generate UI Components](#generate-ui-components)
1. [Trigger](#trigger)
1. [Validation](#validation)

## Generate UI Components

__Dynamic Engine__ requires these properties,

| Property | Description |
| ------ | ----------- |
| template | The indication to Dynamic Engine about what kind of component to show |
| rootValues | A big data block containing information of eApp form |
| path | The path of a component's value in rootValues |
| onUpdateValues | The function is called when user input something on any component |
| error | The data block containing validation information of eApp form |

There are two places call __Dynamic Engine__ in source code. 

1. __/ApplicationFormDynamic/index.js__

    __ApplicationFormDynamic__ is the class to generate 3 eApp form pages "eApp-Residency", "eApp-Insurability", "eApp-Declaration" by calling __Dynamic Engine__. This class get the corresponding template by product and differentiate the path for proposer and insureds.

    __onUpdateValues__: __ApplicationFormDynamic__ passes a function __updateApplicationFormValues__ which will sends the newly input data to saga to do validation and save the newly data into Redux store.

1. __DynamicViews/Table/dialog.js__

    Table dialog is one of the components generated by template, it is dynamic.

    ![eapp-dynamic-form-example-2](assets/img/application/eapp-dynamic-form-example-2.png)

    __onUpdateValues__: __Table Dialog__ passes a function which will update the newly input data in local state, until "Done" button is clicked to save the dialog data into Redux store.

<br>

## Trigger

__Trigger__ defines whether a component should be __`displayed, disabled, mandatory`__ or not in specific condition.

In the example below, it means this block is displayed only if it fullfills the condition that __LIFESTYLE01__ equals to __"Y"__.

The source code of __Trigger__ is in __utilities/trigger.js__ in __eab-web-api__.

```Json
{
    "type":"block",
    "trigger":{
      "id":"LIFESTYLE01",
      "type":"showIfEqual",
      "value":"Y"
    },
    "items":[
    {
      "type":"HBOX",
      "items":[
        {
          "id":"LIFESTYLE01a",
          "mandatory":true,
          ...
```

<br>

## Validation

### Mandatory Check

Each time user applies a new application, continues the application or updates a value in eApp form, function __initValidateApplicationFormError__ will check all the mandatory fields whether they have valid values. The program uses a boolean called __"hasError"__ to decide whether a component fulfills the validation conditions.

From the capture below, we find "__HW01__" is in the path __"application.error.applicationForm.values.proposer.insurability.HW01.hasError"__.

![eapp-dynamic-form-error-example-1](assets/img/application/eapp-dynamic-form-error-example-1.png)

Compare to __HW01__'s value path __"application.application.applicationForm.values.proposer.insurability.HW01"__, we find they share the same part of path __"applicationForm.values.proposer.insurability.HW01"__.

The advantage is a component can get its value and error with a same path, it is easier for maintenance.

### Limitation Check

Some components have limitation check. Take text component as an example, we can see the example below has "max", "min" and "decimal".

```Json
{
  "id": "HW01",
  "type": "text",
  "subType": "number",
  "mandatory":true,
  "max": 3,
  "min":0,
  "decimal": 2,
  "title": "Height (m, meter)"
}
```

In this case, the limitation code is written in __DynamicViews/TextField/index.js__. The limitation rule varies from component to component.