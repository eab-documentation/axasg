---
id: eapp-pdf-form-generation
title: PDF Form Generation
sidebar_label: PDF Form Generation
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1 | 21 JAN 2019 | Init | Felix Chum |
| 1.1 | 21 JAN 2019 | Edited & Reviewed | Keith Lam |

## Prerequisite

> - All eApp related files imported properly

## Introduction

The eAPP form is a template system that reveal the value input from the Dynamic Form. The system share similar logic with BI Generator while its data is user customizable. The template files also predefined by xslt and html string weap in json, and imported to couchbase when the app initalize.

The generator will choose the form dynamically base on JSON indication. Inside the template, it's involved some condition to show some of the fields or not. The page break will be automatically inserted if needed
___

## Hierarchy

```plain
PdfBuilder
| - EABPrintPageRenderer.h
| - EABPrintPageRenderer.m
| - PdfBuilder.h
| - PdfBuilder.m
| - PdfGenerator.h
| - PdfGenerator.m
| - PdfMerger.h
| - PdfMerger.m
```

___

## eApp Form Mapping Structure

The generator will reterive the data from ```application_from_mapping.json``` and choose the corresponding Dynamic Form and template.
> Refer to Dynamic Form Section for more details.

Only the following part are used to generate the eApp template, and the common fields are mentioned.

**application_from_mapping.json**

```javascript
"appFormTemplate": {

    // Main Template will be used
    "main": "appform_report_main",

    // Array of templates will be used
    "template": [
        "appform_report_common",
        ...
    ],

    // Some special options
    "properties": {
        "dollarSignForAllCcy": true
        },
    ...
},
```

**Main Template / Template**

```javascript
{
    // All the css styles used in the templates
    "style": "CSS Styles",

    // For identification
    "pdfCode": "appform_report_main",

     // Main content of the template, most of the viewable content are in here
    "template": {
        "en": [
            "template array of english version"
            ],
        "zh-Hant": [
            "中文版內容"
            ]
        ...
    },

    // Something show at the end of that page, sperated for different language, showing the page number, etc.
    "footer": {
            "en": "content of english version",
            "zh-Hant": "中文版內容",
            ...
 },
}
```

___

## Debugging

> Make sure Debug Mode is enabled

As the engine will compile the xslt and xml to generate the html file, then pdf file. XML and XSLT retrieved can be reviewed here by adding break point or NSLog.

**PDFGenerator.m**

```objective-c
RCT_EXPORT_METHOD(convertToHTML:(NSString *)xml
                  xslt:(NSString *)xslt
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  NSString *html = [PdfHelper transformXml:xml template:xslt];
  resolve(@[html]);
}
```

___

## Known Incompatible Issues and Changes

Some fields used on WEB are not workable in iOS, and there are replacement methods to handle it.

### Abnormal Page Break

It is found that some HTML tags or attributes are not workable and behave unproperly in iOS but works fine on WEB. 

**Misconduct tags / attributes**

```html
1. <br />
2. <p style="text-align: center";></p>
```

The unproperly generation may due to the ```xsltHandler``` library limitation, but upon the docs written date, the abovementioned tags / attributes should be avoid in eApp.

**Alternatively, the following tags / attributes can be used to provide similar experience**

```html
// Replacement for <br />
<div style="height:10px";></div>

// Replacement for <p style="text-align: center";></p>
.centralize{
    align-items: center;
    align-content: center;
    display: flex;
    justify-content: center;
    }
```

### Control Page Break

Sometimes the sections may require break or not to break by requirements. To control the section become breakable or not, following code can be used.

```html
<!-- Use the following code to force a pagebreak -->
<div style="page-break-inside:auto;">

<!-- Use the following code to avoid pagebreak -->
<div style="page-break-inside:avoid;">
```

### Incorrect Page Size

All reports or printing use A4 size in cm except the eApp form. The eApp form is slightly large by 10% in width and height due to the content is too large.

```javascript
// since eapp form use different papaer size, check and set the pageSize
CGSize pageSize = kPageSizeA4;
if ([_options[@"type"] isEqualToString:@"EAPP"]) {
    pageSize = kPageSizeA4_eapp;
}
```

___

## Remarks

___