---
id: eapp-insurability
title: Insurability Information
sidebar_label: Insurability Information
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Fred Fung |

Insurability Information is a section of Application Page. The main purpose of this page is to collect user's data to assess oneself's insurability.

![alt-text](assets/img/application/Application_Insurability_Information_Layout.png)

***

## Business Logic
Proposer is required to complete the mandatory fields. If the application includes dependants, they are also required to complete the mandatory fields. 
<br>When all the mandatory fields are filled in with answers, the icon of the segment at the top will be changed from grey to green.

![alt-text](assets/img/application/Client_Profile_Segment.png)

This section includes many types of form, proposer and dependants may be asked for different questions.

**!! For the generation of the form, please refer to Dynamic Form Page.**

## Form Type
There are **four types** of questionnaire form in Insurability Section, if user chose different product & rider, the system would also provide questionnaire form.

1. Full UW
    - Early Stage CritiCare
    - Life MultiProtect
    - AXA Early Saver Plus (with following rider)
        - CI PremiumEraser
        - Premium Waiver (CIUN)  
    - Term Protector 
    - Term Protector Prime
    - INSPIRE FlexiSaver
    - INSPIRE FlexiProtector
2. GIO (which does not include Insurability Information Section in current release)
    - Savvy Saver (Only Basic)
    - Retire Happy Plus (Only Basic)
    - AXA Early Saver Plus (Only Basic)
    - AXA Wealth Invest (CPF) (Only Basic)
    - AXA Wealth Invest (Cash/SRS) (Only Basic)
    - Band Aid (Only Basic)
    - AXA Wealth Treasure (Only Basic)
3. SIO 
    - Savvy Saver (with following rider)
        - PremiumEraser Total rider (LA)
    - Retire Happy Plus (with following rider)
        - PremiumEraser Total rider (LA)
4. SIO(PA)
    - Band Aid (with following rider)
        - Accident Medical Reimbursement Benefit (AMR) 
        - Broken Bones Benefit
        - Daily Accident Hospitalisation Income & ICU Benefit 
        - Home Modification Reimbursement Benefit 
        - Medical Expenses 
        - Weekly Indemnity 

## Quesionnaire

 ## 1. HEIGHT_WEIGHT
![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEIGHT_WEIGHT.png)
<table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>HW01</td>
            <td>Height (m, meter)</td>
            <td>Not optional</td>
            <td>Full uw / SIO type questionnaire</td>
            <td>Numberic</td>
            <td>4</td>
            <td>2 decimal, should not be 0 (error message if =0: Value must be greater than 0)</td>
        </tr>
        <tr>
            <td>2</td>
            <td>HW02</td>
            <td>Weight (kg, kilogram)</td>
            <td>Not optional</td>
            <td>Full uw / SIO type questionnaire</td>
            <td>Integer</td>
            <td>3</td>
            <td>0 decimal, should not be 0 (error message if =0: Value must be greater than 0)</td>
        </tr>
        <tr>
            <td>3</td>
            <td>HW03</td>
            <td>Any weight change in the last 12 months? (kg)</td>
            <td>Optional</td>
            <td>Full uw / SIO type questionnaire</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>4</td>
            <td>HW03a</td>
            <td>Weight change</td>
            <td>Optional</td>
            <td>HW03 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Gain / Lost</td>
        </tr>
        <tr>
            <td>5</td>
            <td>HW03b</td>
            <td>How many kg?</td>
            <td>Optional</td>
            <td>HW03 = Yes</td>
            <td>Integer</td>
            <td>2</td>
            <td>0 decimal, should not be 0 (error message if =0: Value must be greater than 0)</td>
        </tr>
        <tr>
            <td>6</td>
            <td>HW03c</td>
            <td>Reason of weight change</td>
            <td>Optional</td>
            <td>HW03 = Yes</td>
            <td>Dropdown</td>
            <td>100</td>
            <td>Dropdown list as below: 
                Diet Control, 
                Exercising, 
                Pregnancy,
                Post delivery,
                Other:  [Free Text, alphanumeric, size 100]
            </td>
        </tr>
    </tbody>
</table>

 ## 2. INS_HISTORY
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_INS_HISTORY.png)
 <table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>INS01</td>
            <td>1. Have you ever made an application or application for reinstatement of a life, disability, accident, medical or critical illness insurance which has been accepted with an extra premium or on special terms, postponed, declined, withdrawn or is still being considered?</td>
            <td></td>
            <td>Always</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>2</td>
            <td>INS01a</td>
            <td>Name of Insurance Company</td>
            <td>Not optional</td>
            <td>INS01 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>3</td>
            <td>INS01b</td>
            <td>Type of Couverage</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Display Text</td>
            <td></td>
            <td>At least one of the check box is selected</td>
        </tr>
        <tr>
            <td></td>
            <td>INS01b1</td>
            <td>Life</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS01b2</td>
            <td>Total Permanent Disability</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS01b3</td>
            <td>Critical Illness</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS01b4</td>
            <td>Accident</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS01b5</td>
            <td>Hospitalisation & Surgical Benefit</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>4</td>
            <td>INS01c</td>
            <td>Date incurred (MM/YYYY)</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of MM + Dropdown of YYYY</td>
        </tr>
        <tr>
            <td>5</td>
            <td>INS01d</td>
            <td>Condition of Special Terms</td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of: 
                Medical 
                Residential
                Financial
                Others
            </td>
        </tr>
        <tr>
            <td>6</td>
            <td>INS01e</td>
            <td>Decision & Detailed Reason(s) of special terms </td>
            <td></td>
            <td>INS01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>11</td>
            <td>INS02</td>
            <td>2. Are you presently receiving a disability benefit or incapable for work or have you ever made or intend to make any claim against any insurer for disability, accident, medical care, hospitalisation, critical illness and/or other benefits?</td>
            <td></td>
            <td>Always</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>12</td>
            <td>INS02a</td>
            <td>Name of Insurance Company</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>13</td>
            <td>INS02b</td>
            <td>Type of the Claim</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Display Text</td>
            <td></td>
            <td>At least one of the check box is selected</td>
        </tr>
        <tr>
            <td></td>
            <td>INS02b1</td>
            <td>Total Permanent Disability</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS02b2</td>
            <td>Critical Illness</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS02b3</td>
            <td>Accident</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS02b4</td>
            <td>Hospitalisation & Surgical Benefit</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS02b5</td>
            <td>Others</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>INS02b5-OTH</td>
            <td>Placeholder : Others</td>
            <td></td>
            <td>INS01b5 is selected</td>
            <td>Text Field</td>
            <td>20</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>14</td>
            <td>INS02c</td>
            <td>Date incurred (MM/YYYY)</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of MM + Dropdown of YYYY</td>
        </tr>
        <tr>
            <td>15</td>
            <td>INS02d</td>
            <td>Medical Condition</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>16</td>
            <td>INS02e</td>
            <td>Claim Details (Duration of hospitalisation /offwork, treament, follow-up etc)</td>
            <td></td>
            <td>INS02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
    </tbody>
</table>

 ## 3. LIFESTYLE
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_LIFESTYLE_1.png)
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_LIFESTYLE_2.png)
 <table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>LIFESTYLE01</td>
            <td>1. Have you smoked or used any tobacco, nicotine or smokeless tobacco products (e.g. cigarettes, cigar, e-cigarettes or pipes) within the past 12 months?</td>
            <td></td>
            <td>Full uw / SIO type questionnaire && Life Assured's Smoking Status = Smoker</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No (The field is disabled. Always polulate the answer from eBI)</td>
        </tr>
        <tr>
            <td>2</td>
            <td>LIFESTYLE01a</td>
            <td>Type of product (eg. Cigarettes, cigar, e-Cig, nicotine patch, etc.)</td>
            <td></td>
            <td>LIFESTYLE01 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Option:
               <br> - Cigarettes
               <br> - cigar
               <br> - e-Cig
               <br> - nicotine patch
               <br> - Other
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>LIFESTYLE01a_OTH</td>
            <td>Text Box for "Other"</td>
            <td></td>
            <td>LIFESTYLE01a = Other</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>4</td>
            <td>LIFESTYLE01b</td>
            <td>Number of products smoked per day</td>
            <td></td>
            <td>LIFESTYLE01 = Yes</td>
            <td>Integer</td>
            <td>3</td>
            <td></td>
        </tr>
        <tr>
            <td>5</td>
            <td>LIFESTYLE01c</td>
            <td>Number of years smoked</td>
            <td></td>
            <td>LIFESTYLE01 = Yes</td>
            <td>Integer</td>
            <td>2</td>
            <td></td>
        </tr>
        <tr>
            <td>6</td>
            <td>LIFESTYLE02</td>
            <td>2. Do you consume alcohol? If yes, how much alcohol do you drink per week on average? 
                <br> ** Note: ONE standard unit of alcoholic drink equates to Beer 330ml/can, Wine 125ml/glass, or Spirits 30ml/cup. **
            </td>
            <td></td>
            <td>Full uw / SIO type questionnaire</td>
            <td>Radio Group</td>
            <td></td>
            <td>YES / NO</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE02_LABEL</td>
            <td>How many alcoholic drinks do you drink per week, on average? </td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Display Text</td>
            <td></td>
            <td>At least one of the check box is selected</td>
        </tr>
        <tr>
            <td>7</td>
            <td>LIFESTYLE02a_1</td>
            <td>Beer (330 ml/can)</td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>8</td>
            <td>LIFESTYLE02a_2</td>
            <td>cans</td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Integer</td>
            <td>3</td>
            <td>Enable when LIFESTYLE02a_1 is checked</td>
        </tr>
        <tr>
            <td>9</td>
            <td>LIFESTYLE02b_1</td>
            <td>Wine (125ml/glass)</td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td>Dropdown of: 
                Medical 
                Residential
                Financial
                Others
            </td>
        </tr>
        <tr>
            <td>10</td>
            <td>LIFESTYLE02b_2</td>
            <td>glasses</td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Integer</td>
            <td>3</td>
            <td>Enable when LIFESTYLE02b_1 is checked</td>
        </tr>
        <tr>
            <td>11</td>
            <td>LIFESTYLE02c_1</td>
            <td>Spirits (30ml/cup)</td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>12</td>
            <td>LIFESTYLE02c_2</td>
            <td>cups</td>
            <td></td>
            <td>LIFESTYLE02 = Yes</td>
            <td>Integer</td>
            <td>3</td>
            <td>Enable when LIFESTYLE02c_1 is checked</td>
        </tr>
        <tr>
            <td>13</td>
            <td>LIFESTYLE03</td>
            <td>3. Have you ever used any habit forming drugs or narcotics or been treated for drug habits?</td>
            <td></td>
            <td>Full uw / SIO type questionnaire</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>14</td>
            <td>LIFESTYLE03a</td>
            <td>Substances used</td>
            <td></td>
            <td>LIFESTYLE03 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>15</td>
            <td>LIFESTYLE03b</td>
            <td>Date Commenced (MM/YYYY)</td>
            <td></td>
            <td>LIFESTYLE03 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of MM + Dropdown of YYYY</td>
        </tr>
        <tr>
            <td>16</td>
            <td>LIFESTYLE03c</td>
            <td>Date Ceased (MM/YYYY)</td>
            <td></td>
            <td>LIFESTYLE03 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of MM + Dropdown of YYYY</td>
        </tr>
        <tr>
            <td>17</td>
            <td>LIFESTYLE03d</td>
            <td>Details (e.g. Treatment, Any other medical condition, Any relapses/complications etc)</td>
            <td></td>
            <td>LIFESTYLE03 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>18</td>
            <td>LIFESTYLE04</td>
            <td>4. Do you have any intention of residing outside Singapore for more than 6 months?</td>
            <td></td>
            <td>Full uw type questionnaire</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>19</td>
            <td>LIFESTYLE04a</td>
            <td>Country</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Master Country List</td>
        </tr>
        <tr>
            <td>20</td>
            <td>LIFESTYLE04b</td>
            <td>City</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>21</td>
            <td>LIFESTYLE04c</td>
            <td>Duration (how many days per year)</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Integer</td>
            <td>5</td>
            <td></td>
        </tr>
        <tr>
            <td>22</td>
            <td>LIFESTYLE04d</td>
            <td>Reason</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Display Text</td>
            <td></td>
            <td>At least one of the check box is selected</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d1</td>
            <td>Permanent Residence</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d2</td>
            <td>Business / Work</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d3</td>
            <td>Study</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d4</td>
            <td>Country of birth</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d5</td>
            <td>Place where family members or relatives residing</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d6</td>
            <td>Immigration</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d7</td>
            <td>Visit family/relatives/friends</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d8</td>
            <td>Others</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d8_OTH</td>
            <td>Please specify reason</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>23</td>
            <td>LIFESTYLE05</td>
            <td>Do you participate or intend to participate in any hazardous activities related to your occupation or recreation such as diving, mountaineering, skydiving, parachuting, hang gliding, motor sports or aviation (excluding flying as a passenger on a regular scheduled airline)?</td>
            <td></td>
            <td>Full uw / SIO(PA) type questionnaire</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>24</td>
            <td>LIFESTYLE05a</td>
            <td>Type of hazardous activities</td>
            <td></td>
            <td>LIFESTYLE05 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Option:
                <br> - Diving/scuba diving
                <br> - Mountaineering
                <br> - Skydiving
                <br> - Parachuting
                <br> - Hand gliding
                <br> - Motor sports
                <br> - Aviation (other than fare-paying passenger) 
                <br> - Others
            </td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE05_OTH</td>
            <td>Placeholder: Others</td>
            <td></td>
            <td>LIFESTYLE05a = Other</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>25</td>
            <td>LIFESTYLE05b</td>
            <td>Frequency (per year)</td>
            <td></td>
            <td>LIFESTYLE05 = Yes</td>
            <td>Integer</td>
            <td>5</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>26</td>
            <td>LIFESTYLE05c</td>
            <td>Details (e.g. Expertise, level of participation, experience etc)</td>
            <td>Yes</td>
            <td>LIFESTYLE05 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06</td>
            <td>4. Do you travel outside Singapore for more than 3 months in a year (other than
            for holidays or studies)? If yes, please provide details of country/city, frequency and
            duration of the trips?
            </td>
            <td></td>
            <td>Always</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06a</td>
            <td>Country</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Master Country List</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06b</td>
            <td>City</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06c</td>
            <td>Duration (how many days per year)</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Integer</td>
            <td>5</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d</td>
            <td>Reason</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Display Text</td>
            <td></td>
            <td>At least one of the check box is selected</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d1</td>
            <td>Permanent Residence</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d2</td>
            <td>Business / Work</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d3</td>
            <td>Study</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d4</td>
            <td>Country of birth</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d5</td>
            <td>Place where family members or relatives residing</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d6</td>
            <td>Immigration</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d7</td>
            <td>Visit family/relatives/friends</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d8</td>
            <td>Others</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE06d8_OTH</td>
            <td>Please specify reason</td>
            <td></td>
            <td>LIFESTYLE06 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
    </tbody>
</table>

 ## 4. FAMILY_HISTORY
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_FAMILY_HISTORY.png)
 <table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>FAMILY01</td>
            <td>Has your biological mother, father, or any sister or brother been diagnosed prior to age 60 with any of the following?  Cancer, heart disease, stroke, diabetes, Huntington's disease, polycystic kidney disease, Multiple Sclerosis, Alzheimer's or any other inherited conditions.</td>
            <td></td>
            <td>Always</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td>2</td>
            <td>FAMILY01a</td>
            <td>Relationship</td>
            <td></td>
            <td>FAMILY01 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Option:
               <br> - Mother
               <br> - Father
               <br> - Brother
               <br> - Sister
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>FAMILY01b</td>
            <td>Medical Condition</td>
            <td></td>
            <td>FAMILY01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>4</td>
            <td>FAMILY01c</td>
            <td>Age of Onset</td>
            <td></td>
            <td>FAMILY01 = Yes</td>
            <td>Integer</td>
            <td>3</td>
            <td>Must be < 60</td>
        </tr>
    </tbody>
</table>

 ## 5. REGULAR_DR
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_REGULAR_DR.png)
 <table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>REG_DR01</td>
            <td>Do you have a regular doctor?</td>
            <td></td>
            <td>Always</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No </td>
        </tr>
        <tr>
            <td>2</td>
            <td>REG_DR01a</td>
            <td>Name of Doctor</td>
            <td></td>
            <td>REG_DR01 = Yes</td>
            <td>Text Field</td>
            <td>50</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>3</td>
            <td>REG_DR01b</td>
            <td>Name & Address of clinic/hospital</td>
            <td></td>
            <td>REG_DR01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>4</td>
            <td>REG_DR01c</td>
            <td>Date of last consultation</td>
            <td></td>
            <td>REG_DR01 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td>Cannot be future date</td>
        </tr>
        <tr>
            <td>5</td>
            <td>REG_DR01d</td>
            <td>Details of Consultations (including Reason, Diagnosis, Treatement, etc.)</td>
            <td></td>
            <td>REG_DR01 = Yes</td>
            <td>Dropdown</td>
            <td>1000</td>
            <td>Dropdown list as below: 
                <br> - Minor ailments (e.g. cough/flu/sore throat/fever) 
                <br> - Other: [Free Text, alphanumeric, size 1000]
            </td>
        </tr>
    </tbody>
</table>

 ## 6. HEALTH
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEALTH_1.png)
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEALTH_2.png)
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEALTH_3.png)
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEALTH_4.png)
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEALTH_5.png)
 <table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td>HEALTH01_LABEL</td>
            <td>1. Have you ever had, or been told you had, or received treatment for:</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Label</td>
            <td></td>
            <td>Yes / No (The field is disabled. Always polulate the answer from eBI)</td>
        </tr>
        <tr>
            <td>1</td>
            <td>HEALTH01</td>
            <td>(g) Any physical or developmental impairments or abnormalities or premature birth?</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's age <= 15</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH01 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH01 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH01 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH01h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>2</td>
            <td>HEALTH02</td>
            <td>(a) Chest pain, high blood pressure, heart attack, stroke, diabetes, or any heart, blood or vascular disease or disorder?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH02 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH02 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH02 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02f</td>
            <td>Details (Medication/Treatment / Follow up / Complication) </td>
            <td></td>
            <td>HEALTH02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH02h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>3</td>
            <td>HEALTH03</td>
            <td>(b) Cancer, melanoma, tumour, cysts, lump, polyp or growth of any kind?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH03 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH03 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH03 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH03 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH03 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH03 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH03 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH03h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH03 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>4</td>
            <td>HEALTH04</td>
            <td>(c) Kidney disease (e.g. stone, cyst), mental or nervous disorder (e.g. anxiety, depression), vision or hearing problems (e.g. cataract, meniere’s disease), digestive system  disorder (e.g. gastritis), endocrine disease (e.g. thyroid problem), liver disease (e.g. fatty liver), nervous system disorder
            (e.g. epilepsy, motor neurone disease), respiratory system disorder (e.g. asthma), skin disease
            (e.g. psoriasis), urinary system disorder (e.g. blood/protein in urine), spinal or muscle problems (e.g. slipped disc, dystrophy)?
            </td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>LIFESTYLE04d8</td>
            <td>Others</td>
            <td></td>
            <td>LIFESTYLE04 = Yes</td>
            <td>Check box</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH04 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH04 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH04 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH04 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04e</td>
            <td>Test result (if any)</td>
            <td></td>
            <td>HEALTH04 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication) </td>
            <td></td>
            <td>HEALTH04 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH04 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH04h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH04 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>5</td>
            <td>HEALTH05</td>
            <td>(d) Joint, limb or bone disease or disorder, auto-immune disease (e.g. systemic lupus erythematosus) or infectious disease?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH05 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH05 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH05 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH05 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH05 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH05 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH05 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH05h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH05 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>6</td>
            <td>HEALTH06</td>
            <td>(e) Hepatitis B or C, HIV infection, tuberculosis, alcohol or drug dependency?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH06 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH06 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH06 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH06 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH06 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH06 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH06 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH06h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH06 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>7</td>
            <td>HEALTH07</td>
            <td>(f) Any other illness, disease, disorder, operation, physical disability or accident not mentioned above?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH07 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH07 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH07 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH07 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH07 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH07 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH07 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH07h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH07 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>8</td>
            <td>HEALTH08</td>
            <td>2. Are you currently receiving any medical treatment or do you intend seeking or have been advised to seek medical treatment for any health problems or are you waiting the results of any tests/ investigations?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH08 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH08 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH08 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH08 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH08 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH08 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH08 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH08h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH08 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>9</td>
            <td>HEALTH09</td>
            <td>3. Apart from condition listed above, have you ever seen a doctor or other health professional, or been prescribed medication for any other condition which has lasted for more than (apart from usual flu and colds) 5 days?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH09 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH09 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH09 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH09 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH09 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH09 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH09 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH09h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH09 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>10</td>
            <td>HEALTH10</td>
            <td>4. Have you or your spouse been told to have received any medical advice, counseling or treatment in connection with sexually transmitted disease, AIDS, AIDS Related Complex or any other AIDS related condition?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH10 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH10 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH10 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH10 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH10 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH10 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH10 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH10h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH10 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>11</td>
            <td>HEALTH11</td>
            <td>5. In the PAST FIVE YEARS, have you had any tests done such as X-ray, ultrasound, CT scan, biopsy, electrocardiogram (ECG), blood or urine test? If yes, please provide details in the following table and please submit copy of report (if available).</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH11 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH11 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH11 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH11 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH11 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH11 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH11 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH11h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH11 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12_LABEL</td>
            <td>6. For Female Applicants only (For age 10 and above)</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's gender = Female && Life Assured's age >= 10</td>
            <td>Label</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>12</td>
            <td>HEALTH12</td>
            <td>(a) Have you had any breast disease, menstrual disorders, fibroids, cysts or any other disorders of the female reproductive organs, or abnormal pap smear, mammogram, ultrasound or any gynaecological investigations?</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's gender = Female && Life Assured's age >= 10</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH12 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH12 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH12 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH12 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH12 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH12 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH12 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH12h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH12 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>13</td>
            <td>HEALTH13</td>
            <td>(b) Are you currently pregnant?</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's gender = Female && Life Assured's age >= 10</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13a</td>
            <td>How many weeks pregnancy now? (Weeks)</td>
            <td></td>
            <td>HEALTH13 = Yes</td>
            <td>Integer</td>
            <td>2</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b</td>
            <td>Any complications during pregnancy such as gestational diabetes, hypertension or any other pregnancy related condition?</td>
            <td></td>
            <td>HEALTH13 = Yes</td>
            <td>Radio Group</td>
            <td>Yes / No</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b1</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH13b = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b2</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH13b = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b3</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH13b = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b4</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH13b = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b5</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH13b = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b6</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH13b = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b7</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH13b = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH13b8</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH13b = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH14_LABEL</td>
            <td>7. For Juvenile Applicants only (For 0 - 6 months)</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's age = 0-6 months</td>
            <td>Label</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>14</td>
            <td>HEALTH14</td>
            <td>(a) Was the child born premature or pre-term (before 37 weeks gestation)?</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's age = 0-6 months</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH14a</td>
            <td>Gestational Week:</td>
            <td></td>
            <td>HEALTH14 = Yes</td>
            <td>Integer</td>
            <td></td>
            <td>< 37, error message: Value must be less than 37</td>
        </tr>
        <tr>
            <td>15</td>
            <td>HEALTH15</td>
            <td>Full uw type questionnaire form && Life Assured's age = 0-6 months</td>
            <td></td>
            <td>HEALTH15 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH15a</td>
            <td>Birth Weight (kgs):</td>
            <td></td>
            <td>HEALTH15 = Yes</td>
            <td>Numeric</td>
            <td>4</td>
            <td>< 2.5, 2 decimal points ,  error message: Value must be less than 2.5</td>
        </tr>
        <tr>
            <td>16</td>
            <td>HEALTH16</td>
            <td>(c) Was the duration of hospital stay after birth is more than 3 days?</td>
            <td></td>
            <td>HEALTH16 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH16a</td>
            <td>Duration of Hospital Stay (days):</td>
            <td></td>
            <td>HEALTH16 = Yes</td>
            <td>Integer</td>
            <td>4</td>
            <td>> 3,  error message: Value must be greater than 3</td>
        </tr>
        <tr>
            <td>17</td>
            <td>HEALTH17</td>
            <td>(d) Has the child ever suffered from, or currently suffering from, or being followed up or investigated for any residual birth/delivery complications, congenital disorder/birth defect, physical impairment, mental retardation, G6PD deficiency, cerebral palsy, Down’s Syndrome, prolonged jaundice, respiratory distress syndrome or any other serious disorder?</td>
            <td></td>
            <td>Full uw type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH17 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH17 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH17 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH17 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH17 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH17 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH17 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH17h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH17 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>18</td>
            <td></td>
            <td>Important: If ‘Yes’ for any of the above child questions, please submit latest Child Health Booklet including all assessments done to date.</td>
            <td></td>
            <td>Full uw type questionnaire form && Life Assured's age = 0-6 months</td>
            <td>Label</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>

## 7. HEALTH_GIO
 ![alt-text](assets/img/application/Application_Insurability_Questionnaire_HEALTH_GIO.png)
 <table>
    <thead>
        <tr>
            <th colspan="8">Requirement</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <td>Reference</td>
            <td>Question ID</td>
            <td>Question</td>
            <td>Optional?</td>
            <td>Display when</td>
            <td>Data Type</td>
            <td>Field Size</td>
            <td>Validation</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>HEALTH_GIO01</td>
            <td>1. During the last 3 years, have you ever been hospitalized or have you consulted a medical practitioner for any medical condition that required medical treatment for over 14 consecutive days, or are you intending to do so, or have you had or been advised to have any operation,test or treatment?*
            *Consultations, tests or treatment for the following conditions can be ignored: common cold, fever or flu; uncomplicated pregnancy or caesarean sections; contraception, inoculations, minor joint or muscle injuries or uncomplicated bone fractures from which you have fully recovered.</td>
            <td></td>
            <td>SIO type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO01h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH_GIO01 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td>2</td>
            <td>HEALTH_GIO02</td>
            <td>2. Have you ever had or been told that you have, or have been treated for cancer (including carcinoma-in-situ), growth or tumor of any kind, diabetes, high blood pressure, chest pain, stroke, heart diseases, blood disorder, respiratory diseases, kidney diseases, bowel diseases, hepatitis or liver diseases, nervous or mental disorders, spinal disorders, muscular or joint disorders, AIDS or HIV related conditions, or any other serious illness or impairment?</td>
            <td></td>
            <td>SIO type questionnaire form</td>
            <td>Radio Group</td>
            <td></td>
            <td>Yes / No</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02a</td>
            <td>Medical Condition / Diagnosis</td>
            <td></td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02b</td>
            <td>Year of diagnosis</td>
            <td></td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Select</td>
            <td></td>
            <td>Dropdown of YYYY</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02c</td>
            <td>Type of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02d</td>
            <td>Date of test done (if any)</td>
            <td>Yes</td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Date Picker</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02e</td>
            <td>Test result (if any)</td>
            <td>Yes</td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Radio Group</td>
            <td></td>
            <td>Normal / Abnormal</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02f</td>
            <td>Details  (Medication/Treatment / Follow up / Complication)</td>
            <td></td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02g</td>
            <td>Name of Doctor</td>
            <td>Yes</td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
        <tr>
            <td></td>
            <td>HEALTH_GIO02h</td>
            <td>Address of Hospital/Clinic</td>
            <td>Yes</td>
            <td>HEALTH_GIO02 = Yes</td>
            <td>Text Field</td>
            <td>200</td>
            <td>alphanumeric</td>
        </tr>
    </tbody>
</table>



## Technical
For the source code relate to this page, please refer to 

- Component @ AXA-SG-APP/app/components/DynamicViews/index.js ,<br>AXA-SG-APP/app/routes/Applications/components/ApplicationForm/index.js  , <br>AXA-SG-APP/app/routes/Applications/components/ApplicationFormDynamic/index.js
- Styles @ AXA-SG-APP/app/components/DynamicViews/styles.js,
<br>AXA-SG-APP/app/routes/Applications/components/ApplicationForm/styles.js  , <br>AXA-SG-APP/app/routes/Applications/components/ApplicationFormDynamic/styles.js
- Reducers @ eab-web-api/src/reducers/application.js
- Saga @ eab-web-api/src/saga/application.js

For the mechanism to generate form, please refer to the Dynamic Form document







