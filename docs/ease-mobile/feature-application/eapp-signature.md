---
id: eapp-signature
title: E-Signature
sidebar_label: E-Signature
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Kevin Liang |

## Introduction
EASE iOS uses [Kofax Signature](https://www.kofax.com/Products/E-signature-and-Verification/SignDoc/Overview) v4.0 SDK (Objective-C version) to perform offline signature. 

## Directory Structure
```
axa-sg-app
└───app
│    └───components
│        └───SignDocDialog
│               └───index.js
│    └───routes
│        └───Applications
│               └───components
│                        └───Signature
│                               └───index.js
│                               └───styles.js
└───ios
    └───SignDoc
        └─── include // Function documentation of Kofax Sign Doc SDK
        │   SignDocDialogManager.m
        |   SignDocDialogViewController.m
        |   SignProcessor.m
```

## Signature Flow

### Init Sign Doc Dialog
Init dialog view on native code (/ios/SignDoc/SignDocDialogManager.m)
```
RCT_EXPORT_METHOD(initView) {
  _signDocDialogView = [[SignDocDialogView alloc] initWithBridge:self.bridge];
}
```

Call native code to init sign doc dialog (/app/components/SignDocDialog/index.js)
```
const SignDocDialogView = requireNativeComponent("SignDocDialog");
```

### Init Pdf
When agent choose one of the pdfs, system will init pdf with pdf base64 data and signature related key words.
Signature related key words are hidden behind signature box with white font color. See the example below(red box).

![White hidden signature key words](assets/img/application/eapp-signature-keywords.png)

Pass pdf base64 data and key words to native code. 

```
signDocDialogManager.init(searchTagArr, pdfStr, callback)
```

Because react native pdf reader library can't show signature of pdf, we transform to png format to show.
Native code will return png images (base 64 format) array to react native on callback.
Also, sign doc sdk can extract the position of the signature box and how many positions are under particular key word. Key word details will also send back to react native.
```
// callback result
{
   "tags": [
      {
         "key": "key word 1", // white word behind the signature box
         "name": "name 1", // Id of the signature tag
         "page": "0", // Order of the signature box under current key word, not the page of signature box 
         "signX": "0.432365", // x value of signature box, we don't use this value temporarily
         "signY": "0.434232" // y value of signature box, we don't use this value temporarily
      },
      {
         "key": "key word 2",
         "name": "name 2",
         "page": "0",
         "signX": "0.132353",
         "signY": "0.934232"
      },
      {
         "key": "key word 2",
         "name": "name 2",
         "page": "1",
         "signX": "0.532373",
         "signY": "0.234232"
      },
      ...
   ],
   "images": [
      "Page1 PNG Base64 data",
      "Page2 PNG Base64 data",
      "Page3 PNG Base64 data",
      ...
   ]
}
```

### Init Signature List
React native can get the signature key word details from last step and init signature list.

### Init Capture Dialog

### Sign Process

### After signature





