---
id: ease-mobile-openid-authentication
title: EASE Mobile Openid Authentication
sidebar_label: EASE Mobile Openid Authentication
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 30 JAN 2019 | Init | Kevin Liang |

[![Openid Authentication](assets/architecture/openIdAuthentication.png)](assets/architecture/openIdAuthentication.png)

## Background
There are [3 authentication methods](https://docs.couchbase.com/sync-gateway/2.1/authentication.html) to connect client side and sync gateway. We used Basic Authentication before. On website section, AXA admin can set sync gateway user name and password on their Openshift docker config page and handle by themself. It's safe to protect production data. But on APP section, we need set sync gateway user name and password on APP. It's not safe because EAB can touch production data. If someone hack our APP and get the user name and password, the hacker can also touch production data. Also, AXA can't handle the user name and password by themself. So discussed with AXA team, we uses OpenID Authentication now.

## Section
- Maam: AXA third party authentication system
- [APP](architecture/ease-architecture.md#121-mobile-app): Show related infomation to agent and client
- Safari: For third party log in
- [Mobile API](architecture/ease-architecture.md#121-webserviceapp-ws): Handle online logic from mobile
- [Sync Gateway (Public)](architecture/ease-architecture.md#nosql-sync-gateway): Sync or CRUD data between Couchbase Lite and Couchbase Server, everyone can access this public port.
- [Sync Gateway (Admin)](architecture/ease-architecture.md#nosql-sync-gateway): Sync or CRUD data between Couchbase Lite and Couchbase Server, only Mobile API can access the admin port and grant user channel access.

## Flow
### 1. Open MAAM Front End In Safari

### 2. Initialize SSO, conduct 2-FA

### 3. Get Auth_code

### 4. Pass Auth_code to APP

### 5. Request Access_Token

### 6. Return Access_Token & ID_Token

### 7. Login Authentication (access_token & id_token)

### 8. Verify Access_token

### 9. Return credential

### 10.Request OpenID Session (with ID_token)

### 11. Verify OpenID

### 12. Return credential

### 13. Return Session. Create user (first login)

### 14. Grant user channel access right (for first login)
