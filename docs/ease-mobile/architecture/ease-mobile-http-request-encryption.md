---
id: ease-mobile-http-request-encryption
title: EASE Mobile HTTP Request Encryption
sidebar_label: EASE Mobile HTTP Request Encryption
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 30 JAN 2019 | Init | Kevin Liang |

## Request Method: POST, PUT, DELETE
[![HTTP Request Encryption](assets/architecture/httpRequestEncryption.jpg)](assets/architecture/httpRequestEncryption.jpg)

## Request Method: Get
It's a to do task, will do it later.