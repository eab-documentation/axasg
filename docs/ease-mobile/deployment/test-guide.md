---
id: test-guide
title: Test Guide
sidebar_label: Test Guide
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init (move from share drive) | Kevin Liang |

## 1. Log in and download
Log in your iPad with the account and download Testflight app from App Store.

![Download Testflight](assets/img/deployment/test-guide-download-testflight.png)

## 2. Accept the invitation
An invitation email will send you after a new build is ready and reviewed by Apple. Click ‘View in Testflight’ button to accept the new test.

![Accept the invitation](assets/img/deployment/test-guide-accept-invitation.png)

Then you will get an redeem code. Click the ‘Redeem’ button on Testflight app and then enter the redeem code on the text box.

## 3. Start Testing
Now you can see the app just like you are on App Store. Click download button to download the app. Then you can start your test.

## 4. Reinstall the app
We need reinstall the app sometimes such as testing agent activation. Just uninstall the app and then install the app from Testflight again (In Testflight, press the “Install” button next to the app). Do not need uninstall Testflight because it is a tool just like App Store, or you will need a new redeem code to get our app.
