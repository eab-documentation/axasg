---
id: deployment-testflight
title: Build and deploy to Testflight
sidebar_label: Build and deploy to Testflight
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init (move from share drive) | Kevin Liang |

## 1. Ready for version and build
Open Xcode, modify version and build under General tab for EASE target and EASE-tvOS target. (Please use different build number under each version, or it can’t deploy to Testflight)

[ ![Version number and build number](assets/img/deployment/step1-version-and-build-number.png) ](assets/img/deployment/step1-version-and-build-number.png)

## 2. Sign with developer account
Confirm that you are signing with EAB team (axasg.ease.ios@eabsystems.com) for EASE, EASETests and EASE-tvOSTests targets. If you are not signing, please add our account. (Ask Bernard or Peddi to get account)

[ ![Sign team](assets/img/deployment/step2-sign-team.png) ](assets/img/deployment/step2-sign-team.png)

## 3. Choose Device
Choose EASE > Generic iOS Device
[ ![Choose device](assets/img/deployment/step3-choose-device.png) ](assets/img/deployment/step3-choose-device.png)

## 4. Build app
Click Product -> Clean and then Product -> Archive to build the app
[ ![Build app](assets/img/deployment/step4-build-app.png) ](assets/img/deployment/step4-build-app.png)

## 5. Export package
After several mins, it will finish archiving and show all historic builds. Choose the build just now and click Export button to export .ipa file.
[ ![Export package](assets/img/deployment/step5-export-package.png) ](assets/img/deployment/step5-export-package.png)

Choose App Store (Don’t worry. It will not deploy to real App Store. It will only deploy to sandbox App Store - Testflight). Then choose next with default options.
[ ![Export type](assets/img/deployment/step5-export-type.png) ](assets/img/deployment/step5-export-type.png)

Export to a path.
[ ![Export path](assets/img/deployment/step5-export-path.png) ](assets/img/deployment/step5-export-path.png)

## 6. Upload to iTunes Connect
Click Xcode -> Open Developer Tool -> Application Loader
[ ![Application Loader](assets/img/deployment/step6-application-loader.png) ](assets/img/deployment/step6-application-loader.png)

Choose the .ipa file you exported just now and choose next to upload with default options.
[ ![Pick Up Package](assets/img/deployment/step6-pick-up-package.png) ](assets/img/deployment/step6-pick-up-package.png)

[ ![Uploading](assets/img/deployment/step6-uploading.png) ](assets/img/deployment/step6-uploading.png)

## 7. Build on iTunes Connect
After finish deploying to iTunes Connect, go to [App Store Connect dashboard](https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1411768599/activity/ios/builds) to see whether the app has deployed successfully. 
Note: Usually, it will build again on iTunes Connect and show ‘(Processing)’ after build number. After several mins, it will finish building. The ‘(Processing)’ disappear and it means it is ready to test on Testflight.

[ ![Build on iTunes Connect](assets/img/deployment/step7-build-on-itunes-connect.png) ](assets/img/deployment/step7-build-on-itunes-connect.png)

## 8. Invite internal users and external users
In Testflight world, it has 2 kinds of test users – internal users and external users. Internal users can test all builds and manage the app info. External users can test only particular builds after reviewing by Apple.
- A)	If you want to add internal users, you should add them on [Users and Roles page](https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/users_roles) firstly. Then go back to My App page and import them to [App Store Connect Users group](https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1411768599/testflight?section=internaltesters&subsection=testers) under TestFlight tab.

[ ![Build on iTunes Connect](assets/img/deployment/step8-internal-user1.png) ](assets/deployment/step8-internal-user1.png)

[ ![Build on iTunes Connect](assets/img/deployment/step8-internal-user2.png) ](assets/deployment/step8-internal-user2.png)

For more detail, please ref to [Add internal testers](https://help.apple.com/app-store-connect/#/dev839fb66e9)

- B)	If you want to add external users, you need some external test user group. If you don’t have external test user group, please create one by clicking ‘Add External Testers’ button on the left.

[ ![Build on iTunes Connect](assets/img/deployment/step8-external-user1.png) ](assets/img/deployment/step8-external-user1.png)

Then you can add external users under the group.

[ ![Build on iTunes Connect](assets/img/deployment/step8-external-user2.png) ](assets/img/deployment/step8-external-user2.png)

For more detail, please ref to [Invite external testers](https://help.apple.com/app-store-connect/#/dev859139543)

## 9. Enjoy testing
If you are inactive test user, an invitation email will send to you after we add you in the group. Click active button on the email can active the account. After a new build has finished built on iTunes Connect, the test users will receive an email which includes a redeem code. Download Testflight with the account and enter the redeem code on Testflight. Now you can download the app just like you are on real App Store.

[ ![Build on iTunes Connect](assets/img/deployment/step9-enjoy-testing.png) ](assets/img/deployment/step9-enjoy-testing.png)