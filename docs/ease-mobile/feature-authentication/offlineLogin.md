---
id: offlineLogin
title: Offline Login 离线登录
sidebar_label: Offline Login 离线登录
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Mandy Yiu |
| 1.1 | 4 Dec 2019 | Offline Login Flow Image | Kevin Liang |

## Offline Login Flow Image 离线登陆流程图
[![Offline Login Flow Image](assets/authentication/OfflineLoginFlow.png)](assets/authentication/OfflineLoginFlow.png)

## Business Logic
EASE IOS APP supports offline mode. Without internet connectivity, user logins as offline mode and the app asks online authentication while requiring network-required features such as emails, data sync, submission and online payment. 

> **Notes:** With internet connectivity, it is suggested user logins as [online mode](../authentication/onlineLogin). 


User is needed to perform steps shown as follows: 
1.	Reach Offline Login Page when launching  the app. 

    ![Online Login](assets/authentication/offlineLogin.png) 

2. Key in the correct [app password](activation.md#app-password) set during app activation.

3. Invalid App Password triggers warning message.

    ![First Fail App Password Warning Mesaage](assets/authentication/failAppPasswordLogin1.png) 

    ![Not First Fail App Password Warning Message](assets/authentication/failAppPasswordLogin2.png) 

4. If the number of consecutive failed login attempts hits the threshold (5 times), show warning message as follows. The app is then locked and the user have to log in through [Online Login](../authentication/onlineLogin). Press "Continue" to reach SiteMinder Page and enter Username and Password.

    ![Lock1](assets/authentication/failAppPasswordLock.png) 

    ![Lock1](assets/authentication/failAppPasswordLock2.png) 

5. User have to log in through online authentication. Once they online log in, prompt the user to set a new [App Password](activation.md#app-password) by showing the reset App Password dialog. 

    ![Reset App Password](assets/authentication/resetAppPassword1.png)   

6. App performs check on time zone, time bomb during Offline Log. If violated, warning message or responding action is then triggered. Details of Time zone and Time Bomb are linked below: 
    -   [Time Zone](onlineLogin.md#timezone)
    -   [Time Bomb](onlineLogin.md#timebomb)

7. Reach landing Page if the above 1 and 2 are completed. 

    ![Landing Page](assets/authentication/landingPage.png) 


### Failed Login Attempts
The app will keep a tally of the number of consecutive failed login attempts, whether it is a correct App Password.
If the number of consecutive failed login attempts hits the threshold (5 times). The app will be locked and the user will have to log in through online authentication. Once they online log in, prompt the user to set a new App Password by showing the reset App Password dialog. 

Warning Message listed below if wrong App Password entered. 

|# of Fail Attempt|Warning Message                                                                                            |
|:---------------:|:---------------------------------------------------------------------------------------------------------:|
|1st              |Login failed. Please enter correct credentials                                                             |
|2nd              |You have entered an incorrect password 2 times. After 3 unsuccessful attempts your account would be locked.|
|3rd              |You have entered an incorrect password 3 times. After 2 unsuccessful attempts your account would be locked.|
|4th              |You have entered an incorrect password 4 times. After 1 unsuccessful attempts your account would be locked.|
|5th              |Your account os locked. Please unlock through reset Password                                               |


     


### App Password Expiry Date 

The App Password expires after 60 days.When the user launches the app, check whether the user’s App Password has expired. If it has expired, it shows warning message as following figure and requires the user to log in through online authentication. Once they log in, prompt the user to set a new App Password by showing the Set [App Password](activation.md#app-password) dialog without the option to close it.

![App Password Expiry Page](assets/authentication/appPasswordExpiryDate.png) 