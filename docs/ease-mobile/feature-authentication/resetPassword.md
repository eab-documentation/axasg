---
id: resetPassword
title: Reset Password 重置密码
sidebar_label: Reset Password 重置密码
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 4 Dec 2019 | Init | Kevin Liang |

## Reset Password Flow Image 重置密码流程图
[![Reset Password Flow Image](assets/authentication/resetPasswordFlow.png)](assets/authentication/resetPasswordFlow.png)