---
id: siteminderOtp
title: SiteMinder OTP 获取验证码
sidebar_label: SiteMinder OTP 获取验证码
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 15 FEB 2019 | Init | Kevin Liang |
| 1.1 | 4 Dec 2019 | Update SZ team retrieve OTP method | Kevin Liang |
| 1.1 | 4 Dec 2019 | Translation | Kevin Liang |

## Background
SiteMinder always send OTP(One-time password) to agent after enter account and press log in button. For our convenience, we bind all testing accounts(EAB, AXA-SIT, AXA-UAT, AXA-preprod environment) to a phone number. The phone number sim card is placed on a phone in EAB HK. So when developer test feature, he/she need get OTP on the phone.

## 背景
当进入AXA第三方登陆网站输入完账号后点击登陆按钮，Siteminder会发送验证码（OTP, One Time Password）给agent。为了方便，我们绑定了所有非production厂（EAB SIT厂，AXA SIT厂，AXA UAT厂，AXA PRE-PROD厂）的agent电话到一个电话上。这台电话的sim卡插在香港办公室的一台公共手机上。当程序员需要验证码的时候，可以去这台手机的短信里面获取。

## How to get
### Traditional Method
The phone is placed on IT room. You can go to IT room, open SMS app and then you can get the OTP. But this is method needs much time.

### Using Mightytext
Because traditional method wastes too much time, we use [Mightytext](https://mightytext.net/) to sync data on the web. We can visit the site just like we use the phone.

Here is the guide to get OTP:

1. Visit [Mightytext](http://mightytext.net/web)

2. Log in with google account. User name: nba54129024@gmail.com, password: ABcd1234

3. Click allow remember this account

4. You can get OTP on the website

>The Mightytext website is blocked by [Great Firewall](https://en.wikipedia.org/wiki/Great_Firewall), Shenzhen team need visit this website via VPN. Because the deployment team fully move from HK to SZ on last few months of 2019, so this method is not the best solution. Please refer to the next method.

### Ask AXA to update agent account mobile phone to developer's mobile phone
The agent mobile phone can be set by AXA team. Make sure it is one developer one agent account. Then the OTP can be sent to developer's own mobile phone.

## 如何获取
### 传统方式
这台公共手机放在香港的IT部门房间里。可以前往IT房间，打开短信获取最新的验证码。但这个方法需要花费很多时间。

### 使用Mightytext
因为传统的方法需要花费很多时间， 我们使用了Mightytext这个软件，它是一个同步手机内容的软件，我们可以通过绑定这台手机，然后浏览这个网站就可以同步得到验证码，就像有一台手机在身边一样。

步骤是：
1. 访问Mightytext网站
2. 登陆谷歌账号。用户名：nba54129024@gmail.com, 密码: ABcd1234
3. 点击记账账号
4. 登陆之后就可以拿到验证码了

>由于大陆的防火墙会屏蔽Mightytext这个网站，所以深圳的同事需要通过vpn来访问这个页面。 由于EASE的开发团结全部由香港团队交给深圳团队了，所以这个方法并不好用。请使用下面一个方法

### 让AXA帮忙把手机号绑定到对应开发者的手机号上面
设置agent手机号的权限在AXA团队那边，首先确保一个开发者有一个账号。然后绑定对应的开发者的手机号到agent的账号里。


