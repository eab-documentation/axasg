---
id: activation
title: App Activation 激活
sidebar_label: App Activation 激活
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 22 JAN 2019 | Init | Mandy Yiu |
| 1.1 | 24 JAN 2019 | Add Policy Number, Time Zone and Time Bomb | Mandy Yiu |
| 1.2 | 13 FEB 2019 | Envioronment switch, upline checking | Kevin Liang |
| 1.3 | 15 FEB 2019 | OTP | Kevin Liang |
| 1.4 | 4 Dec 2019 | Activation flow image | Kevin Liang |
| 1.5 | 4 Dec 2019 | Translation | Kevin Liang |

## Activation Flow Image 激活流程图
[![Image of app activation flow](assets/authentication/ActivationFlow.png)](assets/authentication/ActivationFlow.png)

## Business Logic
After installing the App, user is required to log in with SiteMinder through a Safari View. 
When user launches the app, check whether there is internet connectivity. 

> **Notes:** App Activation must be performed with internet connectivity. 

User is needed to perform steps shown as follows: 
1.	Select designated environment of app. Options of environments are listed as follows.
    * SIT1_EAB
    * SIT2_EAB
    * SIT3_EAB
    * UAT
    * PRE_PROD

    ![Environment](assets/authentication/environment.png)
    The environment setting is on `.env` file under root folder.

    If you want to choose production environment, there isn't any drop down list to choose. You should open XCode, choose `product -> Scheme -> EASE(PROD)` and then reinstall the app.
    
    The environment setting is on `.env.prod` file under root folder

2. Press “Continue” to reach SiteMinder Page and enter Username and Password to perform SiteMinder Authentication. 

    ![SiteMinder](assets/authentication/siteMinder.png) 

3. SiteMinder will ask you provide OTP(One-time password). Testing accounts(EAB, AXA-SIT, AXA-UAT, AXA-preprod environment) bind a phone number. You can following the [guide](siteminderOtp.md) to get OTP.

![SiteMinder OTP](assets/authentication/siteminder_otp.png)

4. User types in appPassword

    ![App Password](assets/authentication/appPassword.png) 

5. Check upline
    Firstly, get agent profile. Secondly, check if agent role not equals to 04 and manager code not exist. If yes, alert an error message: [Error] You won't be able to submit case as there is no supervisor information linked to you. Please contact your firm's administrator.

6. Data Sync is triggered as following figure. 

    ![Data Sync](assets/authentication/dataSync.png) 

7. Reach landing Page if the above steps are completed. 

    ![Landing Page](assets/authentication/landingPage.png) 

***
### App Password

Table shows App Password Policy

|Policy Name                     |Description                                                                             |
|:------------------------------:|:------------:                                                                          |
|Minimum length                  |8 characters                                                                            |
|Maximum length                  |16 characters                                                                           |
|Complexity                      |1 lowercase letter, 1 uppercase letter, 1 number and 1 non-alphanumeric                 |          
|Previous passwords              |New password cannot be the same as user’s previous 1 passwords (incl. current password)|
|Consecutive repeating characters|Alphanumeric patterns like AAA, AaA, bbb and 888 are invalid (case insensitive)         |
|Ascending characters            |Alphanumeric patterns like 123, Abc, EFG and xyz are invalid (case sensitive)           |

> **Notes:** P@ss9413, Ab*d4321 are two examples. App Password entered during activation must obey ALL policies listed aboves. 
***
### One Device - One UserID 
Users can activate and use the app on one device only. When the user activates the app, a Device ID made up of the User ID, iOS UUID and Device Model are sent to 121 Online Service. The Device is registered under the User ID in the remote database. If the user activates the app on a new device, the previous Device ID will be replaced with the new one.
[Multple Device Check](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_check) on Latest Version is performed before EACH [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/) Call Throughout tha app.
***

### Latest Version
Users are required to use latest version of EASE IOS App. [Latest Version Check](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_check) on Latest Version is performed before EACH [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/) Call Throughout tha app.

### Policy Number 


***

## 业务逻辑
当用户安装好APP后，需要通过Safari View进入到AXA第三方登陆平台登录。 当用户启动APP的时候，检查网络连接状态。
> **备注:** 激活APP必须在联网的状态下进行
接下来具体的步骤如下：
1. 有个下拉框选择环境：
    * SIT1_EAB
    * SIT2_EAB
    * SIT3_EAB
    * UAT
    * PRE_PROD

    ![Environment](assets/authentication/environment.png)
    环境设置在根目录`.env`文件里

    如果你要选择production环境，该下拉框没有这个选项。你要打开XCode，选择`product -> Scheme -> EASE(PROD)`，然后重新安装APP

    production的环境设置在根目录`.env.prod`文件里

2. 按“Continue”去到SiteMinder页面，输入用户名密码去登录

    ![SiteMinder](assets/authentication/siteMinder.png) 

3. SiteMinder会问你要验证码。 测试账号(EAB, AXA-SIT, AXA-UAT, AXA-preprod厂)会绑定一个手机号。你可以参考[这篇文章](siteminderOtp.md)来知道如何获取。

    ![SiteMinder OTP](assets/authentication/siteminder_otp.png)

4. 用户输入离线密码

    ![App Password](assets/authentication/appPassword.png) 

5. 检查agent的上线

    首先获取用户信息。第二，检查agent的角色是不是等于04和经理代码是不是存在。如果是，弹出错误提示信息： [Error] You won't be able to submit case as there is no supervisor information linked to you. Please contact your firm's administrator.

6. 自动触发Data Sync（数据同步）

    ![Data Sync](assets/authentication/dataSync.png) 

7. 如果上面的步骤都完成了，会进入landing页面

    ![Landing Page](assets/authentication/landingPage.png) 

### 离线密码

### 一设备一用户

### 最新版本检测

### 保单号

## Technical Design

![Image of app activation diagram](assets/authentication/appActivationDiagram.png) 

***
### Workflow in details
1. Redirect to [SiteMinder](https://maam-dev.axa.com/maam/v2/authorize?scope=openid%20axa-sg-lifeapi&client_id=43dd75bc-3cae-4281-906b-fe0404ddab75&redirect_uri=easemobile://&response_type=code) via [React Native Safari View](https://github.com/naoufal/react-native-safari-view)

2. When username and password are valid, the website will redirect to other link many times.

3. Finally, `authCode` will response from SiteMinder with the format `easemobile://?code=xxxxyyyzzz`

4. [React Native Safari View](https://github.com/naoufal/react-native-safari-view) will dismiss, the app can receive the `authCode`

5. Request `authToken` and `idToken` from AXA Authorization Server by `authCode`

6. Respond to `authToken` to `idToken` 

7. Invoke [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_activate) call to request `userId`, `installationId`, `sessionCookieSyncGatewaySession`, `sessionCookieExpiryDate`, `sessionCookiePath` and `syncDocumentIds` to activate user. 

8. Respond to fields listed in Step (7)

9. Type in appPassword: 
* Two keys are generated as sqliteEncryptionKey and masterKey using AES 256 algorithm  

        const sqliteEncryptionKey = aesGen256(); 

        const masterKey = aesGen256();

* masterKey is encrypted by appPassword as encryptedMasterKey. Decryption is performed during offline Login. 

        const encryptedMasterKey = aesEncrypt(masterKey, appPassword);
    

10. Init Couchbase Lite DB and SQLite DB 

11.	Invoke [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/put_auth_encryptionKey) call to store `masterKey` and `installationId` into Oracle Database (121 online DB)

12. Store fields into different places.

    |Fields / DB Stored   |Keychain      |SQLite         |Couchbase Lite |121 Database   |
    |:------------:       |:------------:|:------------: |:------------: | :------------:|
    |appPassword          |✓             |✓              |               |               |
    |appPasswordExpiryDate|✓             |               |               |               |
    |sqliteEncryptionKey  |✓             |               |               |               |
    |masterKey            |              |               |               |✓              | 
    |installationId       |✓             |               |               |✓              | 
    |userId               |✓             |               |               |               | 
    |lastDataSyncTime     |✓             |               |               |               | 
    |encryptedMasterKey   |              |✓              |               |               |  
    |hashedappPassword    |              |✓              |               |               |  
    |lastLoginTime        |              |✓              |               |               |  
    |environment          |              |✓              |               |               |  
    |loginFailCount       |              |✓              |               |               |  
    |syncDocumentIds      |              |✓              |               |               |   
    |agent Data           |              |               |✓              |               |  
    |policy numbers       |              |               |✓              |               | 

13.	Import [default JSON](assets/authentication/testing_data_config.txt) into Couchbase Lite during activation 

14. Invoke [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/#/User/get_mock_user) call to retrieve agent Data. The agent profile is created and stored into couchbase lite 

### Policy Number 
As the app supports offline mode to create cases together with policy numbers, whether it is a non-shield case or shield case. The app initially sets maximum number of non-shield policy number and maximum number of shield policy number as 10 and 20 respectively. 

        MAX_OF_NON_SHIELD_POLICY_NUM = 10
        MAX_OF_SHIELD_POLICY_NUM  = 20

During App Activation, 

- Invoke [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber) call to request both `MAX_OF_NON_SHIELD_POLICY_NUM` and `MAX_OF_SHIELD_POLICY_NUM`. 

- Create JSON namely "policyNumnerPool" stored in Coucbase Lite. An example of [policyNumberPool](assets/authentication/policyNumberPool.json) is shown. 

Other Usage of Policy Number is listed in the following table. 

|During            |Warning Message                                                                                            |
|:----------------:|:---------------------------------------------------------------------------------------------------------:|
|App Activation    | Invoke API call to request MAX_OF_NON_SHIELD_POLICY_NUM  and MAX_OF_SHIELD_POLICY_NUM. and then create a JSON namely “policyNumberPool” and stored policy numbers in Couchbase Lite.|
|Online Login      |Check is performed to refill number of required non-shield and shield policy number.|
|Offline Login     |No action|
|Apply Policy Number|Update "isUsed" in th“policyNumberPool” to Couchbase Lite|
|Submission         |Update “policyNumberPool” to Couchbase Lite|


### EASE Mobile API related to App Activation 
Table as follows lists all [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/) during App Activation. 

|Usage|EASE Mobile API Endpoint|
|:-----------------|:---------------------------------------------------------------------------------------------------------|
|Get Public Key|[/auth/publicKey](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/get_auth_publicKey)|
|Check on multiple device and latest version|[/auth/check](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_check)|
|Activate User|[/auth/activation](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_activate)|
|Store master Key|[/auth/encryptionKey](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/put_auth_encryptionKey)|
|Get Policy Number|[/application/getPolicyNumber](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber)|
|Get Agent Data|[/mock/user](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/User/get_mock_user)| 

### Files related
Paths of related files are as follows: 

1. **Environment UI**: /app/routes/Root/components/Environment/index.js 
2. **Activation UI**: /app/routes/Root/components/AppActivation/index.js
3. **App Password UI**: /app/routes/Root/components/AppPassword/index.js
4. **WorkFlow**: /app/routes/Root/components/Main/index.js
5. **Backend**: /app/saga/login.js
6. **React Reducer**: /app/reducers/login.js
***



## 技术设计
### 详细流程说明
1. 打开Safari的内置浏览器，重定向至[SiteMinder](https://maam-dev.axa.com/maam/v2/authorize?scope=openid%20axa-sg-lifeapi&client_id=43dd75bc-3cae-4281-906b-fe0404ddab75&redirect_uri=easemobile://&response_type=code)

2. 当用户名密码输入正确之后，网页会多次进行跳转

3. 最终app获取到``authCode``

4. [React Native Safari View](https://github.com/naoufal/react-native-safari-view)会关闭，app会收到带回来的`authCode`

5. 通过拿回来的`authCode`请求AXA认证服务器获取 `authToken` 和 `idToken`

6. AXA认证服务器返回`authToken` 和 `idToken`

7. 请求 [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_activate)
进行用户的激活

8. Mobile API返回`userId`, `installationId`, `sessionCookieSyncGatewaySession`, `sessionCookieExpiryDate`, `sessionCookiePath` 和 `syncDocumentIds`字段

9. 用户输入离线密码

    两个key会通过AES256加密方式生成：`sqliteEncryptionKey`和``masterKey``

        const sqliteEncryptionKey = aesGen256(); 

        const masterKey = aesGen256();

    masterKey和appPassword加密会生成encryptedMasterKey。

        const encryptedMasterKey = aesEncrypt(masterKey, appPassword);
    
10. 初始化Couchbase Lite和SQLite DB
11. 请求[EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/put_auth_encryptionKey)去让Oracle保存`masterKey` 和 `installationId`
12. 不同的key会存在不同的地方
    |Fields / DB Stored   |Keychain      |SQLite         |Couchbase Lite |121 Database   |
    |:------------:       |:------------:|:------------: |:------------: | :------------:|
    |appPassword          |✓             |✓              |               |               |
    |appPasswordExpiryDate|✓             |               |               |               |
    |sqliteEncryptionKey  |✓             |               |               |               |
    |masterKey            |              |               |               |✓              | 
    |installationId       |✓             |               |               |✓              | 
    |userId               |✓             |               |               |               | 
    |lastDataSyncTime     |✓             |               |               |               | 
    |encryptedMasterKey   |              |✓              |               |               |  
    |hashedappPassword    |              |✓              |               |               |  
    |lastLoginTime        |              |✓              |               |               |  
    |environment          |              |✓              |               |               |  
    |loginFailCount       |              |✓              |               |               |  
    |syncDocumentIds      |              |✓              |               |               |   
    |agent Data           |              |               |✓              |               |  
    |policy numbers       |              |               |✓              |               | 
13. 激活期间导入默认的JSON进Couchbase Lite
14. 请求[EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/#/User/get_mock_user) 去获取agent用户信息。agent用户信息会在app被创建，并保存在Couchbase Lite内。

### 保单号
因为该APP支持离线模式，所以在激活的时候会同时预先导入非shield和shield的保单号。目前来说，我们设置了最大离线存储的保单号数量为10个非shield保单号和20个shield保单号

        MAX_OF_NON_SHIELD_POLICY_NUM = 10
        MAX_OF_SHIELD_POLICY_NUM  = 20

在激活的时候，
- 会请求[EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber)去获取最大数量的shield保单号`MAX_OF_SHIELD_POLICY_NUM`和非shield保单号`MAX_OF_NON_SHIELD_POLICY_NUM`

- 在Couchbase Lite中创建一个名为“policyNumnerPool"的JSON。 这是个例子：[policyNumberPool](assets/authentication/policyNumberPool.json)

其他关于保单号的操作如下表：
|时间段            |提示信息                                                                                            |
|:----------------:|:---------------------------------------------------------------------------------------------------------:|
|激活    | 请求[EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber)去获取最大数量的shield保单号`MAX_OF_SHIELD_POLICY_NUM`和非shield保单号`MAX_OF_NON_SHIELD_POLICY_NUM`，并且创建一个名为“policyNumnerPool"的JSON并保存在CouchbaseLite里|
|在线登陆      |检查是否需要补充shield或者非shield的保单号|
|离线登陆     |（无任何行为）|
|派保单号|在Couchbase Lite “policyNumberPool”中更新使用状态，字段：`isUsed`|
|提交保单         |在Couchbase Lite中更新“policyNumberPool”|

### 和激活相关的mobile api接口
|场景|EASE Mobile API 接口|
|:-----------------|:---------------------------------------------------------------------------------------------------------|
|Get Public Key（获取公钥）|[/auth/publicKey](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/get_auth_publicKey)|
|Check on multiple device and latest version（检查多设备登陆状态和app版本号检查）|[/auth/check](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_check)|
|Activate User（激活）|[/auth/activation](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_activate)|
|Store master Key （储存master key）|[/auth/encryptionKey](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/put_auth_encryptionKey)|
|Get Policy Number （获取保单号）|[/application/getPolicyNumber](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber)|
|Get Agent Data （获取agent用户信息）|[/mock/user](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/User/get_mock_user)| 

### 相关文件
有如下的相关文件

1. **Environment UI**: /app/routes/Root/components/Environment/index.js 
2. **Activation UI**: /app/routes/Root/components/AppActivation/index.js
3. **App Password UI**: /app/routes/Root/components/AppPassword/index.js
4. **WorkFlow**: /app/routes/Root/components/Main/index.js
5. **Backend**: /app/saga/login.js
6. **React Reducer**: /app/reducers/login.js
***