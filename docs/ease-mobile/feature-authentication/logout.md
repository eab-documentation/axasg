---
id: logout
title: Logout
sidebar_label: Logout
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Mandy Yiu |


## Business Logic

If the user presses the **Log Out** button on the left bar, it prompts the user for confirmation via a confirmation dialog.

Two cases are listed as follows: 
|Login Mode                    |Description                                                                             |
|:----------------------------:|:------------:                                                                          |
|If user logins as online mode |pressing "YES" button on the dialog triggers data sync and then redirects to AXA logout Page. Press "Done" at top right corner to re-reach the online login page.|
|If user logins as offline mode|pressing "YES" button on the dialog redirects to offline login page. |

![Logout - left bar](assets/authentication/logout1.png) 

![Logout - confirmation dialog](assets/authentication/logout2.png) 

![Logout - AXA logout page](assets/authentication/logout3.png) 

![Logout - online login Page](assets/authentication/logout4.png) 

***
## Technical Design 

Diagram as below shows Logout as offline mode and Logout as online mode. 

![Logout Diagram](assets/authentication/logoutDiagram.png) 

### Files related
Paths of related files are as follows: 

1. **Left Bar UI**: /app/routes/Landing/components/Home/index.js
***
