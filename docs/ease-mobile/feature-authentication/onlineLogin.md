---
id: onlineLogin
title: Online Login 在线登录
sidebar_label: Online Login 在线登录
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Mandy Yiu |
| 1.1 | 24 JAN 2019 | Add Policy Number, Time Zone and Time Bomb | Mandy Yiu |
| 1.3 | 4 Dev 2019 | Online Login Flow Image | Kevin Liang |

## Online Login Flow Image 在线登录流程图
[![Online Login Flow Image](assets/authentication/onlineLoginFlow.png)](assets/authentication/onlineLoginFlow.png)

## Business Logic
After installing the App, user is required to log in with SiteMinder through a Safari View. 
When user launches the app, check whether there is internet connectivity. 

> **Notes:** Online Login must be performed with internet connectivity. Otherwise, user logins as [Offline Mode](offlineLogin.md). 


User is needed to perform steps shown as follows: 
1.	Reach Online Login Page when launching  the app. 

    ![Online Login](assets/authentication/onlineLogin.png) 

2. Press “Continue” to reach SiteMinder Page and enter Username and Password to perform SiteMinder Authentication. 

    ![SiteMinder](assets/authentication/siteMinder.png) 

3. SiteMinder will ask you provide OTP(One-time password). Testing accounts(EAB, AXA-SIT, AXA-UAT, AXA-preprod environment) bind a phone number. You can following the [guide](siteminderOtp.md) to get OTP.

![SiteMinder OTP](assets/authentication/siteminder_otp.png)

4. App performs check on policy number, time zone, time bomb during Online Login. If violated, warning message or responding action is then triggered. Details of Time zone and Time Bomb are linked below: 
    -   [Policy Number](onlineLogin.md#policy-number)
    -   [Time Zone](onlineLogin.md#timezone)
    -   [Time Bomb](onlineLogin.md#timebomb)

5. Data Sync is triggered as following figure. 

    ![Data Sync](assets/authentication/dataSync.png) 

6. Reach landing Page if the above steps are completed. 

    ![Landing Page](assets/authentication/landingPage.png) 


### Reset App Password Manually 

Users are allowed to set App Password Manually by pressing "Reset App Password".  If users logins as offline mode, online authentication is required to perform before reset [App Password](activation.md#app-password). 

![Reset App Password Manually - Left Bar](assets/authentication/resetAppPasswordManually1.png) 

![Reset App Password Manually - Require online Auth](assets/authentication/resetAppPasswordManually2.png) 

![Reset App Password Manually Page](assets/authentication/resetAppPasswordManually3.png) 


***


### Policy Number 
During Online Login, app checks `numOfRequiredNonShieldNum` and `numOfRequiredShieldNum` computed as follows to refill policy number pool for BOTH non-shield and shield cases. 
        
        numOfRequiredNonShieldNum = MAX_OF_NON_SHIELD_POLICY_NUM – numOfUsedNonShieldNum
        numOfRequiredShieldNum = MAX_OF_SHIELD_POLICY_NUM - numOfUsedShieldNum
where `MAX_OF_NON_SHIELD_POLICY_NUM = 10` and `MAX_OF_SHIELD_POLICY_NUM = 20`. 

Other Usage of Policy Number is listed in the following table. 

|During            |Warning Message                                                                                            |
|:----------------:|:---------------------------------------------------------------------------------------------------------:|
|App Activation    | Invoke API call to request MAX_OF_NON_SHIELD_POLICY_NUM  and MAX_OF_SHIELD_POLICY_NUM. and then create a JSON namely “policyNumberPool” and stored policy numbers in Couchbase Lite.|
|Online Login      |Check is performed to refill number of required non-shield and shield policy number.|
|Offline Login     |No action|
|Apply Policy Number|Update "isUsed" in th“policyNumberPool” to Couchbase Lite|
|Submission         |Update “policyNumberPool” to Couchbase Lite|


### Timezone
This app requires your device to be in Singapore time zone. Warning Message shown if violated. User is not allowed to login until valid zone is detected. 
|Time Zone|Message shown |Action|
|:--------|:-------------|:------|
|Singapore| - | Allowed to Login|
|Not Singapore|This app requires your device to be in Singapore time zone. Please change the time zone to Singapore and log in again.| Not allowed to Login|

### Timebomb
The App requires user to perform data sync regularly. Timebomb is triggered if consecutive number of days in offline mode exceeds designated days. It clears all data stored in Couchbase Lite. User is needed to delete the app entirely and perform re-installation and activation. Details are listed below: 

|# of consecutive days in offline mode|Action| Message shown if triggered| 
|:-----------------------------------|:-----|:--------------------------|
|5|Warning message pops up to remind user to perform data Sync.|Time Bomb for this App will explode in <x>. Please connect to internet and Login.|
|8|Data Sync is forced to performed after Login|App is locked because Timebomb for this App will explode <x> day(s). Please perform data sync first.|
|15|Clear data stored in Couchbase Lite. Not allowed to Login.|The app has been locked as you have not synced your data recently. Please uninstall and install the app again.|

## Technical Design 

### Reset App Password Manually 
1. The newly-entered App Password also requires to obey [App Password](activation.md#app-password).

2. Invoke [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_login) call to request `masterKey`. 

3. Update newly-encryted **encryptedMasterKey** to SQLite. masterKey is encrypted by newly-created **appPassword** as encryptedMasterKey.  

        const newEncryptedMasterKey = aesEncrypt(masterKey, newAppPassword);
    
> **Notes:** masterKey is stored in 121 Online Database so online authenitcation is required to set App Password Manually. 

### EASE Mobile API related to Online Login
Table as follows lists all [EASE Mobile API](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/) during Online Login.

|Usage|EASE Mobile API Endpoint|
|:-----------------|:---------------------------------------------------------------------------------------------------------|
|Get Public Key|[/auth/publicKey](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/get_auth_publicKey)|
|Check on multiple device and latest version|[/auth/check](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_check)|
|Authenticate User|[/auth/login](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Auth/post_auth_login)|
|Get Policy Number|[/application/getPolicyNumber](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber)|

> **Notes:** [/application/getPolicyNumber](http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/docs/index.html#/Application/post_application_getPolicyNumber) is required only if refill. 


### Files related
Paths of related files are as follows: 
1. **Reset App Password UI**: /app/routes/Root/components/AppPassword/index.js
2. **Backend**: /app/saga/login.js  -  resetAppPasswordManually
***
