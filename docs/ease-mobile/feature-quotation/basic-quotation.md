---
id: basic-quotation
title: Quotation
sidebar_label: Quotation
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1 | 24 JAN 2019 | Init | CK Mo|
| 1.1 | 25 JAN 2019 | Edited & Reviewed | Keith Lam |

All handling are handled by the quotation engine. If you want to know the details, please ask
web site team for help.
```js
basicQuotation\index.js
```
## Business Logic

### Dynamic Form
All fields in Quotation are defined by the corresponding Json[^1]. 

- If Fund is defined, it will create an input form for fund.
```
"fundCode": "asd",
"fundInd": "Y",
"fundList": [
    "AEMD",
    "AGHY",
    "ALVE",
    "AUTR"
]
```
- If policyOptions is defined, i will create couple options for input.
```
"policyOptions": [
    {
        "calcRef": "N",
            "disable": "N",
            "groupID": "basic",
            "id": "paymentMethod",
            "mandatory": "Y",
            "options": [
                {
                    "title": {
                        "en": "Cash"
                    },
                    "value": "cash"
                },
                {
                    "title": {
                        "en": "SRS"
                    },
                    "value": "srs"
                }
            ],
    }
]
```
- If rider is defined, it will create an rider input form.
```
"riderList": []
```
### Calculation
Every action in Quotation will trigger a re-calc program to make sure the data are correct. All calculation
logic are defined by the corresponding functions in the product Json[^1]. Below is a sample of functions AWT product. 
Each products has different number of functions and there are some common one. In order to understand how the functions work, please consult the creator from web team/china team. If there is calculation issue, please check with the creator.

```
"formulas": {
    "IRR": 
    "adjustNum": 
    "calcQuotFunc": 
    "illustrateFunc":
    "polTermsFunc": 
    "premFunc": 
    "premTermsFunc":
    "prepareAttachableRider": 
    "prepareClassConfig": 
    "preparePolicyOptions":
    "prepareQuotConfigs": 
    "prepareSAPremConfig":
    "prepareTermConfig": 
    "saFunc": 
    "validAfterCalcFunc": 
    "validBeforeCalcFunc": 
    "validQuotFunc": 
    "willValidQuotFunc":
}
```
### Create proposal
Create proposal require Quotation is valid. All the required fields are highlighted in red. In general, you have to enter the correct amount of permium and enter all required quotation options. For inverstment linked products, you need select funds and allocated 100% of your amount. The "Create Proposal" will turn blue and clickable if all requirement meet. You may check the mandatory fields in the product json for reference.

```
"calcRef": "N",
"disable": "N",
"groupID": "basic",
"id": "deathBenefit",
"mandatory": "Y",
```

### FNA Data
FNA Data affect Quotation calculation, available funds and validation.
In general, you need to enter
1. Personal Data Acknowledgement 
2. Financial Evaluation - Make sure you have entered the fianances and budgets.
3. Needs Analysis - Make sure you have selected the needs.
4. Customer Knowledge Assessment - For investment linked product, this forms must completed.
5. Risk Assessment - For investment linked product, this forms must completed.

### Quick Quote
Quick Quote is a Quotation with less requirement, such as no FNA requirement and less Client Profile requirement.
You may use Quick Quote to check the product quote and BI. There is no difference between the normal & quick quote input form.
The difference is the BI print out whether quick quote BI comes with signature section and normal quote does not.
To access quick quote. You need to complete a mimium client profile.
```
Name
Gender
Title
Date of Birth
Nationality
Smoking Status
Maritual Status
Occupation
```

## Technical Design
There are no data handling in the app side and Web-API. All data handling locate in Core-API.  
Most UI behaviors are related to Quotation Data.

## Notices
Quotation table use many hard code style logic to modify the layout. Please check function `getTableConfig`
to get the details.

Since different products have different basic plan input fields and we share a single template. Please check the code and make sure it is aligned nicely.

- AXA Life Treasure (ALT) layout
![(assets/img/quotation/quotation1.png) ](assets/img/quotation/quotation1.png)

- Term Protector Prime layout
![(assets/img/quotation/quotation2.png) ](assets/img/quotation/quotation2.png)

- AXA Wealth Treasure (AWT) layout
![(assets/img/quotation/quotation3.png) ](assets/img/quotation/quotation3.png)
```
const getStyle = ({ columnKey }) => {
      switch (String(columnKey)) {
        case "Plan name":
          switch (String(quotation.baseProductCode)) {
            case "BAA":
              return styles.baaCovNameHeader;
            case "PUL":
            case "AWT":
            case "PNP":
            case "PNP2":
            case "PNPP":
            case "PNPP2":
              return styles.compactNameHeader;
            default:
              return styles.covNameHeader;
          }
```

[^1]: Json name format: `08_product_{productCode}_{version}.json`. For example `08_product_AWT_1.json`