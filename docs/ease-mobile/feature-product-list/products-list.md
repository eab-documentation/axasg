---
id: products-list
title: Products List
sidebar_label: Products List
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 21 JAN 2019 | Init | CK Mo |

Product List used to show products. Have to be aware, most business logic are handle by core-api.
If you want to know the details, please ask website team for help.

***

## Business Logic

### Change Life Assured

Default is proposer(`For you`). Life Assured affect products filtering.

### Change Currency

Default is `All Currencies`. Currency affect products filtering. Options map and agent company[^1]
affect available options.

[^1]: Although we have this feature, but current we just only have one company code.
