---
id: client-profile
title: Client Profile
sidebar_label: Client Profile
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 17 JAN 2019 | Init | CK Mo |

Client profile is a form that used to edit or create client. Almost all section require client 
profile data. The different is each section have their own client profile requirement.

***

## Business Logic
There are three type of client profile:
1. **basic**  
Given Name, Surname, English or Other Name, Han Yu Pin Yin Name, Name, Name Order, Title, Gender,
Birthday, Nationality, Singapore PR Status, Country of Residence, City/State, City/State Other,
ID Document Type, ID Document Type other, ID Number, Smoking Status, Marital Status, Language,
Language Other, Education, Employment Status, Employment Status Other, Occupation, Occupation Other, Industry, 
Name of Employer/Business/School, Country of Employer/Business/School, Type of Pass, Type of Pass Other,
Pass Expiry Date, Allowance, Mobile No Prefix, Mobile No, Other Mobile No Prefix, Other Mobile No,
Email Address, Country, City/State(Address), Postal Code, Block/House Number, Street/Road Number,
Unit Number and Building/Estate Name.
2. **family member**.  
Base on basic, Add Relationship and Relationship Other.
3. **trusted individual**.  
Given Name, Surname, Name, Name Order, Relationship, Relationship Other, Mobile No Prefix, Mobile No,
ID Document Type, ID Document Type other and ID Number

Symbol description:  
- ✓ : yes  
- ✗ : no  
- \- : not related

### Save
- Save client require all input fields are valid.

### Relationship
- Relationship only show when edited client is current client's family member or trusted individual client profile.  
- When change Relationship, update Gender with corresponding sexual.
- If change Relationship to `Spouse`, lock Marital Status to `Married`.

**Rules**:
- **gender rule**: option should match gender.
  - ✓: relationship is father and gender is male
  - ✗: relationship is father and gender is female
- **spouse rule**: current client can not have more than two spouse.
- **spouse gender rule**: if relationship is spouse, edited client gender should match current client gender.
  - ✓: edited client's gender is male and current client's gender is female
  - ✗: edited client's gender is male and current client's gender is male

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | ✓ | - | - | - | - | ✓ |
| gender rule | ✓ | - | - | - | - | ✗ |
| spouse rule | ✓ | - | - | - | - | ✗ |
| spouse gender rule | ✓ | - | - | - | - | ✗ |

### Relationship Other
Relationship Other is additional field of Relationship
- Relationship Other only show when Relationship select `Others`.  
- This field is mandatory.

### Given Name
- This field is mandatory.

### Surname
- This field is mandatory.

### Name
- This field is mandatory.  
- When Given Name, Surname or Name Order update. Name will automatically update the value with Given Name, Surname and Name Order. 
- If Name already have value but Given Name not. User should fill Given Name's value too.

### Name Order
Default to `Surname first`

### Title
- Title should match gender.
  - ✓: Title is Mr and gender is male
  - ✗: Title is Mr and gender is female

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | ✗ | ✗ | ✓ | ✗ | ✗ | - |

### Gender
- This field is mandatory. 

### Birthday
Default option is `30 years old`. **NO DEFAULT VALUE**.  
- If age is change to less than 6, update Occupation to `Child / Juvenile / Infant`, update 
Name of Employer/Business/School and Country of Employer/Business/School to `N/A`  
- If age is change to equal or more than 6 and less than 18, update Occupation to `Student (Below age 18)`
- If age is change to less than 18, update Employment Status to `Student`
- If age is change to equal or more than 18 and Employment Status is `Student`, change Occupation
to `Student (Age 18 and above)`
- Value should equal or less than today. equal or more than 100 years before 

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | ✓ | ✗ | ✓ | ✓ | ✓ | - |

### Singapore PR Status
show when Nationality is `Singapore`.
- This field is mandatory. 

### Country of Residence
Default to `Singaopore`  
When user change this value, update the same value to Country(Address)
- This field is mandatory. 

### City/State
Show when Country of Residence option require City/State[^1]. like `Indonesia`.  
When user change this value, update the same value to City/State(Address)
- This field is mandatory. 

### City/State Other
Show when City/State option related to other. like `OTHER THAN Aceh, West Timor, Sulawesi, Maluku`[^1].
- This field is mandatory. 

### ID Document Type
If Country of Residence is `Singapore` or Singapore PR Status is `Yes`, ID Document Type is 
`NRIC`

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | ✓ |

### ID Document Type other
show when ID Document Type is `Others`.
- This field is mandatory. 

### ID Number
- ID Number should be a valid value of the selected ID Document Type

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | ✓ |

### Smoking Status
| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✗ | ✓ | ✓ | - |

### Marital Status
| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | - |

### Language
| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | - |

### Language Other
show when Language has select `Others`.
- This field is mandatory. 

### Education
| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | - |

### Employment Status
- When Employment Status change to Student and age is less than 18, update Employment Status to `Student (Below age 18)`
- When Employment Status change to Student and age is equal or more than 18, update Employment Status to `Student (Age 18 and above)`
- Employment Status should match gender.
  - ✓: Employment Status is Househusband and gender is male
  - ✗: Employment Status is Househusband and gender is female

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | - |
  
***

Employment Status will affect other fields:

| Value | Occupation | Industry | Name of Employer/Business/School | Country of Employer/Business/School |
| - | - | - | - | - |
| Househusband | Househusband | General | N/A(lock) | N/A(lock) |
| Housewife | Housewife | General | N/A(lock) | N/A(lock) |
| Retiree | Retiree / Pensioner | General | N/A(lock) | N/A(lock) |
| Unemployed | Unemployed | General | N/A(lock) | N/A(lock) |

Select Option that not in this list do not have any effect. But if selecting option is one of options in the list, 
All effected fields should reset.

### Employment Status Other
show when Employment Status has select `Others`.
- This field is mandatory. 

### Occupation
- Occupation options have filter by Industry value
- When user select a option(**not clean**), auto update Industry with corresponding industry[^1].

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✓ | ✓ | - |
  
***

Occupation will affect other fields:

| Value | Employment Status | Industry | Name of Employer/Business/School | Country of Employer/Business/School |
| - | - | - | - | - |
| Househusband | Househusband | General | N/A(lock) | N/A(lock) |
| Housewife | Housewife | General | N/A(lock) | N/A(lock) |
| Retiree / Pensioner | Retiree | General | N/A(lock) | N/A(lock) |
| Unemployed | Unemployed | General | N/A(lock) | N/A(lock) |
| Student (Below age 18) | Student | General | - | - |
| Student (Age 18 and above) | Student | General | - | - |

Select Option that not in this list do not have any effect. But if selecting option is one of options in the list, 
reset Employment Status, Name of Employer/Business/School and Country of Employer/Business/School.

### Occupation Other
show when Occupation has select `OTHERS`.

### Industry
When user change Industry, If Occupation not match the selected option[^1], clean the value. 

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✓ | ✓ | - |

### Type of Pass
show When Singapore PR Status is `No`

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✗ | ✗ | ✓ | - |

### Type of Pass Other
show When Type of Pass is `Other`
- This field is mandatory. 

### Pass Expiry Date
Show When Singapore PR Status is `No`
- Value should equal or more than today. equal or less than 10 years later 

### Allowance
| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | - |

### Mobile No Prefix
Default to `+65`

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | ✓ |

### Mobile No
| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✓ | ✗ | ✓ | ✓ |

### Other Mobile No Prefix
Default to `+65`

### Email Address
Value should be a valid email address

| Requirement | Family Member | Create Client | FNA | Product | Application | Trusted Individual |
| - | - | - | - | - | - | - |
| mandatory | - | ✗ | ✗ | ✗ | ✓ | ✗ |

### Postal Code
If Country of Residence is `Singapore` and Postal Code is valid, auto update fields that related to
Address[^2].

### Allowance
Format to Currency.
- Value should be more than $0
- Value can't change to less then $0 even if clean the field value.

## Technical Design
All logic of the Client Profile are configure with reducer `clientForm/config`.  
Each field have their own reducer and action type. All side effect saga(include validation) is
trigger by these independent action.

## Notices
The naming and data structure between Web and App is different. So every times we try to get the
data from database, we have to remap and initialize the data.

[^1]: Please check options map to get the relationship
[^2]: The address specified by the Postal code can check on files
named like `postal_code_cov_XX_XX`. For example `postal_code_cov_80_89`
