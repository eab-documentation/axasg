---
author: Walter Lam
id: applicants
title: Applicants
sidebar_label: Applicants
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Walter Lam |

#### Applicants page allows user to select application for Myself/Spouse/Dependants.

***

## User Interface(s)

#### Applicants
![alt-text](assets/img/pda/applicants_1.png)
***

#### Create Spouse
![alt-text](assets/img/pda/applicants_2.png)
***

#### Create Family Member
![alt-text](assets/img/pda/applicants_3.png)
***

#### Link Family Member
![alt-text](assets/img/pda/applicants_4.png)

![alt-text](assets/img/pda/applicants_5.png)

***

## Business Requirement

### Selected Client
- Selected client can access the CLIENT'S ACCOMPANIMENT tab
- Requires 2 of 3 below conditions:
    - Age 62 or above 
    - Below GCE 'N' or 'O' Levels certificate
    - Non ENGLISH Speaker

|   Age 62 or above  |   Below GCE 'N' or 'O' Levels certificate |    Non ENGLISH Speaker |    Selected Client?   |
|:------------------:|:-----------------------------------------:|:----------------------:|:---------------------:|
|         ✕          |                  ✕                        |            ✕           |         ✕             |
|         ✕          |                  ✓                        |            ✕           |         ✕             |
|         ✕          |                  ✕                        |            ✓           |         ✕             |
|         ✕          |                  ✓                        |            ✓           |         ✓             |
|         ✓          |                  ✕                        |            ✕           |         ✕             |
|         ✓          |                  ✓                        |            ✕           |         ✓             |
|         ✓          |                  ✕                        |            ✓           |         ✓             |
|         ✓          |                  ✓                        |            ✓           |         ✓             |

✓ = yes  ,  ✗ = no 

***

### Applicants
- Mandatory : Yes
- Applicants value can either be MYSELF or MYSELF AND SPOUSE

| Marital   | MYSELF   | MYSELF AND SPOUSE  |  Default Value     |
|:---------:|:--------:|:------------------:|:------------------:|
| Married   |   ✓      |        ✓           |     None           |
| Single    |   ✓      |        ✕           |     MYSELF         |
| Divorced  |   ✓      |        ✕           |     MYSELF         |
| Widow     |   ✓      |        ✕           |     MYSELF         |

✓ = clickable  ,  ✗ = un-clickable 

Profile updated:
- Married -> Single/Divorced/Widow, default value -> MYSELF
- Single/Divorced/Widow -> Married, default value -> MYSELF

***

### Dependants
- Mandatory : No
- Create or Link Family Member
- Maximum no. of dependants = 10

***

### Tab Icon

- Default =  Incomplete (Blue & Digit)

| Applicants Completed? |         Tab Icon           | 
|:---------------------:|:--------------------------:|
|          ✗            |  Incomplete (Blue & Digit) |
|          ✓            |   Complete (Blue & Tick)   |

✓ = yes  ,  ✗ = no

***

### Next
- Next is used for navigation to consent notices tab, and enables when either MYSELF or MYSELF AND SPOUSE is selected

| Applicants Completed? |  Next Button  | 
|:---------------------:|:-------------:|
|          ✗            |    Disable    |
|          ✓            |    Enable     |

✓ = yes  ,  ✗ = no

***

### Save
- Always enabled
- Save the current progress and closes the page. 

***

### PDA Completed 

| Selected Client?  | Applicants         |  Consent Notices           |  Client's Accompaniment    |  PDA Completed?    |
|:-----------------:|:------------------:|:--------------------------:|:--------------------------:|:------------------:|
|        ✕          |         ✕          |           -                |             -              |         No         |
|        ✕          |         ✓          |           ✕                |             -              |         No         |
|        ✕          |         ✓          |           ✓                |             -              |         Yes        |
|        ✓          |         ✕          |           -                |             -              |         No         |
|        ✓          |         ✓          |           ✕                |             ✕              |         No         |
|        ✓          |         ✓          |           ✓                |             ✕              |         No         |
|        ✓          |         ✓          |           ✓                |             ✓              |         Yes        |

✓ = yes  ,  ✗ = no ,  - = not accessible

***

### Done
- Done is available once all the required tabs are completed

***

## Technical Design

- Component @ Needs/components/ApplicantsAndDependants/index.js
- Container @ Needs/containers/ApplicantsAndDependants.js
- Styles @ Needs/components/ApplicantsAndDependants/styles.js

## Issues



