---
author: Walter Lam
id: consent-notices
title: Consent Notices
sidebar_label: Consent Notices
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Walter Lam |

#### Consent Notices page allow users to select the method of information delivery
***

## User Interface(s)

#### Consent Notices
![alt-text](assets/img/pda/consent_1.png)
***

## Business Requirement

### Tab Icon

- Default =  Blocked (Grey & Digit)

| Applicants Completed?   | Consent Notices Completed? |         Tab Icon           | 
|:-----------------------:|:--------------------------:|:--------------------------:|
|          ✗              |               -            |   Blocked (Grey & Digit)   |
|          ✓              |               ✗            |   Incomplete (Blue & Digit)|
|          ✓              |               ✓            |   Complete (Blue & Tick)   |

✓ = yes  ,  ✗ = no ,  - = not accessible

***

### Previous
- Always enabled
- Save the current progress and navigate back to Applicants page.

***

### Next / Continue to Financial Evaluation
- Next navigates to the CLENT'S ACCOMPANIMENT TAB for Selected Client
- Continue to Financial Evaluation navigates to Financial Evaluation for Non-Selected Client

| Selected Client? |  Next Button  | Continue to Financial Evalution Button | 
|:----------------:|:-------------:|:--------------------------------------:|
|        ✗         |       N/A     |                   ✓                    |
|        ✓         |        ✓      |                  N/A                   |

✓ = yes  ,  ✗ = no , N/A = not available

***

### Done
- Done is available once all the required tabs are completed
- Set PDA status to "EDIT"

| Selected Client?  | Applicants Completed? | Consent Notices Completed? | Done Button?     |
|:-----------------:|:---------------------:|:--------------------------:|:----------------:|
|        ✕          |         ✕             |           -                |    No            |
|        ✕          |         ✓             |           ✕                |    No            |
|        ✕          |         ✓             |           ✓                |    Yes           |
|        ✓          |         ✕             |           -                |    No            |
|        ✓          |         ✓             |           ✕                |    No            |
|        ✓          |         ✓             |           ✓                |    No            |

✓ = yes  ,  ✗ = no ,  - = not accessible

***

### Save
- Always enabled
- Save the current progress and close the page.

***

### Consent Method Checkbox
- Mandatory : No

***

## Technical Design

- Component @ Needs/components/ApplicantsAndDependants/index.js
- Container @ Needs/containers/ApplicantsAndDependants.js
- Styles @ Needs/components/ApplicantsAndDependants/styles.js

## Issues




