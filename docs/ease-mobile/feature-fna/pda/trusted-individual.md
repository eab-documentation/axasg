---
author: Walter Lam
id: trusted-individual
title: Trusted Individual (Client's Accompaniment)
sidebar_label: Trusted Individual
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Walter Lam |

#### Client Accompaniment page allows users to select a trusted individual for the application.

***

## User Interface(s)

#### Trusted Individual Options
![alt-text](assets/img/pda/trustedIndividual_1.png)
***

#### Trusted Individual Form
![alt-text](assets/img/pda/trustedIndividual_2.png)
***

#### Pick from Contact
![alt-text](assets/img/pda/trustedIndividual_3.png)
***

## Business Requirement

### Save
- Always enabled
- Save the current progress and close the page. 

***

### Done
- Save the current progress and close the page.
- Set PDA status to "EDIT"
- Done is available when:
    - Trusted Individual = N
    - Trusted Individual = Y and TI Profile completed

| Trusted Individual |  TI Profile Completed  | Done | 
|:------------------:|:----------------------:|:----:|
|        ✗           |        -               |   ✓  |
|        ✓           |        ✗               |   ✗  |
|        ✓           |        ✓               |   ✓  |

✓ = yes  ,  ✗ = no ,  - = not accessible

***

### Previous

- Always enabled
- Save the current progress and navigate back to Consent Notices page.

***

### Continue to Financial Evaulation
- Saves the current page and navigates to Financial Evaluation page

| Trusted Individual |  TI Profile Completed  | Show Financial Evaluation Button | 
|:------------------:|:----------------------:|:--------------------------------:|
|        ✗           |        -               |              ✓                   |
|        ✓           |        ✗               |              ✗                   |
|        ✓           |        ✓               |              ✓                   |

✓ = yes  ,  ✗ = no , - = not available

***

### Trusted Individual Options
- Mandatory : Yes

***

### Trusted Individual Profile
- A form used to save trusted individual profile.
- Available when Trusted Individual Options = Y

| Trusted Individual |   Form Available       |
|:------------------:|:----------------------:|
|        ✗           |        ✗               |
|        ✓           |        ✓               |

✓ = yes  ,  ✗ = no

***

### Tab Icon
- Default = Incomplete (Blue & Digit)
- Trusted Individual = N and TI profile complete =>  Complete (Blue & Tick)
- Trusted Individual = Y and TI profile complete =>  Complete (Blue & Tick)
- Trusted Individual = Y and TI profile incomplete => Incomplete (Blue & Digit)

| Trusted Individual   | TI Profile Completed       |         Tab Icon            | 
|:--------------------:|:--------------------------:|:---------------------------:|
|          -           |               -            |   Incomplete (Blue & Digit) |
|          ✗           |               -            |   Complete (Blue & Tick)    |
|          ✓           |               ✗            |   Incomplete (Blue & Digit) |
|          ✓           |               ✓            |   Complete (Blue & Tick)    |

✓ = yes  ,  ✗ = no ,  - = not available

***

## Technical Design

- Component @ Needs/components/ApplicantsAndDependants/index.js
- Container @ Needs/containers/ApplicantsAndDependants.js
- Styles @ Needs/components/ApplicantsAndDependants/styles.js

## Issues
