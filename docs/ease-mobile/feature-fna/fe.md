---
id: fe
title: Financial Evaluation
sidebar_label: Financial Evaluation
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 23 JAN 2019 | Init | CK Mo |

## Business Logic

### Dependant Financial Evaluation
| Section | Client | Client spouse | Client son | Client daughter |
| - | - | - | - | - |
| My finances (net worth) | ✓ | ✓ | ✗ | ✗ |
| Existing Insurance Portfolio | ✓ | ✓ | ✓ | ✓ |
| My finances (Cash Flow) | ✓ | ✓ | ✗ | ✗ |
| Budget | ✓ | ✗ | ✗ | ✗ |
| Income or Expense Confirm | ✓ | ✗ | ✗ | ✗ |
The others dependants are not allow access Financial Evaluation

### My finances (net worth)
**Rules**:
- **other asset rule**: If the user fills in one of other asset field or other asset
value field, another one need to be filled out.
- **other liability rule**: If the user fills in one of other liability field or other liability
value field, another one need to be filled out.
- **reason rule**: If none fields has been fill, need to provide reason(s) for not documenting this information.

| Section | Client | Client spouse | Client son | Client daughter |
| - | - | - | - | - |
| other asset rule | ✓ | ✓ | ✓ | ✓ |
| other liability rule | ✓ | ✓ | ✓ | ✓ |
| reason rule | ✓ | ✗ | ✗ | ✗ |

### Existing Insurance Portfolio
**Rules**:
- **mandatory rule**: If the user fills in one of Existing Insurance Portfolio fields or Insurances Premium
fields, need to be filled out at least one field of another section.
- **reason rule**: If none fields has been fill, need to provide reason(s) for not documenting this information.

| Section | Client | Client spouse | Client son | Client daughter |
| - | - | - | - | - |
| mandatory rule | ✓ | ✓ | ✓ | ✓ |
| reason rule | ✓ | ✗ | ✗ | ✗ |

### My finances (Cash Flow)
**Rules**:
- **other expense rule**: If the user fills in one of other expense field or other expense
value field, another one need to be filled out.
- **reason rule**: If none fields has been fill, need to provide reason(s) for not documenting this information.

| Section | Client | Client spouse | Client son | Client daughter |
| - | - | - | - | - |
| other expense rule | ✓ | ✓ | ✓ | ✓ |
| reason rule | ✓ | ✗ | ✗ | ✗ |

### Budget
- At least fill one field
- Budget can not more than corresponding asset
- If filled Regular Premium Budget, need to confirm does the budget for Regular Premium form
a substantial portion of your Cash flow surplus? If yes, need to give reason.
- If filled Single Premium Budget, need to confirm Does the budget for Single Premium form a
substantial portion of your assets (including Savings, fixed deposit(s) and investments)?
If yes, need to give reason.

### Income or Expense Confirm
- If yes, need to give reason.
