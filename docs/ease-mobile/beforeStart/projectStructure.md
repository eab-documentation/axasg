---
id: project-structure
title: Project Structure 项目结构
sidebar_label: Project Structure 项目结构
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 5 Dec 2019 | Init | Kevin Liang |

## Background
Axa-sg-app is the main project. ``eab-web-api`` and ``eab-core-api`` are child projects.The ``eab-core-api`` project is as same as the bz folder under ease-web project. It's designed for share using between ease-web and ease ios project. But... we can't handle this project very well, so finally fail.
## Structure
```
axa-sg-app
└───node_modules
│    └───eab-core-api
│    └───eab-web-api
```
## About SmartBuild
>Concept: SmartBuild means build and integration. Firstly build source code and then move the build result into main project ``axa-sg-app``

Remember to execute ``npm run smartBuild`` when you edit 3 projects together. It will compose and move to main project, or it will be work on main project. Also, when you build ipa package, remember to pull the latest code of 3 projects and then smartBuild into main project.

## Note
> Note: Under ``eab-core-api`` and ``eab-web-api`` projects, there are 2 folders under root folder - lib folder and src folder. Src folder includes the source code. Lib folder is the result of compose. So when you do global search, ignore the contents under lib folder.

## 背景
Axa-sg-app是主项目，``eab-web-api``和``eab-core-api``是子项目，``eab-core-api``是网页版代码bz文件夹下面的内容，几乎是一样的, 当时想着将``eab-core-api``作为ios和web开发的共享工程，但是发现两边的代码无法进行很好的差异化管理，因此保留了这样一种项目结构
## 结构
```
axa-sg-app
└───node_modules
│    └───eab-core-api
│    └───eab-web-api
```
## 关于smartBuild
>概念：smartBuild也就是build好之后做集成。具体来说就是先build代码，再把build好的代码搬到主项目``axa-sg-app``中

同时编辑三个工程的时候记得，如果在eab-core-api或者eab-web-api中修改了代码，一定要``npm run smartBuild``,把更改放进主项目``axa-sg-app``中，否则不会生效。在手动打包ipa的过程中，也一定要注意同时拿3个项目最新的代码，``eab-core-api``或者``eab-web-api``要smartBuild进``axa-sg-app``中。
## 注意事项
> 备注：在``eab-core-api``和``eab-web-api``代码中，根目录下面有两个文件夹，一个是lib, 一个是src，其中src是代码的文件，lib是src下的文件通过编译形成的文件，所以在全局搜索的时候可以忽略``eab-core-api``和``eab-web-api``项目中lib文件夹下面的内容。