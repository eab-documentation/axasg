---
id: debug-method
title: Debug Method 调试方法
sidebar_label: Debug Method 调试方法
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 5 Dec 2019 | Init | Kevin Liang |

APP的开发分为两种语言，一种是react native，一种是Objective-C

因为有些功能没有node相关的sdk或者lib，因此使用了Objective-C，包括签名功能，Couchbase Lite的数据库操作，PDF创建

所以调试分为两种，一种是Objective-C相关的代码，需要看XCode的右下角的log，比如调试product产品、调试data sync

另外一种就是react native的代码，需要看react native的调试器，比如激活、登陆、创建保单

## 如何使用XCode调试Objective-C的代码
在XCode当中修改代码和设置断点，在XCode下面会有控制台输出打印log

## 如何使用React Native调试器调试React Native的代码
### 使用debug模式开启模拟器的APP
在左上角EASE图标那里单击找到Edit Scheme，点击进入
![Edit Scheme](assets/img/ease-mobile/beforeStart/debugEditScheme.png)

选择debug模式
![Select Debug Mode](assets/img/ease-mobile/beforeStart/selectDebugMode.png)

### 打开react native调试器
然后启动模拟器和app，app启动后用快捷键command+D调出辅助菜单,选择Debug remotely
![Select Debug Remotely](assets/img/ease-mobile/beforeStart/selectDebugRemotely.png)

在弹出的菜单中按快捷键Option+Command+I打开开发者工具

### react native的使用
React native的Log会在console中
![React native log](assets/img/ease-mobile/beforeStart/debugLog.png)

如果想设置断点，要点开Source版面，按快捷键command+P，输入文件名，进入即可设着断点
![React native breakpoint](assets/img/ease-mobile/beforeStart/debugBreakpoint.png)

## 如何找相应的代码
### 通过React native调试器的状态跟踪代码逻辑
比如搜索CHECK_LOGIN_STATUS即可搜索到对应的代码
![Find Code By Reducer state](assets/img/ease-mobile/beforeStart/findLogicByReducerState.png)

### 通过关键字的全局搜索
如果在UI上面特殊的文字、句子，复制粘贴至IDE进行全局搜索