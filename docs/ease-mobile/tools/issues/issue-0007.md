---
id: issue-0007
title: Xcode 10 fail to build with the new build system
sidebar_label: Issue 0007
---

<table>
    <thead>
        <tr>
            <th>Version</th>
            <th>Date</th>
            <th>Desc</th>
            <th>Author</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.0</td>
            <td>14 Feb 2019</td>
            <td></td>
            <td>Jack Hon</td>
        </tr>
    </tbody>
</table>

## In short
Let's see the error log from the Xcode.
![Error log](assets/img/issues/xcode10-build-fail.png)

Multiple commands produce "xxx/xxx"..., Xcode can't build the app.

After some reserch, since Xcode9, Xcode have a new build system call "New Build System", when it's come to Xcode10, this new build system become the default build system, which cause the build fail. 

Before Xcode10, whole world's developer are using the Legacy Build System.

## New Build System
>Xcode 10 uses a new build system. The new build system provides improved reliability and build performance, and it catches project configuration problems that the legacy build system does not.
>
>https://developer.apple.com/documentation/xcode_release_notes/xcode_10_release_notes/build_system_release_notes_for_xcode_10?language=objc

As the Xcode DOCs mention, 
>Although the new build system is highly compatible with existing projects, some projects may require changes due to the following,
>...
>It is an error for any individual file in the build to be produced by more than one build command. For example, if two targets each declare the same output file from a shell script phase, factor out the declaration of the output file into a single target.

That explained well that we have to remove all the duplicate files that exists in an application bundle to one and only.

## Which files should be removed?
In our application bundle(Resources), we had build our own structure with propose, we put agent json file, let's say U_T9807923A.json, into <b>agent_account folder</b>, but we also dupliate this file into <b>demo folder</b>, then two same files exists in an applicaion bundle, which one should be removed?

If you know how bundle works, then the answer is <b>doesn't matter</b>.

We put our files into the right place just for us to manage easier. Because we don't have much methods to control the bundle.

## no --inputencodeing specified and could not detect encoding from input file
The next issue we encounter is like the title. Check the error from the Xcode, we can see errors happend on these <b>.strings file</b>
![string-file](assets/img/issues/string-file.png)

![string-file](assets/img/issues/string-file2.png)

### What is .strings file
>The STRINGS files that are used by the Apple Safari and Mac OS X platforms are used to create alerts, status and error messages for the user of the Mac computer. The STRINGS files that are used by these Mac operating systems may also contain language translation of an application or other localization data.

Do we decide the app to be functionally at different language? Yes.
Do we actually use different language in our app? No.

Our people whom work on the app(Objective-C) didn't know how to handle the language translation at all......

### Still Bad Practices
When we need third party library, we usually copy the example to our program, once it work, we don't do any improvement on these copy-code.

For example, we add the listener at react-native component inorder to listen the callback function from SignDoc library, but what I saw at code is <b>addListener("sayHello", ...)</b>, who can tell that "sayHello" is the SignDoc's callback???????

>![bad-practices](assets/img/issues/bad-practices.png)
>
>People always copy the online example to the program without changes which cause us hard to debug.
>In the end, I renamed it to "SignDoc" at both react-native(listener) and objective-c(callback) side.

So does the <b>.strings file</b>, whoever work on the SignDoc library on our app, they had copy the whole example into our program, with <b>Rubbish files</b> which we don't need. These rubbish files include <b>.strings file</b>.

### Solution
So we remove these rubbish files.

## um......
Please read document first...

## Last Option
As the last option, you can still switch the build system to "Legacy Build System", but the "New Build System", will probably be the future solution.
