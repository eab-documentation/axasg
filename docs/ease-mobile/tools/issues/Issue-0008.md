---
id: issue-0008
title: Cannot build (npm i) in China Office
sidebar_label: Issue 0008
---

<table>
    <thead>
        <tr>
            <th>Version</th>
            <th>Date</th>
            <th>Desc</th>
            <th>Author</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.0</td>
            <td>14 Jul 2019</td>
            <td>Initial Update</td>
            <td>Keith Lam</td>
        </tr>
    </tbody>
</table>

## In Short
When you want to compile/build the EASE-Web or any variance of 121-Web. You will encounter many different type of errors that stop you from running (npm run dev) the app.

![Error log](assets/img/issues/china-npm-install.jpg)


## Before you begin
Before you npm i, make sure you have install all the required softwares.

- Java SDK8
- python 2.7
- npm install -g windows-build-tools --vs2015
- npm config get msvs_version
- npm config set msvs_version 2015 --global

In china, you may also need to run install additional software when you see error.
![Error log](assets/img/issues/china-npm-install2.jpg)
- visual studio 2015
- set GYP_MSVS_VERSION=2015
- set GYP_MSVS_OVERRIDE_PATH=C:\Program Files (x86)\Microsoft Visual Studio 14.0

## Look closely during "npm i" & Solution

During npm i in china office, you have to look closely the installation prcoess even though the network speed is slow. If the installation pause or throw error, look closely. If your installation pause in installing "chromedriver 2.46", you have encounter the error discussed here.

The solution to this problem is simply remove the chromedriver 2.46 in devDepencies and REMOVE THE NODE_MODULES and "npm i" again.

It would still failed if you copy a workable (HK) node_module. The errors you will see is also unpredicatable. We also try VPN and you will still failed in some point.

## Reason
We have noticed that in China, the npm cannot download the chromedriver. The npm might pause or throw error. If you ignore or bypass this error, npm will let you go but it will generate many unpredicted errors during run. Therefore, a complete installation in one-go is important. It is suggested to completely remove the node_modules if any failure.
