---
id: issue-0003
title: CRUD before data sync cause can't sync any data
sidebar_label: Issue 0003
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 30 JAN 2019 | Init | Kevin Liang |

## Issue Date
29 Jan 2019

## Detail
We found that v1.3.3 b4 can perform data sync after activation, but v1.3.3 b5 can't.

## Diagnosis
We check out the commit between b4 and b5 to trace issue. We found that [commit 11c706b](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/commit/11c706b1a69abadcbc728ebac8d641cfb36ce6a5) cause this issue. After activation, system perform get avator from Couchbase Lite before data sync. There are some conflicts if we perform CRUD before data sync or perform CRUD and data sync at the same time. It will cause data sync fail.

## Solution
Update code. Perform get avatar after data sync. [commit 7954722](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/commit/79547229474bbd2f7a12b59e38d64c68982ed5b5)