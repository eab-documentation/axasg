---
id: issue-0006
title: Release build crash after app upgrade
sidebar_label: Issue 0006
---

<table>
    <thead>
        <tr>
            <th>Version</th>
            <th>Date</th>
            <th>Desc</th>
            <th>Author</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.0</td>
            <td>14 Feb 2019</td>
            <td></td>
            <td>Jack Hon</td>
        </tr>
    </tbody>
</table>

## In short
Let's see the error log from the Xcode.
![Error log](assets/img/issues/xcode-app-launch-error-log.png)

Two errors happend,
- Module AppRegistry is not a registered callable module (calling runApplication)
- 'RCTFatalException: Unhandled JS Exception: Can't find variable: n'

## Module AppRegistry
>AppRegistry is the JS entry point to running all React Native apps. App root components should register themselves with AppRegistry.registerComponent, then the native system can load the bundle for the app and then actually run the app when it's ready by invoking AppRegistry.runApplication.

According to the react-native docs, we are doing it right, still waste some time to debug with this error hint, comes up with no conclusion.

## 'Unhandled JS Exception: Can't find variable: n'

<b>Where dose the "n" come from?</b>

Usually, when error says "Can't find variable", it obviously means that the program can't execute because the reference of the variable is missing,misspelling or have been deleted somehow during the runtime. But in debug mode, everythings works fine. 

### Debug mode vs Release mode
The differences between the debug and release are similar to the differences on other system. Basically, symbolic debug information is uploaded to the debug builds to help while debugging app, but release builds, no symbolic debug information there and code execution is optimized.

### Code Optimization
>Code optimization is any method of code modification to improve code quality and efficiency. A program may be optimized so that it becomes a smaller size, consumes less memory, executes more rapidly, or performs fewer input/output operations.
>
> Optimization can be performed by automatic optimizers, or programmers. An optimizer is either a specialized software tool or a built-in unit of a compiler (the so-called optimizing compiler). Modern processors can also optimize the execution order of code instructions.

In the world of JavaScript, we do the code optimization by compress and minify the js file. After that, you will see the content of the mini-js-file would be totally different, class names, function names and variable names get renamed to be shorter, for example: "function getToken(id)" could be renamed to "function r(n)".

I believe that is where the "n" come from.

## Ridiculous Practices 
This "n" come from the file "bz/Quote/ath2.js" at "core-api", what a big shock to me to know there are people whom actually doing such self-idea things.

Point:
- You don't require any mini-js-file when you have webpack or optimizing compiler to do the mini job. Why do so???
- You don't mix different library code together into one single file and then do the minify by yourself. Why do so??? Al least put different library into different file with meaningful file name, <b>math2.js with different library code inside it ??????????????????????????????</b>

## Root cause
Xcode have its own compiler to do the code optimization, if we require our own minify-js-file, after we build the app under release mode, code conflict happend, which cause the problem 'Unhandled JS Exception: Can't find variable: n', and this 'n' problem cause "Module AppRegistry" problem. If we use the normal js file/ libaray, then no more error happend.

### Why Xcode 9.x works fine before with release mode build?
Don't know why, not much info on the internet, and [Build System Release Notes for Xcode 10](https://developer.apple.com/documentation/xcode_release_notes/xcode_10_release_notes/build_system_release_notes_for_xcode_10?language=objc) don't mention anything releated to our issues.

## Why iOS 11 SDK wroks fine before with any mode build?
[iOS 12 Release Notes](https://developer.apple.com/documentation/ios_release_notes/ios_12_release_notes?language=objc) do mention an issues about iOS 12,
>Various Core Graphics calls have been hardened against continuing with invalid parameters. In iOS 12, these calls may now return nil or return early. (38344690)

but is Core Graphics related to the app launch process, we still don't know.

## Solution
We need to find the right library to replace this math2.js!
