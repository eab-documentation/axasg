---
id: issue-0001
title: Objective C NSDateFormatter return date with 1 more year
sidebar_label: Issue 0001
---

## Issue Date
31 Dec 2018  

## Detail
EASE Mobile Pilot launch was planned on 31 Dec 2018. The DVT was passed on 28 Dec 2018, but user report the data sync is not working suddenly on 31 Dec.
Symptom:
- Able to do activation
- After activation, make some change in either online or offline, press Sync button
- System prompt "data is up to date" message

## Diagnosis
- The mobile app has not been changed since 28 Dec 2018
- The Mobile API and server side program logic has not been changed since 28 Dec 2018
- In the Mobile API server, found the data sync request has passed a future last sync date as 31 Dec 2019. This is the reason system think there is no data changed.
- We focus on the Mobile APP Programming Code with break point to find out how's the date is generated.
- At last, we found the date "2018-12-31" is presented as "2019-12-31" after NSDateFormatter format the date with the format @"YYYY-MM-dd HH:mm:ss:SSS"

## Solution
- Work around - ask AXA not to activate the mobile application until 1 Jan 2019
- Program fix - change the date format from  YYYY to yyyy

## External reference 
People says  
@"YYYY" is week-based calendar year.  
@"yyyy" is ordinary calendar year.

Reference: [stackoverflow](https://stackoverflow.com/questions/15133549/difference-between-yyyy-and-yyyy-in-nsdateformatter)







