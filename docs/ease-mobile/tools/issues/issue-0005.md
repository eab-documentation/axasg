---
id: issue-0005
title: Issues during react-native upgrade
sidebar_label: Issue 0005
---

<table>
    <thead>
        <tr>
            <th>Version</th>
            <th>Date</th>
            <th>Desc</th>
            <th>Author</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.0</td>
            <td>14 Feb 2019</td>
            <td></td>
            <td>Jack Hon</td>
        </tr>
    </tbody>
</table>

## In short
Starting March 2019, all iOS apps submitted to App Store must be built with the iOS 12.1 SDK or later, included in Xcode 10.1 or later. We need to upgrade our react-native version to at least v0.57(iOS 12.1 SDK support), which cause some logic are not function correctly.
>### Task list
>
>http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1992

## React
>http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1989

These Known issues are put into the wrong thinking which context api is working fine at the latest version of react. 

Since React version v16.7.0, accroding to the [docs](https://5c54aa429e16c80007af3cd2--reactjs.netlify.com/docs/context.html), React provided one more method to use Context api which makes you more easy to bind the specified type of Context to one or more components(see [Component.contextType](https://5c54aa429e16c80007af3cd2--reactjs.netlify.com/docs/context.html#classcontexttype)), means everyone can keep using the OLD METHOD of the Context api while staying at the latest React version. This contextType, is not a must.

## React-Navigation
>http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1991

These Known issues are also put into the wrong thinking which if we use navigation correctly at the beginning, nothing will goes wrong.

Since Navigation version v2.x, [docs](https://reactnavigation.org/docs/en/2.x/redux-integration.html) WARN about Redux integration at the very top of the article, obviously Navigation wouldn't recommend to store Navigation into Redux, but the project starters still decided to do so, not sure why, but it's a bad practice.

Even more, Navigation supported lifecycle since version v2.x, we should know better before.

## The real issues
Most of the issues description at Gitlab about upgrade of React and React-Navigation are wrong, so what and where is the real problems? After a little digging, is just the way to write the code was wrong. 

For example the "Product" page, we know the logic to display all product is not simply, but the code who wrote is so confusing, there are "setState()" function everywhere without explanation, you can never predict what would happend before and after calling "setState()", because "setState()" is a asynchronous function(not synchronous function).

The [navigation setParams problem](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1995) is like the "Product" page problem which "setParams()" function is also a asynchronous function. 

Both setState() and setParams() will trigger the re-render function of the component, if we don't understand well and use it wisely, re-render function will be called again and again and again forever, that is the real issue we are dealing with.

The matter of fact, this only happen outside the EAPP module, I mention this because I wrote the EAPP module, everything's works fine in EAPP before and after the upgrade.

## Issues other than above
- [Release build crash after app upgrade](#issues-0006)

## Solution
Read document first, don't waste time.

## Reference
- https://5c54aa429e16c80007af3cd2--reactjs.netlify.com/docs/context.html
- https://5c54aa429e16c80007af3cd2--reactjs.netlify.com/docs/context.html#classcontexttype
- https://reactnavigation.org/docs/en/2.x/redux-integration.html
- https://reactnavigation.org/docs/en/navigation-prop.html#setparams-make-changes-to-route-params
