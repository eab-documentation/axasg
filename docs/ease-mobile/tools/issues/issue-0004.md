---
id: issue-0004
title: iOS Resources behavior (json file content won't update after app updated)
sidebar_label: Issue 0004
---

<table>
    <thead>
        <tr>
            <th>Version</th>
            <th>Date</th>
            <th>Desc</th>
            <th>Author</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.0</td>
            <td>11 Feb 2019</td>
            <td></td>
            <td>Jack Hon</td>
        </tr>
    </tbody>
</table>

## In short
We noticed if we manually(without programmatically) edit files inside "Resources" folder won't actually update the content(can't see the new data in our app), although we found the root cause, missing files to trigger to display the expected new data, we clearly need to know how iOS system works.

## Resources (Application Bundles)
>Application bundles are one of the most common types of bundle created by developers. The application bundle stores everything that the application requires for successful operation. Resources is one of the file types in an application bundle.
Resources are data files that live outside your application’s executable file. The placement of resource files in the bundle directory structure depends on whether you are developing an iOS or Mac app.

In our app, we store every json file in an application bundle, which is bad practice, especially the file like json file, json file contains data, and data is code, you don't put code in "Resources", nor any system's "Resources".

## Resources Rules
>Systems before OS X Mavericks 10.9 documented a signing feature (--resource-rules) to control which files in a bundle should be sealed by a code signature. This feature has been obsoleted for Mavericks. Code signatures made in Mavericks and later always seal all files in a bundle; there is no need to specify this explicitly any more. This also means that the Code Signing Resource Rules Path build setting in Xcode should no longer be used and should be left blank.
>
>It is thus no longer possible to exclude parts of a bundle from the signature. Bundles should be treated as read-only once they have been signed.

When debuging our issue, our first thought was bundle don't refresh/update itself, so we tried to manipulate to refresh, recreate or reload the bundle, but obviously there are no method to do so, bundle is read-only, so when is bundle update itself? <a id="StandardXcodebuildflow"/>See [Standard Xcode build flow](#StandardXcodebuildflow).

## Standard Xcode build flow
>Build the innermost code, copy it into the next-outer bundle, then sign that, etc., signing the outermost bundle last.

Install and Update app will do this. Means "Resources" (application bundles) will update to latest content everytime when you do install or update.

## Conclusion
The application bundle works fine but we still can't saw any updated content in our app, why? We simply forgot to put the must-have file into "Resources".

## Solution
Read document first, don't waste time.

## Reference
- https://developer.apple.com/library/archive/documentation/CoreFoundation/Conceptual/CFBundles/BundleTypes/BundleTypes.html#//apple_ref/doc/uid/10000123i-CH101-SW1
- https://developer.apple.com/library/archive/technotes/tn2206/_index.html#//apple_ref/doc/uid/DTS40007919-CH1-TNTAG206
- https://developer.apple.com/library/archive/technotes/tn2206/_index.html#//apple_ref/doc/uid/DTS40007919-CH1-TNTAG302
