---
id: issue-0002
title: Upgrade react native from v0.55 to v0.57
sidebar_label: Issue 0002
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 24 JAN 2019 | Init | Kevin Liang |
| 1.1 | 25 JAN 2019 | Update simulator model usage | Kevin Liang |

## Issue Date
Jan 2019

## Detail
All iOS apps submitted to the App Store/Testflight must be built with the iOS 12.1 SDK or later, included in Xcode 10.1 or later before March 2019. We use react native v0.55 (iOS SDK 11.4) before, so can't push new build to App Store/Testflight after March 2019.

![iOS SDK version upgrade](assets/img/issues/ios-sdk-version-upgrade.png)

## Solution
Upgrade react native version

### 1. Use XCode v10.1
Because of the slowly network connect to macOS App Store, I suggest go to [shared software pool](smb://192.168.222.56/Software_pool) to copy XCode_10.1.xip file and unarchive.

### 2. Check out to upgrade branch
The new APP with SDK 12.1 is ready on [upgrade branch](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/tree/upgrade). Please check out this branch.

### 3. Update Build System
>!.Please read [Xcode 10 fail to build with the new build system](issue-0007) first.
Update `File -> Project Setting -> Per-User Project Settings -> Build Setting` to "Legacy Build System".
Then run the APP on XCode on the first time.

### 4. New command to run APP on simulator
Because the simulator name can't support hyphen character on new version, the old command `"npm run ios (react-native run-ios --simulator='iPad Pro (10.5-inch)')"` can't use now. Please use `"npm run ios-mini (react-native run-ios --simulator='iPad Air 2')"`. If you still want to use simulator with hyphen character model, please use XCode to run.

### 5. Follow the upgrade milestone to upgrade
We create a [iOS SDK Upgrade milestone](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/milestones/1). Please follow the tasks to upgrade.

## Issues encountered
See the [Issues during react-native upgrade](issue-0005).

## External reference 
[Upgrading to new React Native versions](https://facebook.github.io/react-native/docs/upgrading)

