---
id: issue-knowledge-base
title: Issue Knowledge Base
sidebar_label: Issue Knowledge Base
---

Issues Reference:
- [Objective C NSDateFormatter return date with 1 more year](issue-0001.md)
- [Upgrade react native from v0.55 to v0.57](issue-0002.md)
- [CRUD before data sync cause can't sync any data](issue-0003.md)
- [iOS folder structure and behavior](issue-0004.md)
- [Issues during react-native upgrade](issue-0005.md)
- [Release build crash after app upgrade](issue-0006.md)
- [Xcode 10 fail to build with the new build system](issue-0007.md)
- [Cannot build (npm i) in China Office](issue-0008.md)





