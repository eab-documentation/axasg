---
id: debug-sentry-log
title: Sentry debug log Setting in EASE Mobile
sidebar_label: Sentry Log in Mobile
---


[Sentry.io](https://sentry.io/about) is the no.1 open-source error tracking platform that provides complete app logic, deep context, and visibility across the entire stack in real time. For the first time, developers can fix bugs at every stage of the product lifecycle, well before users ever encounter a problem.

In order not to slow down the performance, the production deployment version is set to limited logging information.
This guide show additional configuration of Sentry to enable stronger logging information for your development trouble shooting.

Reference:  
[Sentry Configuration for React-native](https://docs.sentry.io/clients/react-native/config/)


<span style="color:red">**Warning!** The setting aim for development, please do not check in your change to source control.</span>

## Change Log level
1. Open App.js (for EASE Mobile Project)
1. Locate the Sentry.config
1. Change or add the following setting to enable different level of Logging  
   ```js
   Sentry.config("https://xxxx@sentry.io/1257671", {
       // default SentryLog.None | Possible values:  .None, .Error, .Debug, .Verbose
       logLevel: SentryLog.Verbose
   }).install();
   ```

## Set User Context
Setup User Context so that you can easily find out the exception throw from your development machine.  
```js
Sentry.setUserContext({  
   userID: "bernard.wan",  
   username: "Bernard Wan"  
});
```  







