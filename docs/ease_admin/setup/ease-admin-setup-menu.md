---
id: ease-admin-setup-menu
title: Menu Setup
sidebar_label: Menu Setup
---

### Database Update
```sql
-- Rerunable
delete from SYS_FUNC_MST where FUNC_CODE ='FN.REPORT.WEBVSIOS';
INSERT INTO SYS_FUNC_MST (FUNC_CODE,NAME_CODE,FUNC_URI,SHOW_MENU,ACCESS_CTRL,UPLINE_FUNC_CODE,DISP_SEQ,STATUS,MODULE,SETABLE) VALUES ('FN.REPORT.WEBVSIOS', 'REPORT.WEBVSIOS', '/Report', 'Y',4,null, 2740,'A','WEBVSIOSReport','Y');

delete from SYS_FUNC_MST where FUNC_CODE ='FN.REPORT.WEBVSIOS.GENERATE';
INSERT INTO SYS_FUNC_MST (FUNC_CODE,NAME_CODE,FUNC_URI,SHOW_MENU,ACCESS_CTRL,UPLINE_FUNC_CODE,DISP_SEQ,STATUS,MODULE,SETABLE) VALUES ('FN.REPORT.WEBVSIOS.GENERATE', 'BUTTON.GENERATE', '/Report/Generate', 'N',5,'FN.REPORT.WEBVSIOS', 2741,'A','WEBVSIOSReport','Y');

delete from SYS_NAME_MST where NAME_CODE ='REPORT.WEBVSIOS';
INSERT INTO SYS_NAME_MST  (NAME_CODE, LANG_CODE, NAME_DESC, STATUS, IS_OFTEN_USE) VALUES ('REPORT.WEBVSIOS', 'en', 'WEB VS IOS Report', 'A', 'Y');

-- AXA will delete the records in SYS_USER_ROLE_FUNC, if the user does not has the related siteminder group
delete from SYS_USER_ROLE_FUNC where FUNC_CODE ='REPORT.WEBVSIOS';

INSERT INTO SYS_USER_ROLE_FUNC  (COMP_CODE, ROLE_CODE, FUNC_CODE, STATUS, CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY, APPROV_DATE,APPROV_BY)
VALUES ('08', 'ADMIN', 'REPORT.WEBVSIOS', 'A', sysdate, 'ADMIN', sysdate, 'ADMIN', sysdate, 'ADMIN');

delete from SYS_USER_ROLE_FUNC where FUNC_CODE ='FN.REPORT.WEBVSIOS.GENERATE';
INSERT INTO SYS_USER_ROLE_FUNC  (COMP_CODE, ROLE_CODE, FUNC_CODE, STATUS, CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY, APPROV_DATE,APPROV_BY)
VALUES ('08', 'ADMIN', 'FN.REPORT.WEBVSIOS.GENERATE', 'A', sysdate, 'ADMIN', sysdate, 'ADMIN', sysdate, 'ADMIN');
```

### Admin JAVA Code Change
```java
// In loginBySaml function
/** A roleList can be found in LoginManager.java
* This roleList contains all the group of siteminder
* Each AXA user has different groups to decide which function that the user can access
*/
List<String> roleList = new ArrayList<String>() {
				{
					add("ease_sg_agt-amt");
					add("ease_sg_prd-egt");
					add("ease_sg_fin");
					add("ease_sg_cpf");
					add("ease_sg_prd-cnf");
					add("ease_sg_ita");
					add("ease_sg_ics");
					add("ease_sg_ops");
					add("ease_sg_prxy");
					add("ease_sg_wvi");
				}
			};

/** Below example is adding the web vs ios report to the "ease_sg_ics" group
*   Then web vs ios report will display to user when the user has "ease_sg_ics" group
*/
rights = new ArrayList<String>() {{
						add("FN.REPORT.ALLCHANNEL");
						add("FN.REPORT.ALLCHANNEL.GENERATE");
						
						// Add Web vs IOS report when all channel report right is granted by siteminder
						add("REPORT.WEBVSIOS");
						add("FN.REPORT.WEBVSIOS.GENERATE");
					}};
					put("ease_sg_ics", rights);
```