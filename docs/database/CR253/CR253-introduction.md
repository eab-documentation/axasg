---
id: CR253-introduction
title: CR253 Introduction
sidebar_label: CR253 Introduction
---
| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |

## Infra Image 架构图
[![EASE architecture](assets/img/database/infra.png)](assets/img/database/infra.png)

## Simple Infra Image (not fully) 简单版的架构图（不是最全版）
[![EASE simple architecture](assets/img/database/simpleInfra.png)](assets/img/database/simpleInfra.png)

## Timeline/Plan 时间表/计划
[![Timeline](assets/img/database/timeline.png)](assets/img/database/timeline.png)

## Testing 测试
### EASE-WEB
- Client Profile (check that all drop downs are as expected and able to save and retrieve)
- FNA ➡️ Test all different options, needs, etc. 
- FNA Report ➡️ Check that FNA report is as expected
- PI ➡️ 2 STP (1st party and 3rd party) and 2 Quick Quote per product.
- eApp ➡️ Recommendation, applying, signature, payment, supporting documents, submission
- eApp report ➡️ Check that eApp form PDF is as expected
- Supervisor approval ➡️ Review documents, applications and approve
- RLS ➡️ Check that the RLS mapping is ok for all STP cases above.
- WFI ➡️ Check that cases flow to WFI
- SFDC ➡️ Check that cases flow to SFDC on successful callback from RLS
- Email ➡️ Check emails at each stage like PI, eApp, post submission
- Signature expiry and warning
- Application Invalidation scenarios, like changing client profile information, etc.

### EASE Admin
- All channels report
- Product eligibility upload
- BCP module

### EASE Data Transfer (CR157)
- Initial job works as expected
- Daily job works as expected
