---
id: CR253-config
title: CR253 Config
sidebar_label: CR253 Config
---
| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |

## Database
### Mongodb (v3.6) for DEV
Reference Link: [https://docs.mongodb.com/v3.6/reference/method/js-collection/](https://docs.mongodb.com/v3.6/reference/method/js-collection/)

| Key | Value |
| :-: | :-: |
| DB Server | 192.168.225.180 |
| DB Name | AXASG_DEV |
| DB Port | 47017 |
| DB Login ID | admin |
| DB Password | [Click here to get](https://teams.microsoft.com/l/entity/com.microsoft.teamspace.tab.wiki/tab::be61d822-4a4c-421f-8502-e10da065f6d0?context=%7B%22subEntityId%22%3A%22%7B%5C%22pageId%5C%22%3A2%2C%5C%22sectionId%5C%22%3A4%2C%5C%22origin%5C%22%3A2%7D%22%2C%22channelId%22%3A%2219%3A7716c49af5d34cf0ab57b0b073fde880%40thread.skype%22%7D&tenantId=c53d384d-46a9-4e08-998c-5133acc818f0) |

### DocumentDB for DEV
???

### Sync Gateway
Link: [http://192.168.225.97:4985/axasg_cr253](http://192.168.225.97:4985/axasg_cr253)

### Couchbase Server
Link: [http://192.168.225.98:8091/](http://192.168.225.98:8091/)

Account: [Click here to get](https://teams.microsoft.com/l/entity/com.microsoft.teamspace.tab.wiki/tab::be61d822-4a4c-421f-8502-e10da065f6d0?context=%7B%22subEntityId%22%3A%22%7B%5C%22pageId%5C%22%3A2%2C%5C%22sectionId%5C%22%3A4%2C%5C%22origin%5C%22%3A2%7D%22%2C%22channelId%22%3A%2219%3A7716c49af5d34cf0ab57b0b073fde880%40thread.skype%22%7D&tenantId=c53d384d-46a9-4e08-998c-5133acc818f0)

### S3 Dashboard
Link: [https://725405650012.signin.aws.amazon.com/console](https://725405650012.signin.aws.amazon.com/console)

Account: [Click here to get](https://teams.microsoft.com/l/entity/com.microsoft.teamspace.tab.wiki/tab::be61d822-4a4c-421f-8502-e10da065f6d0?context=%7B%22subEntityId%22%3A%22%7B%5C%22pageId%5C%22%3A2%2C%5C%22sectionId%5C%22%3A6%2C%5C%22origin%5C%22%3A2%7D%22%2C%22channelId%22%3A%2219%3A7716c49af5d34cf0ab57b0b073fde880%40thread.skype%22%7D&tenantId=c53d384d-46a9-4e08-998c-5133acc818f0)