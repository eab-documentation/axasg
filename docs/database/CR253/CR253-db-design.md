---
id: CR253-db-design
title: CR253 DB Design
sidebar_label: CR253 DB Design
---
| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |


| Type | Couchbase doc Type | DocumentDB Collection |
| :-: | :-: | :-: |
| Transaction Data | cust | customer |
| Transaction Data | agent | agent |
| Transaction Data | bundle | fna |
| Transaction Data | fe | fna |
| Transaction Data | na | fna |
| Transaction Data | pda | fna |
| Transaction Data | quotation | quotation |
| Transaction Data | application | application |
| Transaction Data | approval | approval |
| Transaction Data | masterApplication | sheildApplication |
| Transaction Data | masterApproval | shieldApproval |
| Master Data | BLOCK | masterData |
| Master Data | PAPER | masterData |
| Master Data | apiResponse | masterData |
| Master Data | campaign | masterData |
| Master Data | email_template | masterData |
| Master Data | layout | masterData |
| Master Data | menuItem | masterData |
| Master Data | pdfTemplate | masterData |
| Master Data | settings | masterData |
| Master Data | setup | masterData |
| Master Data | template | masterData |
| Master Data | material | masterData |
| Master Data | fund | masterData |
| Master Data | product | masterData |
| Master Data | empty or null | masterData |
