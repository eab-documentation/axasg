---
id: CR253-documentDB-guideline
title: CR253 DocumentDB Guideline
sidebar_label: CR253 DocumentDB Guideline
---
| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 7 May 2020 | Init | Kevin Liang |

## Introduction  介绍
### EAB Environment
Because DocumentDB is a private service on AWS, we need EC2 as a bridge to connect when we are on EAB environment.

[Draw IO File](assets/img/database/ec2DocumentDBVPConAWS.drawio)

因为DocumentDB是一个AWS私有的服务，所以我们EAB环境需要EC2来搭桥进行连接 

[Draw IO 文件](assets/img/database/ec2DocumentDBVPConAWS.drawio)

[![ec2DocumentDBVPConAWS](assets/img/database/ec2DocumentDBVPConAWS.png)](assets/img/database/ec2DocumentDBVPConAWS.png)

### AXA Environment
In AXA environment, dockers(ease-web, ease-api, etc.) and DocumentDB are in same VPC. They don't need EC2 service.

在AXA环境下，由于docker(ease-web, eaes-api等等)和DocumentDB在同一个VPC内，所以他们不需要EC2服务

## Official Document to Setup DocumentDB 启动DocumentDB的官方文档
English Version: [English Document](https://docs.aws.amazon.com/documentdb/latest/developerguide/getting-started-prerequisites.html)

中文官方文档： [Chinese Document](https://docs.aws.amazon.com/zh_cn/documentdb/latest/developerguide/getting-started-prerequisites.html)

## Non-offical Document to Setup DocumentDB 启动DocumentDB的非官方文档
### Prerequisite 先决条件
- Please make sure you have grant below access right from IT before staring to create a new AWS documentDB and EC2.

- 请确保你的AWS账户有下面的权限，如果没有，请联系IT部门进行授权
[![accessRight](assets/img/database/accessRight.png)](assets/img/database/accessRight.png)

- Please make sure you selected “Singapore” region in AWS”
- 请确保你的AWS区域是新加坡

### Step 1 第一步
Go to ```Amazon DocumentDB```

前往```Amazon DocumentDB```页面
[![setupDocumentDBStep1](assets/img/database/setupDocumentDBStep1.png)](assets/img/database/setupDocumentDBStep1.png)

### Step 2 第2步
Click ```Create``` Clusters

点击 ```创建``` Clusters
[![setupDocumentDBStep2](assets/img/database/setupDocumentDBStep2.png)](assets/img/database/setupDocumentDBStep2.png)

### Step 3 第3步
Input below highlighted areas then click create

在下面画圈区域输入，然后点击创建
[![setupDocumentDBStep3](assets/img/database/setupDocumentDBStep3.png)](assets/img/database/setupDocumentDBStep3.png)

### Step 4 第4步
Go to EC2

前往EC2页面
[![setupDocumentDBStep4](assets/img/database/setupDocumentDBStep4.png)](assets/img/database/setupDocumentDBStep4.png)

### Step 5 第5步
Click ```Launch Instance```

点击```启动实例```
[![setupDocumentDBStep5](assets/img/database/setupDocumentDBStep5.png)](assets/img/database/setupDocumentDBStep5.png)

### Step 6 第6步
Click below item then click ```Select```

点击下面然后点 ```选择```
[![setupDocumentDBStep6](assets/img/database/setupDocumentDBStep6.png)](assets/img/database/setupDocumentDBStep6.png)

### Step 7 第7步
Select below item then click ```Next```

选择下面然后点击```下一步```
[![setupDocumentDBStep7](assets/img/database/setupDocumentDBStep7.png)](assets/img/database/setupDocumentDBStep7.png)

### Step 8 第8步
Nothing to change, click ```Next```

什么都不用设置，点```下一步```
[![setupDocumentDBStep8](assets/img/database/setupDocumentDBStep8.png)](assets/img/database/setupDocumentDBStep8.png)

### Step 9 第9步
Nothing to change, click ```Next```

什么都不用设置，点```下一步```
[![setupDocumentDBStep9](assets/img/database/setupDocumentDBStep9.png)](assets/img/database/setupDocumentDBStep9.png)

### Step 10 第10步
Nothing to change, click ```Next```

什么都不用设置，点```下一步```
[![setupDocumentDBStep10](assets/img/database/setupDocumentDBStep10.png)](assets/img/database/setupDocumentDBStep10.png)

### Step 11 第11步
Change below areas, click ```Review and Launch```

改变下方的区域，点击 ```审核并发布```
[![setupDocumentDBStep11](assets/img/database/setupDocumentDBStep11.png)](assets/img/database/setupDocumentDBStep11.png)

### Step 12 第12步
Nothing to change, click ```Launch```

什么都不用设置，点```启动```
[![setupDocumentDBStep12](assets/img/database/setupDocumentDBStep12.png)](assets/img/database/setupDocumentDBStep12.png)

### Step 13 第13步
Change the name what you want and download the key pair, Click ```Launch Instances```

改成你想要的名字，下载私钥，点击```启动实例```
[![setupDocumentDBStep13](assets/img/database/setupDocumentDBStep13.png)](assets/img/database/setupDocumentDBStep13.png)

### Step 14 第14步
Download CA certificate from below area

输入下面的网址下载CA证书

[https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem](https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem)

## Connect DocumentDB via Studio 3T (For EAB Environment)
### Step 1 第1步
Open Studio 3T and click Connect

打开Studio 3T软件，点击连接
[![studio3TConnection1](assets/img/database/studio3TConnection1.png)](assets/img/database/studio3TConnection1.png)

### Step 2 第2步
Click new Connection

点击新建连接
[![studio3TConnection2](assets/img/database/studio3TConnection2.png)](assets/img/database/studio3TConnection2.png)

### Step 3 第3步
Input Connection Name and click ```From URL…```

输入连接名，并点 ```来自URL...```
[![studio3TConnection3](assets/img/database/studio3TConnection3.png)](assets/img/database/studio3TConnection3.png)

### Step 4 第四步
Copy and paste the AWS documentDB connection String

Make sure to replace the ```<insertYourPassword>``` in the connection string to your defined password in [Step 3](#step-3-第3步)

复制粘贴AWS DocumentDB的连接字符串

确保替换了字符串里面的```<insertYourPassword>```，替换成[步骤3](#step-3-第3步)设置的密码
[![studio3TConnection4](assets/img/database/studio3TConnection4.png)](assets/img/database/studio3TConnection4.png)

### Step 5 第5步
Go to SSL tab and upload CA file

CA certificate file can get from [Step 14](#step-14-第14步)

前往SSL选项卡，并上传CA证书文件

CA证书文件可以从[14步](#step-14-第14步)获得
[![studio3TConnection5](assets/img/database/studio3TConnection5.png)](assets/img/database/studio3TConnection5.png)

### Step 6 第6步
Go to SSH tab, copy and paste SSH Address from AWS EC2 service

前往SSH选项卡，从EC2面板上复制粘贴SSH地址
[![studio3TConnection6](assets/img/database/studio3TConnection6.png)](assets/img/database/studio3TConnection6.png)

### Step 7 第7步
Copy and paste SSH User name from AWS EC2 service (default: ec2-user)

复制粘贴EC2 SSH的用户名，默认是ec2-user
[![studio3TConnection7](assets/img/database/studio3TConnection7.png)](assets/img/database/studio3TConnection7.png)

### Step 8 第8步
Select ```Private Key``` then upload the key, the key file can be found in [Step 13](#step-13-第13步)

认证类型选择```私钥```，然后上传[13步](#step-13-第13步)的私钥文件
[![studio3TConnection8](assets/img/database/studio3TConnection8.png)](assets/img/database/studio3TConnection8.png)

### Step 9 第9步
Final step click ```Test Connection``` to test

最后点击 ```测试连接``` 进行测试
[![studio3TConnection9](assets/img/database/studio3TConnection9.png)](assets/img/database/studio3TConnection9.png)










