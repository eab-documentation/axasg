---
id: ease-architecture
title: EASE Architecture 架构
sidebar_label: EASE Architecture 架构
---

| Version | Date | Desc | Author |
| :-: | :-: | :-: | :-: |
| 1.0 | 30 JAN 2019 | Init | Kevin Liang |
| 1.1 | 17 April 2020 | Update Infra Image | Kevin Liang |
| 1.2 | 17 April 2020 | Update data center links, Delete useless links | Kevin Liang |
| 1.3 | 6 May 2020 | Team Member, Ease-data-transfer introduction, Mantis | Kevin Liang |
| 1.3 | 6 May 2020 | Add Openshift, Redis | Kevin Liang |

## Infra Image on AWS 在AWS上的架构图
[![EASE architecture](assets/architecture/infraOnAWS.png)](assets/architecture/infraOnAWS.png)

## Simple Infra Image (not fully) 简单版的架构图（不是最全版）
[![EASE architecture](assets/architecture/architecture.png)](assets/architecture/architecture.png)

## Mobile Section (Deprecated) 移动版（项目已取消）
### 121 Mobile App (Deprecated)
- Role: Show related infomation to agent and client
- Gitlab Project: [AXA_SG_APP](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app)

### 121-WebService(APP-WS) (Deprecated)
- Role: Handle online logic from mobile.We also call it Mobile API sometimes.
- Gitlab project: [AXA_SG_APP_web_service](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service)

## Website Section 网站部分
### 121-Web
- Role: Show related infomation to agent and client
- Gitlab Project: [ease-web](http://192.168.222.60:8001/AXA-SG/ease-web)

### 121-Admin
- Role: Template settings
- Gitlab Project: [ease-admin](http://192.168.222.60:8001/AXA-SG/ease-admin)

### Product Configurator (nickname: iConfig)
- Role: Config product with easy way
- Gitlab Project: [iConfig](http://192.168.222.60:8001/eab-product-team/121-configurator)

### eSignature
- Role: Show website signature section

### Redis
- Role: For store/get session info for ease-web backend

## Public Section 公共部分

### 121-API(EASE-API)
- Role: Handle logic shared with mobile and website. Handle the logic between ease-api and third-party systems. Save audit logs.
- 角色：处理手机端和网页端共同的逻辑，和第三方系统交互的逻辑，以及审计日志的记录
- Path:
    - ~~SIT1: [http://api.ease-sit.intraeab/](http://api.ease-sit.intraeab/)~~ (no use now)
    - ~~SIT2: [http://api.ease-sit2.intraeab/](http://api.ease-sit2.intraeab/)~~ (no use now)
    - SIT3(Old Openshift): [http://api.ease-sit3.intraeab/](http://api.ease-sit3.intraeab/)
    - SIT3(New Openshift): [http://api.ease-sit3.intraeabdc/](http://api.ease-sit3.intraeabdc/)

### NoSQL Sync Gateway (Couchbase Sync Gateway)
- Role: Sync or CRUD data between client side and Couchbase Server.
- 角色： 在Couchbase服务器和客户端之间同步或者CRUD数据
- Path: 
    - ~~SIT1: [http://192.168.222.169:4985](http://192.168.222.169:4985)~~ (no use now)
    - ~~SIT2: [http://sync.ease-sit2.intraeab](http://sync.ease-sit2.intraeab)~~ (no use now)
    - ~~SIT3: [http://192.168.222.97](http://192.168.222.97)~~ (move to data center xxx.xxx.225.xxx)
    - SIT3: [http://192.168.225.97](http://192.168.225.97) (4985 port for admin, 4984 port for non-admin)
    - AXA SIT: [https://app.ease-sit.axa.com.sg](https://app.ease-sit.axa.com.sg)
    - AXA UAT: [https://app.ease-uat.axa.com.sg](https://app.ease-uat.axa.com.sg)
    - AXA PRE-PROD: [https://app.ease-preprd.axa.com.sg](https://app.ease-preprd.axa.com.sg)
    - AXA PROD: [https://sync.ease.axa.com.sg](https://sync.ease.axa.com.sg)

### NoSQL Database （Couchbase Server)
- Role: Store data
- 角色：存数据
- Path:
    - ~~SIT1: [http://192.168.222.168:8091/ui/index.html#](http://192.168.222.168:8091/ui/index.html#)~~(no use now)
    - ~~SIT2~~(no use now)
    - ~~SIT3: [http://192.168.222.98:8091/ui/index.html](http://192.168.222.98:8091/ui/index.html)~~(move to data center xxx.xxx.225.xxx)
    - SIT3: [http://192.168.225.98:8091/ui/index.html](http://192.168.225.98:8091/ui/index.html)

### Relational Database(Oracle) 关系型数据库(Oracle)
- Role: Store other data such as audit log, mobile log in related information, policy number pool, etc.
- 角色： 存例如审计日记、移动端相关的信息、保单号库等等
- Config:
    - SIT1: 
        - Server: 192.168.222.156
        - Port: 1521
        - User name: epos
    - SIT2:
        - Server: 192.168.222.156
        - Port: 1521
        - User name: epos_sit2
    - SIT3: (shared with SIT2)
        - Server: 192.168.222.156
        - Port: 1521
        - User name: epos_sit2

### Firewall 防火墙
- Role: Block illegal http request
- 角色：禁止不合法的http请求
- Note: We have this firewall setting only in AXA environment. It contains 2 systems: kong and WAF. Kong blocks the end points which not include in our Mobile API. WAF blocks the suspicious http request such as 'Attack signature detected' and 'Illegal HTTP status in response'.
- 注意： 仅在AXA场才会有这个防火墙。 它包含两个系统： kong和WAF。Kong是用来禁止非自家API类的请求。 WAF是用来禁止类似'Attack signature detected'和'Illegal HTTP status in response'的请求

### SPE and SPE Agent
- Role: For anti virus on upload files stage
- 角色： 上传文件阶段用于扫描病毒
- Gitlab Project: [spe-agent](http://192.168.222.60:8001/AXA-SG/ease-spe-agent)
- Path: [http://spe-agent.ease-sit3.intraeab/](http://spe-agent.ease-sit3.intraeab/)

### EASE Data Transfer
- Role: For retrieve data which are update each days to analyze
- 角色：用于每天获取数据库更新的数据，以便用于分析
- Gitlab Project: [EASE Data Transfer](http://192.168.222.60:8001/AXA-SG/ease-data-transfer)
- Note: There are 2 major branches on this project. ```init_dev``` branch is for retrieving old data. ```transfer_dev``` branch is for retrieving previous day update data.
- 注意： 这个项目有两个分支， ```init_dev```分支是用来处理旧数据的，```transfer_dev```分支是用来获取前一天更新的数据的

### Openshift
- Role: A manage platform which built-in with docker for hosting services on SIT environment
- 角色：一个自带docker的管理平台，用于SIT场跑服务
- Path:
    - Old Openshift: 
        - Address: [https://vm-openshift-poc-1:8443/login](https://vm-openshift-poc-1:8443/login)
        - User name: admin
        - Password: Check with IT/[Arne Ovesen](https://teams.microsoft.com/l/chat/0/0?users=arne.ovesen@eabsystems.com.hk)/[Biggos Zhao](https://teams.microsoft.com/l/chat/0/0?users=biggos.zhao@eabsystems.com.hk)
    - New Openshift on data center:
        - Address: [https://admin.ocp.eabdc:8443/](https://admin.ocp.eabdc:8443/)
        - User name: Your LDAP user name (such as ```kevin.liang```)
        - Password: Your LDAP password (or your Windows/MacBook password)
        - Note: Please select second option (ldap_login) to login ⬇️ 
        - 注意：请使用第二个选项(ldap_login)进行登陆
[![See react](assets/img/cicd/example19.png)](assets/img/cicd/example19.png)
    - Which Openshift/VM host which services

| Project | Old Openshift | New Openshift(on data center) | vm | comment |
| :-: | :-: | :-: | :-: | :-: |
| [ease-web](#121-web) |  | √ | | |
| [ease-admin](#121-admin) |  | √ | | |
| [product-configurator](#product-configurator-nickname-iconfig) |  | √ | | |
| [e-Signature](#esignature) |  | √ | | |
| [redis](#redis) |  | √ | | |
| [ease-api](#121-apiease-api) |  | √ | | |
| [Couchabse Sync Gateway](#nosql-sync-gateway-couchbase-sync-gateway) |  |  |√ | |
| [Couchabse Server](#nosql-database-（couchbase-server) |  |  |√ | |
| [Oracle](#relational-databaseoracle-关系型数据库oracle) |  |  |√ | |
| [SPE-agent](#spe-and-spe-agent) | √ |  | | |
| [SPE](#spe-and-spe-agent) |  |  | √| |
| [ease-data-transfer(daily)](#ease-data-transfer) | √ |  | | |
| [ease-data-transfer(init)](#ease-data-transfer) | √ |  | | |

## Team Member 团队成员

| Member | Location |Role | Comment |
| :-: | :-: | :-: | :-: |
| [Arne Ovesen](https://teams.microsoft.com/l/chat/0/0?users=arne.ovesen@eabsystems.com.hk) | HK | EASE Project Manager | Familiar with EASE business |
| [Kelvin Wong](https://teams.microsoft.com/l/chat/0/0?users=kelvin@eabsystems.com.hk) | HK | EASE Technical Support | Familiar with integrating with third-party systems, such as [RLS](/axasg/docs/ease_api/RLS), [WFI](/axasg/docs/ease_api/WFI), [DCR](/axasg/docs/ease_api/DCR), [SFDC](/axasg/docs/ease_api/SFDC) |
| [Biggos Zhao](https://teams.microsoft.com/l/chat/0/0?users=biggos.zhao@eabsystems.com.hk) | SZ | EASE Developer Team Leader | Knows much about deployment/publish |
| [Hu Zhongbin](https://teams.microsoft.com/l/chat/0/0?users=hu.zhongbin@eabsystems.com.hk) | SZ | Developer |  |
| [Gao Xiangyu](https://teams.microsoft.com/l/chat/0/0?users=xiangyu.gao@eabsystems.com.hk) | SZ | Developer | Knows more about [ease-data-transfer](#ease-data-transfer) project |
| [Huang Xiaolin](https://teams.microsoft.com/l/chat/0/0?users=xiaolin.huang@eabsystems.com.hk) | SZ | Developer |  |

## Issues Reported by user (AXA) 客户报的issue
- Link: https://itracker.eabsystems.com/mantisbt
